package com.oseasy.xlxq.service.user.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.user.model.UserInfo;
import com.oseasy.xlxq.service.api.user.service.UserInfoService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.user.dao.UserInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
@Service
public class UserInfoServiceImpl extends AbstractService<UserInfo> implements UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;

    @Override
    public BaseDaoInterface<UserInfo, Serializable> getDao() {
        return userInfoDao;
    }
}
