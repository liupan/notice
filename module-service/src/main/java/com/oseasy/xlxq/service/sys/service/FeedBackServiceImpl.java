package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.FeedBackInfo;
import com.oseasy.xlxq.service.api.sys.service.FeedBackService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.FeedBackDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
@Service
public class FeedBackServiceImpl extends AbstractService<FeedBackInfo> implements FeedBackService {

    @Autowired
    private FeedBackDao feedBackDao;

    @Override
    public BaseDaoInterface<FeedBackInfo, Serializable> getDao() {
        return feedBackDao;
    }
}
