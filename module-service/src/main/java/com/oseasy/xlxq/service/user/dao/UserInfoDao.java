package com.oseasy.xlxq.service.user.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.user.model.UserInfo;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface UserInfoDao extends BaseDaoInterface<UserInfo,Serializable>{
}
