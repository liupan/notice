package com.oseasy.xlxq.service.user.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.user.model.AdminInfo;
import com.oseasy.xlxq.service.api.user.service.AdminInfoService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.user.dao.AdminInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/10/15.
 */
@Service
public class AdminInfoServiceImpl extends AbstractService<AdminInfo> implements AdminInfoService {

    @Autowired
    private AdminInfoDao adminInfoDao;

    @Override
    public BaseDaoInterface<AdminInfo, Serializable> getDao() {
        return adminInfoDao;
    }
}
