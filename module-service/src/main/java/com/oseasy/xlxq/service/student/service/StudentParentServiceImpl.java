package com.oseasy.xlxq.service.student.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.model.StudentParent;
import com.oseasy.xlxq.service.api.student.service.StudentParentService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.student.dao.StudentParentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/19.
 */
@Service
public class StudentParentServiceImpl extends AbstractService<StudentParent> implements StudentParentService{

    @Autowired
    private StudentParentDao studentParentDao;

    @Override
    public BaseDaoInterface<StudentParent, Serializable> getDao() {
        return studentParentDao;
    }
}
