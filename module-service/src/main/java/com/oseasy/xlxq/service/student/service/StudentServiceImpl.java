package com.oseasy.xlxq.service.student.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.service.StudentService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.student.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/16.
 */
@Service
public class StudentServiceImpl extends AbstractService<Student> implements StudentService {

    @Autowired
    private StudentDao studentDao;
    @Override
    public BaseDaoInterface<Student, Serializable> getDao() {
        return this.studentDao;
    }


}
