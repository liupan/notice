package com.oseasy.xlxq.service.classinfo.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;
import com.oseasy.xlxq.service.api.classinfo.service.ClassService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.classinfo.dao.ClassInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
@Service
public class ClassInfoServiceImpl extends AbstractService<ClassInfo> implements ClassService {

    @Autowired
    private ClassInfoDao classInfoDao;

    @Override
    public BaseDaoInterface<ClassInfo, Serializable> getDao() {
        return this.classInfoDao;
    }
}
