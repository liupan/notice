package com.oseasy.xlxq.service.message.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.message.model.MessageInfo;
import com.oseasy.xlxq.service.api.message.service.MessageService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.message.dao.MessageInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
@Service
public class MessageInfoServiceImpl extends AbstractService<MessageInfo> implements MessageService {

    @Autowired
    private MessageInfoDao messageInfoDao;

    @Override
    public BaseDaoInterface<MessageInfo, Serializable> getDao() {
        return messageInfoDao;
    }
}
