package com.oseasy.xlxq.service.message.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.message.model.MessageInfo;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface MessageInfoDao  extends BaseDaoInterface<MessageInfo,Serializable>{
}
