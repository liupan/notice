package com.oseasy.xlxq.service.student.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.student.model.Student;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/16.
 */
public interface StudentDao extends BaseDaoInterface<Student,Serializable>{
}
