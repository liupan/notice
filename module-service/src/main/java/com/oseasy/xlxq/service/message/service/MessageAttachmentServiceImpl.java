package com.oseasy.xlxq.service.message.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.message.model.MessageAttachment;
import com.oseasy.xlxq.service.api.message.service.MessageAttachmentService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.message.dao.MessageAttachmentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
@Service
public class MessageAttachmentServiceImpl extends AbstractService<MessageAttachment> implements MessageAttachmentService {

    @Autowired
    private MessageAttachmentDao messageAttachmentDao;

    @Override
    public BaseDaoInterface<MessageAttachment, Serializable> getDao() {
        return messageAttachmentDao;
    }
}
