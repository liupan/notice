package com.oseasy.xlxq.service.classinfo.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface ClassInfoDao extends BaseDaoInterface<ClassInfo,Serializable> {
}
