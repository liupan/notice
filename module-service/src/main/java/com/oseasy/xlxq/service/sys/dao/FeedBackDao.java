package com.oseasy.xlxq.service.sys.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.FeedBackInfo;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface FeedBackDao extends BaseDaoInterface<FeedBackInfo,Serializable> {
}
