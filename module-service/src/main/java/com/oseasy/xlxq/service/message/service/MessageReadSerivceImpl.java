package com.oseasy.xlxq.service.message.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.message.model.MessageRead;
import com.oseasy.xlxq.service.api.message.service.MessageReadService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.message.dao.MessageReadDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
@Service
public class MessageReadSerivceImpl  extends AbstractService<MessageRead> implements MessageReadService{

    @Autowired
    private MessageReadDao messageReadDao;

    @Override
    public BaseDaoInterface<MessageRead, Serializable> getDao() {
        return messageReadDao;
    }
}
