package com.oseasy.xlxq.service.student.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.model.StudentParent;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/19.
 */
public interface StudentParentDao  extends BaseDaoInterface<StudentParent,Serializable> {
}
