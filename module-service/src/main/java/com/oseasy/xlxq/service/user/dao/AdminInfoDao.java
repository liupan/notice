package com.oseasy.xlxq.service.user.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.user.model.AdminInfo;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/10/15.
 */
public interface AdminInfoDao extends BaseDaoInterface<AdminInfo,Serializable> {
}
