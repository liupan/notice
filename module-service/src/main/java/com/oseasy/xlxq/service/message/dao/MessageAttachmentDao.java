package com.oseasy.xlxq.service.message.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.message.model.MessageAttachment;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface MessageAttachmentDao extends BaseDaoInterface<MessageAttachment,Serializable> {
}
