package com.oseasy.xlxq.service.core.exceptions;

/**
 * Created by Tandy on 2016/6/25.
 */
public class FrameworkRuntimeException extends Exception{
    private String errorCode;
    private String errorMsg;
    private Object[] ctxParams;

//    public FrameworkRuntimeException(AllErrors error){
//        super(error.toString());
//        this.errorCode = error.getErrorCode();
//        this.errorMsg = error.getErrorMsg();
//
//    }

    public void params(Object ... params){
        this.ctxParams = params;
    }

}
