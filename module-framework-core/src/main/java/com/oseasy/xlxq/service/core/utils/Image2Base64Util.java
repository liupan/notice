package com.oseasy.xlxq.service.core.utils;


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Image2Base64Util {

    /**
     * @Description: 将base64编码字符串转换为bytes
     * @Author:
     * @CreateTime:
     * @param imgStr base64编码字符串
     * @return
     */
    public static byte[] base642bytes(String imgStr) {
        if (imgStr == null) return null;
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] b;
        try {
            // 解密
            b = decoder.decodeBuffer(imgStr);
            // 处理数据
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
        }
        catch (Exception e) {
            return null;
        }
        return b;
    }

    /**
     * @Description: bytes转换为base64编码字符串
     * @Author:
     * @CreateTime:
     * @param data byte数组
     * @return
     */
    public static String bytes2base64(byte[] data) {
        // 加密
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }

    /**
     * @Description: 根据图片地址转换为base64编码字符串
     * @Author:
     * @CreateTime:
     * @return
     */
    public static String getImageStr(String imgFile) {
        InputStream inputStream = null;
        byte[] data = null;
        try {
            inputStream = new FileInputStream(imgFile);
            data = new byte[inputStream.available()];
            System.out.println(data.length);
            inputStream.read(data);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 加密
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }
}

