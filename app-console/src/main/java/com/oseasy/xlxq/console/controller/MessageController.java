package com.oseasy.xlxq.console.controller;

import com.oseasy.xlxq.console.util.StringUtil;
import com.oseasy.xlxq.service.api.message.model.MessageAttachment;
import com.oseasy.xlxq.service.api.message.model.MessageInfo;
import com.oseasy.xlxq.service.api.message.service.MessageAttachmentService;
import com.oseasy.xlxq.service.api.message.service.MessageService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/17.
 */
@RestController
@RequestMapping("message")
public class MessageController {


    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageAttachmentService messageAttachmentService;

    @RequestMapping("findMessageList")
    public RestResponse findMessageList(String title, Integer parentMessageId, String startTime, String endTime, Integer classId, Integer pageNo, Integer pageSize, Integer isRely, Integer isUrgent) {
        RestResponse restResponse = new RestResponse();
        try {

            String hql = "from MessageInfo where isDelete = 0";
            List<Object> value = new ArrayList<>();
            if (!StringUtil.isBlank(parentMessageId)) {
                hql += " and parentMessageId = ? ";
                value.add(parentMessageId);
            } else {
                hql += " and parentMessageId is null";
            }
            if (!StringUtil.isBlank(title)) {
                hql += " and title like ?";
                value.add("%" + title + "%");
            }
            if (!StringUtil.isBlank(classId)) {
                hql += " and classId = ?";
                value.add(classId);
            }
            if (!StringUtil.isBlank(isRely)) {
                hql += " and isReply = ?";
                value.add(isRely);
            }
            if (!StringUtil.isBlank(isUrgent)) {
                hql += " and isUrgent = ?";
                value.add(isUrgent);
            }
            if (!StringUtil.isBlank(startTime)) {
                hql += " and insertTime > ?";
                value.add(startTime);
            }
            if (!StringUtil.isBlank(endTime)) {
                hql += " and insertTime < ?";
                value.add(endTime);
            }
            Page<MessageInfo> messageInfoPage = messageService.getPageLists(hql, pageNo, pageSize, value);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(messageInfoPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restResponse;
    }


    @RequestMapping("deleteMessage")
    public RestResponse deleteMessage(Integer id) {
        RestResponse restResponse = new RestResponse();
        this.messageService.executeSql("update MessageInfo set isDelete = 1 , deleteTime = SYSDATE() where id = " + id);
        this.messageService.executeSql("update MessageInfo set isDelete = 1 , deleteTime = SYSDATE() where parentId = " + id);
        this.messageService.executeSql("update MessageAttachment set isDelete = 1 , deleteTime = SYSDATE() where messageId = " + id);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }


    @RequestMapping("findMessageAttachment")
    public RestResponse findMessageAttachment(Integer messageId, Integer type,Integer pageNo,Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from MessageAttachment where isDelete = 0 ";
            List<Object> value = new ArrayList<>();
            if (!StringUtil.isBlank(messageId)) {
                hql += " and messageId = ?";
                value.add(messageId);
            }
            if (!StringUtil.isBlank(type)) {
                hql +=" and type = ?";
                value.add(type);
            }
            Page<MessageAttachment> messageAttachmentPage = messageAttachmentService.getPageLists(hql, pageNo, pageSize, value);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(messageAttachmentPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restResponse;
    }

    @RequestMapping("deleteMessageAttachment")
    public RestResponse deleteMessageAttachment(Integer id){
        RestResponse restResponse = new RestResponse();
        this.messageService.executeSql("update MessageAttachment set isDelete = 1 , deleteTime = "+new Date()+" where id = " + id);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }
}
