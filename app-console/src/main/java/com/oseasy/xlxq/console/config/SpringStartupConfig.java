package com.oseasy.xlxq.console.config;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Tandy on 2016/6/7.
 */
@ComponentScan(basePackages={"com.oseasy"})
@EnableWebMvc
@Configuration
@PropertySource("classpath:/config.properties")
@Import({ FrameworkCacheConfig.class,
        ServiceConfig.class, ServiceApiConfig.class
})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
public class SpringStartupConfig {
    @Bean
    public String getSystemId(){
        return "app.console";
    }
}
