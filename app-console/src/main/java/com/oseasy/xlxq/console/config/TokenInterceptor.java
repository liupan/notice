package com.oseasy.xlxq.console.config;

import com.oseasy.xlxq.console.util.StringUtil;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/9/24.
 */

public class TokenInterceptor extends HandlerInterceptorAdapter {

    private RedisCacheService redisCacheService;

    public TokenInterceptor(RedisCacheService redisCacheService) {
        this.redisCacheService = redisCacheService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        String url = request.getRequestURI();
        if (url != null && !url.contains("sys/login")) {
            String token = request.getParameter("token");
            Map map = new HashMap<>();
            if (StringUtil.isBlank(token)) {
                map.put("success", false);
                map.put("errorMsg", "请输入token");
                map.put("errorCode",20001);
                response.getWriter().println(JSONUtil.mapToJson(map));
                return false;
            } else if (StringUtil.isBlank(redisCacheService.get("notice_admin:login:token:" + token))) {
                map.put("success", false);
                map.put("errorMsg", "token已失效");
                map.put("errorCode",20001);
                response.getWriter().println(JSONUtil.mapToJson(map));
                return false;
            }

            if (!StringUtil.isBlank(redisCacheService.get("notice_admin:login:token:" + token))) {
                redisCacheService.set("notice_admin:login:token:" + token,redisCacheService.get("notice_admin:login:token:" + token),3600);
            }
        }
        return true;
    }

}
