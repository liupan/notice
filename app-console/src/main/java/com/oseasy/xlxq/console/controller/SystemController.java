package com.oseasy.xlxq.console.controller;

import com.oseasy.xlxq.console.util.Md5Util;
import com.oseasy.xlxq.console.util.StringUtil;
import com.oseasy.xlxq.service.api.sys.model.FeedBackInfo;
import com.oseasy.xlxq.service.api.sys.service.FeedBackService;
import com.oseasy.xlxq.service.api.user.model.AdminInfo;
import com.oseasy.xlxq.service.api.user.service.AdminInfoService;
import com.oseasy.xlxq.service.api.user.service.UserInfoService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.tools.ant.taskdefs.condition.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by Administrator on 2017/10/15.
 */
@RestController
@RequestMapping("sys")
public class SystemController {


    @Autowired
    private AdminInfoService adminInfoService;
    @Autowired
    private RedisCacheService redisCacheService;
    @Autowired
    private FeedBackService feedBackService;

    @RequestMapping("login")
    public RestResponse login(String username, String password, String enc) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(username)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("账号不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(password)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("密码不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(enc)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("随机串不能为空");
                return restResponse;
            }

            AdminInfo adminInfo = adminInfoService.getObjectByCondition("from AdminInfo bean where bean.username = ?", username);
            if (adminInfo == null) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("账号不存在");
                return restResponse;
            }

            if (!password.equals(Md5Util.code(adminInfo.getPassword() + enc, 32))) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("密码错误");
                return restResponse;
            }
            String token = UUIDGenerator.uuid();
            redisCacheService.set("notice_admin:login:token:" + token, adminInfo.getId().toString(), 3600);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(token);
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
        }
        return restResponse;
    }

    @RequestMapping("updatePwd")
    public RestResponse updatePwd(String newPassword, String oldPassword, HttpServletRequest request) {
        RestResponse restResponse = new RestResponse();
        if (StringUtil.isBlank(newPassword)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("新密码不能为空");
            return restResponse;
        }
        if (StringUtil.isBlank(oldPassword)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("旧密码不能为空");
            return restResponse;
        }
        String token = request.getParameter("token");
        String userId = redisCacheService.get("notice_admin:login:token:" + token);
        AdminInfo adminInfo = adminInfoService.getObjectByCondition("from AdminInfo bean where bean.id = ?", userId);
        if (adminInfo == null) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("token异常");
            return restResponse;
        }
        if (!oldPassword.equals(adminInfo.getPassword())) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("原始密码错误");
            return restResponse;
        }
        adminInfo.setPassword(newPassword);
        adminInfoService.update(adminInfo.getId(), adminInfo);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }

    @RequestMapping("findFeedBackList")
    public RestResponse findFeedBackList(Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            Page<FeedBackInfo> feedBackInfoPage = feedBackService.pageList("from FeedBackInfo where isDelete = 0", pageNo, pageSize);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(feedBackInfoPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restResponse;
    }

    @RequestMapping("deleteFeedBack")
    public RestResponse deleteFeedBack(Integer id) {
        RestResponse restResponse = new RestResponse();
        adminInfoService.executeSql("update FeedBackInfo set isDelete = 1 , deleteTime = SYSDATE() where id = " + id);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }
}
