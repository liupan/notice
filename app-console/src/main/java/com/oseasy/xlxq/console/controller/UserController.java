package com.oseasy.xlxq.console.controller;

import com.oseasy.xlxq.console.util.StringUtil;
import com.oseasy.xlxq.service.api.student.model.StudentParent;
import com.oseasy.xlxq.service.api.student.service.StudentParentService;
import com.oseasy.xlxq.service.api.user.model.UserInfo;
import com.oseasy.xlxq.service.api.user.service.UserInfoService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/17.
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private StudentParentService studentParentService;
    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping("findUserList")
    public RestResponse findParentList(String name, Integer studentId, Integer pageNo, Integer pageSize,Integer type) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from UserInfo  where isDelete = 0";
            List<Object> value = new ArrayList<>();
            if(!StringUtil.isBlank(type)){
                hql += " and type = ?";
                value.add(type);
            }
            if (!StringUtil.isBlank(name)) {
                hql += " and name like ? ";
                value.add("%" + name + "%");
            }
            if (!StringUtil.isBlank(studentId)) {
                List<StudentParent> list = studentParentService.getList("from StudentParent where isDelete = 0 and studentId = " + studentId);
                String ids = "";
                if (list != null && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        StudentParent sp = list.get(i);
                        if (i == 0) {
                            ids += sp.getParentId();
                        } else {
                            ids += "," + sp.getParentId();
                        }
                    }
                }
                if (StringUtil.isBlank(ids)) {
                    hql += " and id in (" + ids + ")";
                }
            }

            Page<UserInfo> page = userInfoService.getPageLists(hql, pageNo, pageSize, value);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(page);
        } catch ( Exception e){
            e.printStackTrace();
        }
        return restResponse;
    }

    @RequestMapping("deleteUser")
    public RestResponse deleteParent(Integer id) {
        RestResponse restResponse = new RestResponse();
        this.userInfoService.executeSql("update UserInfo set isDelete = 1, deleteTime = SYSDATE() where id = " + id);
        this.userInfoService.executeSql("update StudentParent set isDelete = 1, deleteTime = SYSDATE() where parentId = " + id);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }

}
