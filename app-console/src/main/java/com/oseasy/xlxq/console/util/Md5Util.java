package com.oseasy.xlxq.console.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Administrator on 2017/10/15.
 */
public class Md5Util {

    private static final char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public Md5Util() {
    }

    public static String encryption(String plainText) {
        String re_md5 = new String();

        try {
            MessageDigest e = MessageDigest.getInstance("MD5");
            e.update(plainText.getBytes());
            byte[] b = e.digest();
            StringBuffer buf = new StringBuffer("");

            for (int offset = 0; offset < b.length; ++offset) {
                int i = b[offset];
                if (i < 0) {
                    i += 256;
                }

                if (i < 16) {
                    buf.append("0");
                }

                buf.append(Integer.toHexString(i));
            }

            re_md5 = buf.toString();
        } catch (NoSuchAlgorithmException var7) {
            var7.printStackTrace();
        }

        return re_md5;
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < 16; ++i) {
            int t = bytes[i];
            if (t < 0) {
                t += 256;
            }

            sb.append(hexDigits[t >>> 4]);
            sb.append(hexDigits[t % 16]);
        }

        return sb.toString();
    }

    public static String md5(String input, int bit) throws Exception {
        return code(input, bit);
    }

    public static String code(String input, int bit) throws Exception {
        try {
            MessageDigest e = MessageDigest.getInstance(System.getProperty("MD5.algorithm", "MD5"));
            return bit == 16 ? bytesToHex(e.digest(input.getBytes("utf-8"))).substring(8, 24) : bytesToHex(e.digest(input.getBytes("utf-8")));
        } catch (NoSuchAlgorithmException var3) {
            var3.printStackTrace();
            throw new Exception("Could not found MD5 algorithm.", var3);
        }
    }

    public static String md5_3(String b) throws Exception {
        MessageDigest md = MessageDigest.getInstance(System.getProperty("MD5.algorithm", "MD5"));
        byte[] a = md.digest(b.getBytes());
        a = md.digest(a);
        a = md.digest(a);
        return bytesToHex(a);
    }

}
