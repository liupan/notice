package com.oseasy.xlxq.console.controller;

import com.oseasy.xlxq.console.util.StringUtil;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;
import com.oseasy.xlxq.service.api.classinfo.service.ClassService;
import com.oseasy.xlxq.service.api.exceptions.PageSizeTooLargeException;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.model.StudentParent;
import com.oseasy.xlxq.service.api.student.service.StudentParentService;
import com.oseasy.xlxq.service.api.student.service.StudentService;
import com.oseasy.xlxq.service.api.user.model.UserInfo;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/16.
 */
@RestController
@RequestMapping("classinfo")
public class ClassController {

    @Autowired
    private ClassService classService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentParentService studentParentService;

    @RequestMapping("findClassList")
    public RestResponse findClassList(String name, String classmaster, String code, Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from ClassInfo where isDelete = 0 ";
            List<Object> value = new ArrayList<>();
            if (!StringUtil.isBlank(name)) {
                hql += " and name like ? ";
                value.add("%" + name + "%");
            }
            if (!StringUtil.isBlank(classmaster)) {
                hql += " and classmaster like ?";
                value.add("%" + classmaster + "%");
            }
            if (!StringUtil.isBlank(code)) {
                hql += " and code = ?";
                value.add("%" + code + "%");
            }
            Page<ClassInfo> page = classService.getPageLists(hql, pageNo, pageSize, value);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(page);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restResponse;
    }

    @RequestMapping("deleteClass")
    public RestResponse deleteClassInfo(Integer id) {
        RestResponse restResponse = new RestResponse();
        this.classService.executeSql("update ClassInfo set isDelete = 1 , deleteTime = "+new Date()+" where id = " + id);
        this.classService.executeSql("update Student set isDelete = 1 , deleteTime = "+new Date()+" where classId = " + id);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }

    @RequestMapping("findStudentList")
    public RestResponse findStudentList(String name, String classId, Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from Student where isDelete = 0 ";
            List<Object> value = new ArrayList<>();
            if (!StringUtil.isBlank(name)) {
                hql += " and studentName like ? ";
                value.add("%" + name + "%");
            }
            if (!StringUtil.isBlank(classId)) {
                hql += " and classId = ?";
                value.add(classId);
            }

            Page<Student> page = studentService.getPageLists(hql, pageNo, pageSize, value);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(page);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restResponse;
    }

    @RequestMapping("deleteStudent")
    public RestResponse deleteStudent(Integer id) {
        RestResponse restResponse = new RestResponse();
        this.classService.executeSql("update Student set isDelete = 1, deleteTime = "+new Date()+" where id = " + id);
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }

}
