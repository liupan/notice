package com.oseasy.xlxq.sms.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.sms.FrameworkSmsConfig;
import com.oseasy.xlxq.service.sms.service.SmsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value={FrameworkSmsConfig.class,ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(SmsTest.class)
public class SmsTest {

    @Autowired
    private SmsService smsService;

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location","classpath:"+ Constants.DEFAULT_CONFIG_FILE);
    }


    @Test
    public void test001(){
        Map<String,String> params = new HashMap<>();
//        smsReceiverParams.setParam("code", "3212");
//        smsReceiverParams.setParam("product", "乡丽乡亲");
        params.put("code","3212");
        params.put("product","乡丽乡亲");
        boolean result = smsService.sendsmsByTemplate("13971068693","SMS_65070087",params);
        Assert.assertTrue(result);
    }
}
