package com.oseasy.xlxq.service.sms.service;

import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.sms.clients.SMSClientFactory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Tandy on 2016/6/29.
 * 生产环境使用
 * 生成环境，先短信网关发短信，然后异步执行入库处理
 * 测试环境为保证功能正确性,可使用网关进行发送
 */

//@Profile({"production","oseasy"})
@Component
public class SmsServiceImplProduction  extends AbstractSmsServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(SmsServiceImplProduction.class);


    @Autowired
    private AsyncSmsSaveTask asyncSmsSaveTask;

    @Autowired
    private SMSClientFactory smsClientFactory;

    @Autowired
    RedisCacheService redisCacheService;


    public RedisCacheService getRedisCacheService(){
        return this.redisCacheService;
    }


    @Override
    public boolean sendsms(String to, String content) {
        if(logger.isDebugEnabled()){
            logger.debug("发送短信给：{}",to);
            logger.debug("内容:{}",content);
        }
        SMSSendLog log =  smsClientFactory.getSMSClient().sendsms(to,content);

        if(logger.isDebugEnabled()){
            logger.debug("发送结果:{}",log.getStatus());
        }

        if(log.getStatus() != 0){
            asyncSmsSaveTask.saveToDB(log);
            return true;
        }else{
            logger.error("短信发送失败，发送对象["+to+"]发送内容["+content+"]");
            return false;
        }
    }

    @Override
    public boolean sendsmsByTemplate(String to, String template, Map<String,String> params) {
        if(logger.isDebugEnabled()){
            logger.debug("发送短信给：{}",to);
            logger.debug("模板编号：{}",template);
            logger.debug("短信参数：{}",params);
        }
        SMSSendLog log  =  smsClientFactory.getSMSClient().sendsmsByTemplate(to,template,params);

        if(logger.isDebugEnabled()){
            logger.debug("发送结果:{}",log.getBid());
        }
        if(StringUtils.isNotEmpty(log.getBid())){
            //如果短信发送成功就异步存到数据库
            asyncSmsSaveTask.saveToDB(log);
            return true;
        }else{
            logger.error("短信发送失败，发送对象["+to+"]发送内容["+""+"]");
            return false;
        }
    }
}
