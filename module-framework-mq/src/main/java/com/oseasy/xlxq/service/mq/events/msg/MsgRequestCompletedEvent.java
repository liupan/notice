package com.oseasy.xlxq.service.mq.events.msg;

import com.oseasy.xlxq.service.mq.api.AbstractMQEvent;
import com.oseasy.xlxq.service.mq.topic.MQTopicConstants;

/**
 * Created by liups on 2017/3/23.
 */
public class MsgRequestCompletedEvent extends AbstractMQEvent {

    private String msgKey;

    @Override
    public String getTopicName() {
        return MQTopicConstants.TOPIC_APP_OC;
    }

    public MsgRequestCompletedEvent() {
    }

    public MsgRequestCompletedEvent(String msgKey) {
        this.msgKey = msgKey;
    }

    public String getMsgKey() {
        return msgKey;
    }

    public void setMsgKey(String msgKey) {
        this.msgKey = msgKey;
    }
}
