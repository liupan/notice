package com.oseasy.xlxq.service.mq.ons;

import com.oseasy.xlxq.service.mq.api.MQEvent;
import com.oseasy.xlxq.service.mq.api.MQMessageParser;
import com.oseasy.xlxq.service.mq.exceptions.InvalidMQEventMessageException;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import net.sf.json.JSONObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Created by Tandy on 2016/7/23.
 */
@Component
@ConditionalOnProperty(value = "global.mq.provider", havingValue = "ons", matchIfMissing = false)
public class OnsMessageParser implements MQMessageParser {
    @Override
    public MQEvent parse(String message) throws InvalidMQEventMessageException {

		JSONObject jsonObject = null;
		try{
			jsonObject = JSONObject.fromObject(message);
		}catch(Exception ex){
			throw new InvalidMQEventMessageException(ex);
		}
		Assert.notNull(jsonObject,"无效的json 消息字符串");
		String eventClassName = jsonObject.getString("eventName");
		Assert.notNull(eventClassName,"无效的事件消息体："+message);
		MQEvent result = null;
		try {
			FrameworkCacheConfig.logger.debug("实例化事件对象："+eventClassName);
			Class clazz = Class.forName(eventClassName);
			Object obj = JSONObject.toBean(jsonObject, clazz);
			FrameworkCacheConfig.logger.debug("事件对象赋值："+jsonObject);

			if(obj instanceof MQEvent){
				result = (MQEvent) obj;
				FrameworkCacheConfig.logger.debug("解析出事件类:"+result.getEventName());
			}
		} catch (ClassNotFoundException | SecurityException e) {
			throw new InvalidMQEventMessageException(e);
		}
		return result;

    }
}
