package com.oseasy.xlxq.service.mq.events.callcenter;

import com.oseasy.xlxq.service.mq.api.AbstractMQEvent;
import com.oseasy.xlxq.service.mq.topic.MQTopicConstants;

/**
 * Created by liuws on 2016/11/9.
 */
public class ConversationCompletedEvent extends AbstractMQEvent {

    private String conversationId;

    public ConversationCompletedEvent(){
    }

    public ConversationCompletedEvent(String conversationId){
        this.conversationId = conversationId;
    }

    @Override
    public String getTopicName() {
        return MQTopicConstants.TOPIC_CALL_CENTER;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

}
