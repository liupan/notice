package com.oseasy.xlxq.service.mq.api;

import javax.jms.JMSException;

/**
 * Created by Tandy on 2016/7/23.
 */

public interface MQMessageHandler<T extends MQEvent> {
    public abstract void handleMessage(T message) throws JMSException;
}
