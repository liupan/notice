package com.oseasy.xlxq.service.mq.events.apigw.test;

import com.oseasy.xlxq.service.mq.api.AbstractMQEvent;
import com.oseasy.xlxq.service.mq.topic.MQTopicConstants;

/**
 * Created by tandy on 16/8/10.
 */
public class TestResetStasticsCountEvent extends AbstractMQEvent {

    @Override
    public String getTopicName() {
        return MQTopicConstants.TOPIC_TEST;
    }
}
