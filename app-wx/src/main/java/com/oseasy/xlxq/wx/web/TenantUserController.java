package com.oseasy.xlxq.wx.web;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.order.model.Cart;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.model.OrderRefund;
import com.oseasy.xlxq.service.api.order.service.*;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.api.tenant.model.*;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import com.oseasy.xlxq.wx.utils.WebserviceUtil;
import com.sun.net.httpserver.Authenticator;
import org.apache.commons.collections.map.HashedMap;
import org.apache.tools.ant.taskdefs.condition.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by cannan on 2017/5/23.
 */
@RestController
public class TenantUserController {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    public static final String  USER_TO_REFUND_TYPE = "20";//用户进入我的售后时用的判断标志

    @Autowired
    private TenantUserService tenantUserService;
    @Autowired
    private TenantUserWxExtService tenantUserWxExtService;
    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private OrderProductService orderProductService;
    @Autowired
    private OrderNoGenService orderNoGenService;
    @Autowired
    private OrderRefundService orderRefundService;
    @Autowired
    private CartService cartService;
    @Autowired
    private RedisCacheService redisCacheService;


    //===========================页面跳转============================
    //跳转到用户首页
    @RequestMapping("/user/index")
    public ModelAndView touserIndex (String code){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            Map<String, Object> userinfo= WebserviceUtil.giveuserinfo(code);
            System.out.println("微信用户信息:"+userinfo);
            //                获取测试
//                Map<String, Object> userinfo = new HashedMap();
//                userinfo.put("openid","oz6g2wCNf7XO8UuuROvhIPAEzqrY");
//                userinfo.put("nickname","流火");
//                userinfo.put("sex","1");
//                userinfo.put("headimgurl","http://wx.qlogo.cn/mmopen/PiajxSqBRaEJzsIonE2pveSGwiaEU1V8Vr6ibARfmo46hyH6IIY91SsloBdooECUxjOiclBtv1gQpWLibiabV0iarCVSg/0");


            TenantUserWxExt tenantUserWxExt = new TenantUserWxExt();
            TenantUser tenantUser = new TenantUser();

            String hql = "from TenantUserWxExt obj ";
            hql= HqlUtil.addCondition(hql,"wxOpenid",userinfo.get("openid").toString());
            tenantUserWxExt = tenantUserWxExtService.findUnique(hql);
            if(null  == tenantUserWxExt){
                tenantUser.setName(userinfo.get("nickname").toString());
                tenantUser = tenantUserService.save(tenantUser);
                SessionUtils.put(Constant.USER_ID,tenantUser.getId());
                tenantUserWxExt = new TenantUserWxExt();
                tenantUserWxExt.setWxOpenid(userinfo.get("openid").toString());
                tenantUserWxExt.setWxNickName(userinfo.get("nickname").toString());
                tenantUserWxExt.setSexFlag(Integer.parseInt(userinfo.get("sex").toString()));
                tenantUserWxExt.setWxheadUrl(userinfo.get("headimgurl").toString());
//                    tenantUserWxExt.setWxUnionid(userinfo.get("unionid").toString());
                tenantUserWxExt.setTenantUser(tenantUser);
                tenantUserWxExtService.save(tenantUserWxExt);
            }

            SessionUtils.put(Constant.USER_ID,tenantUserWxExt.getTenantUser().getId());
            SessionUtils.put(Constant.OPEN_ID, userinfo.get("openid"));


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/user/user_index");
        return mav;
    }

    //跳转到查看订单
    @RequestMapping("/user/toorderlist")
    public ModelAndView toorderlist (String ordertype){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            //ordertype: "":全部  ； 1:待支付    2:待发货    3:待收货     7:待评价   20：去我的售后
            SessionUtils.put(Constant.USER_MY_ORDER_TYPE, ordertype);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/user/user_all_orderlist");
        return mav;
    }


    //跳转到查看物流页面
    @RequestMapping("/user/togetlogistic")
    public ModelAndView togetlogistic (String orderId){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

            SessionUtils.put(Constant.USER_MY_ORDER_ID, orderId);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/user/user_order_logistics");
        return mav;
    }

    //跳转到申请退货页面
    @RequestMapping("/user/toaftersale")
    public ModelAndView toaftersale (String orderId){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

            SessionUtils.put(Constant.USER_MY_ORDER_ID, orderId);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/user/user_order_aftersale");
        return mav;
    }

    //跳转到编辑个人信息
    @RequestMapping("/user/edituserinfo")
    public ModelAndView edituserinfo (){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/user/user_info_edit");
        return mav;
    }

    //跳转到我的购物车
    @RequestMapping("/user/tomycart")
    public ModelAndView tomycart (){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/user/user_my_cart");
        return mav;
    }

    //跳转到我的反馈
    @RequestMapping("/user/tomyfeedback")
    public ModelAndView tomyfeedback (){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();
        mav.addObject("wxjsticket", redisCacheService.get("wxjsticket"));
        mav.setViewName("/user/user_my_feedback");
        return mav;
    }



    //===========================数据处理============================
    //绑定手机号获取验证码
    @RequestMapping("/userbind/getidentify")
    public String getidentify(){
        String result = "";
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            SessionUtils.put(Constant.USER_MY_INFO_EDIT_CODE,"1112");
            result = "success";

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);
            result = "false";
        }
        return result;
    }

    //校验验证码
    @RequestMapping("/user/checkcode")
    public String checkcode(String code ){
        String result = "";
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String realcode = SessionUtils.get(Constant.USER_MY_INFO_EDIT_CODE) + "";
            if(code.equals(realcode)){
                result = "success";
            }else {
                result = "wrongcode";
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);
            result = "false";
        }
        return result;
    }

    //获取用户信息
    @RequestMapping("/userinfo/getuserinfo")
    public RestResponse<TenantUserWxExt> getuserinfo(){
        RestResponse<TenantUserWxExt> response = new RestResponse<TenantUserWxExt>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
           String userid = SessionUtils.get(Constant.USER_ID).toString();
//            String userid = "4028d70a5c477e3c015c4782c38d0004";
            String hql = "from TenantUserWxExt obj ";
            hql = HqlUtil.addCondition(hql,"tenantUser.id",SessionUtils.get(Constant.USER_ID));
           TenantUserWxExt tenantUserWxExt= tenantUserWxExtService.findUnique(hql);
           if(tenantUserWxExt != null){
               if(StringUtil.isNotBlank(tenantUserWxExt.getTenantUser().getTelephone())){
                   response.setSuccess(true);
                   response.setData(tenantUserWxExt);
               }else {
                   response.setErrorMsg("请绑定手机");
               }
           }

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }
        return response;
    }


    //获取订单类型数量信息（待支付数量，代发货数量。。）
    @RequestMapping("/usermy/getallOrderinfo")
    public RestResponse<OrderInfo> getallOrderinfo(){
        RestResponse<OrderInfo> response = new RestResponse<OrderInfo>();
        List<OrderInfo> orderInfoList = new ArrayList<OrderInfo>();
        OrderInfo orderInfo = new OrderInfo();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String hql = "from OrderInfo obj ";
            hql = HqlUtil.addCondition(hql,"tenantUser.id",SessionUtils.get(Constant.USER_ID));
            hql= HqlUtil.addCondition(hql,"orderStatus","1");
            orderInfoList = orderInfoService.getList(hql);
            orderInfo.setPendpaynum(orderInfoList.size()+"");
            String hql2 = "from OrderInfo obj ";
            hql2 = HqlUtil.addCondition(hql2,"tenantUser.id",SessionUtils.get(Constant.USER_ID));
            hql2 = HqlUtil.addCondition(hql2,"orderStatus","2");
            List<OrderInfo> orderInfoList2 = orderInfoService.getList(hql2);
            orderInfo.setShippendnum(orderInfoList2.size()+"");
            String hql3 = "from OrderInfo obj ";
            hql3 = HqlUtil.addCondition(hql3,"tenantUser.id",SessionUtils.get(Constant.USER_ID));
            hql3 = HqlUtil.addCondition(hql3,"orderStatus","3");
            List<OrderInfo>  orderInfoList3 = orderInfoService.getList(hql3);
            orderInfo.setWaitrecnum(orderInfoList3.size()+"");
            String hql4 = "from OrderInfo obj ";
            hql4 = HqlUtil.addCondition(hql4,"tenantUser.id",SessionUtils.get(Constant.USER_ID));
            hql4= HqlUtil.addCondition(hql4,"orderStatus","7");
            List<OrderInfo> orderInfoList4 = orderInfoService.getList(hql4);
            orderInfo.setPendevalunum(orderInfoList4.size()+"");
            response.setSuccess(true);
            response.setData(orderInfo);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }
        return response;
    }


    //根据用户id获取全部订单信息
    @RequestMapping("/usermy/getallorder")
    public RestResponse<List<JSONObject>> getallorder(@RequestParam(defaultValue = "1") Integer pageno ,
                                                     @RequestParam(defaultValue = "10") Integer pagesize) {
        RestResponse<List<JSONObject>> response = new RestResponse<List<JSONObject>>();
        try {
            String id = SessionUtils.get(Constant.USER_ID) + "";
            String ordertype = SessionUtils.get(Constant.USER_MY_ORDER_TYPE) + "";
            if (!StringUtils.isEmpty(id)) {
                String hql = "from OrderInfo obj ";
                hql = HqlUtil.addCondition(hql, "tenantUser.id", id);
                if(StringUtils.isEmpty(ordertype)){

                }else if(ordertype.equals("1")){
                    hql = HqlUtil.addCondition(hql, "orderStatus", "1");
                }else if(ordertype.equals("2")){
                    hql = HqlUtil.addCondition(hql, "orderStatus", "2");
                }else if(ordertype.equals("3")){
                    hql = HqlUtil.addCondition(hql, "orderStatus", "3");
                }else if(ordertype.equals("7")){
                    hql = HqlUtil.addCondition(hql, "orderStatus", "7");
                }else if(ordertype.equals("20")){
                    hql += "and ( obj.orderStatus = '11'  or obj.orderStatus = '12'or obj.orderStatus = '13'or obj.orderStatus = '14') ";
//                    hql = HqlUtil.addCondition(hql, "orderStatus", "11", HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_EQ);
//                    hql = HqlUtil.addCondition(hql, "orderStatus", "12", HqlUtil.LOGIC_OR, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_EQ);
//                    hql = HqlUtil.addCondition(hql, "orderStatus", "13", HqlUtil.LOGIC_OR, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_EQ);
//                    hql = HqlUtil.addCondition(hql, "orderStatus", "14", HqlUtil.LOGIC_OR, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_EQ);
                }

                hql = HqlUtil.addOrder(hql,"createTime","desc");
                Page<OrderInfo> orderlist = orderInfoService.pageList(hql,pageno,pagesize);
                List<JSONObject> datalist = new LinkedList<JSONObject>();
                JSONObject data = new JSONObject();
                String hql2 = "from OrderProduct obj ";
                List<OrderProduct> orderProductList = new LinkedList<OrderProduct>();
                for(int i =0; i<orderlist.getResult().size(); i++){
                    data = JSONObject.parseObject(JSONObject.toJSONString(orderlist.getResult().get(i)));
                    hql2 = HqlUtil.addCondition(hql2, "orderInfo.id", orderlist.getResult().get(i).getId());
                    orderProductList = orderProductService.getList(hql2);
                    data.put("orderProductList",orderProductList);
                    datalist.add(data);
                }

                response.setSuccess(true);
                response.setData(datalist);




            } else {
                response.setSuccess(false);

            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            response.setSuccess(false);

        }
        return response;
    }



    //保存用户信息
    @RequestMapping("/user/saveuserinfo")
    public RestResponse<TenantUser> saveuserinfo(String name,int sexFlag,String birthday, String telephone){
        RestResponse<TenantUser> response = new RestResponse<TenantUser>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd ");
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String userid = SessionUtils.get(Constant.USER_ID).toString();
            TenantUser tenantUser = new TenantUser();
            String bt1[] = birthday.split("年");
            String bt2[] = bt1[1].split("月");
            String bt3[] = bt2[1].split("日");
            birthday = bt1[0] + "-" + bt2[0] + "-" + bt3[0];
            Date birthdayDate = sdf.parse(birthday);
            tenantUser.setId(userid);
            tenantUser.setName(name);
            tenantUser.setSexFlag(sexFlag);
            tenantUser.setBirthday(birthdayDate);
            tenantUser.setTelephone(telephone);
            tenantUserService.update(userid,tenantUser);
            response.setSuccess(true);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);
            response.setSuccess(false);

        }
        return response;
    }

    //保存用户退款信息
    @RequestMapping("/user/saverefund")
    public RestResponse<TenantUser> saverefund(String refundType,String refundreason){
        RestResponse<TenantUser> response = new RestResponse<TenantUser>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            Date date=new Date();
            DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String time=format.format(date);

            String orderid = SessionUtils.get(Constant.USER_MY_ORDER_ID).toString();

            OrderRefund orderRefund =new OrderRefund();
            OrderInfo orderInfo = orderInfoService.findById(orderid);
            String tknum = orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_TK);
            orderRefund.setOrderInfo(orderInfo);
            orderRefund.setRefundId(tknum);
            orderRefund.setRefundMoney(orderInfo.getOrderSum());
            orderRefund.setRefundTime(format.parse(time));
            orderRefund.setRefundStatus("11");
            orderRefund.setRefundType(refundType);
            orderRefund.setRefundReason(refundreason);
            orderRefundService.save(orderRefund);
            orderInfo.setOrderStatus("11");
            orderInfoService.update(orderid,orderInfo);
            response.setSuccess(true);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);
            response.setSuccess(false);

        }
        return response;
    }


    //获取购物车产品信息
    @RequestMapping("/usermy/getcartproinfo")
    public RestResponse<List<Cart>> getcartproinfo(){
        RestResponse<List<Cart>> response = new RestResponse<List<Cart>>();

        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String userid = SessionUtils.get(Constant.USER_ID) + "";
            String hql = "from Cart obj ";
            hql = HqlUtil.addCondition(hql,"tenantUser.id",userid);
            hql = HqlUtil.addCondition(hql,"payStatus", "0");
            List<Cart> cartslist = cartService.getList(hql);

            response.setSuccess(true);
            response.setData(cartslist);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }
        return response;
    }

    //确认购买
//    @RequestMapping("/user/submitcart")
//    public String submitcart (JSONObject productinfo){
//        String result = "";
//        try {
//            if(logger.isDebugEnabled()){
//                logger.debug("debugger");
//            }
//            List<JSONObject> merchantlist = (List<JSONObject>) productinfo.get("merchantlist");
//            TenantMerchant tenantMerchant = new TenantMerchant();
//            TenantUserAddrs tenantUserAddrs = new TenantUserAddrs();
//            TenantProduct tenantProduct = new TenantProduct();
//            TenantSpecialty tenantSpecialty = new TenantSpecialty();
//            TenantUser tenantUser = new TenantUser();
//            OrderProduct orderProduct = new OrderProduct();
//            OrderInfo orderInfo = new OrderInfo();
//            String ordernum = orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK);
//
//            String hql = "from TenantProduct obj ";
//            hql= HqlUtil.addCondition(hql,"id",SessionUtils.get(Constant.SPE_ID).toString());
//            tenantProduct = tenantProductService.findUnique(hql);
//            tenantUserAddrs.setId(addressId);
//            tenantUser.setId(SessionUtils.get(Constant.USER_ID).toString());
//            tenantMerchant.setId(tenantProduct.getTenantMerchant().getId());
//            orderInfo.setTenantUserAddrs(tenantUserAddrs);
//            orderInfo.setTenantUser(tenantUser);
//            orderInfo.setTenantMerchant(tenantMerchant);
//            orderInfo.setOrderNum(ordernum);
//            BigDecimal fprice = tenantProduct.getFavorablePrice();
//            BigDecimal buynumber = new BigDecimal(buynum);
//            BigDecimal freight = new BigDecimal(0);
//            if(tenantSpecialty instanceof TenantProduct){
//                tenantSpecialty = (TenantSpecialty) tenantProduct;
//                freight = tenantSpecialty.getFreight();
//            }
//            if(freight == null){
//                freight = new BigDecimal(0);
//            }
//            fprice = fprice.multiply(buynumber);
//            freight = fprice.add(freight);
//            orderInfo.setOrderSum(freight);
//            orderInfo.setOrderType("1");
//            orderInfo.setOrderStatus("1");
//            //保存进订单信息
//            orderInfo = orderInfoService.save(orderInfo);
//
//            //订单产品信息
//
//            orderProduct.setTenantProduct(tenantProduct);
//            orderProduct.setTenantMerchant(tenantMerchant);
//            orderProduct.setOrderInfo(orderInfo);
//            orderProduct.setProductPrice(tenantProduct.getProductUnit());
//            orderProduct.setPurchaseNum(buynum);
//            orderProduct.setFavorablePrice(tenantProduct.getFavorablePrice());
//            orderProduct = orderProductService.save(orderProduct);
//            if(StringUtil.isNotBlank(orderInfo.getId().toString()) && StringUtil.isNotBlank(orderProduct.getId().toString())){
//                result = "success";
//            }else {
//                result = "falls";
//            }
//
//
//        }catch(Exception ex) {
//            logger.error("error is appreare",ex);
//
//        }
//
//
//
//        return result;
//    }
}
