package com.oseasy.xlxq.wx.config;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.monitor.FrameworkMonitorConfig;
import com.oseasy.xlxq.service.mq.FrameworkMQConfig;
import com.oseasy.xlxq.wx.security.SpringSecurityConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Tandy on 2016/6/7.
 */
@ComponentScan({"com.oseasy.xlxq.wx"})
@EnableWebMvc
@Configuration
@PropertySource("classpath:/config.properties")
@Import({SpringSecurityConfig.class, RedisAutoConfiguration.class,
        FrameworkCacheConfig.class, FrameworkMonitorConfig.class,
        ServiceConfig.class, ServiceApiConfig.class,
        FrameworkMQConfig.class
})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
public class SpringStartupConfig {
    @Bean
    public String getSystemId(){
        return "app.wx";
    }
}
