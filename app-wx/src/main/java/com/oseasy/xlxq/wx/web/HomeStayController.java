package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.service.api.order.model.HomeStayRoomStock;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.service.HomeStayRoomStockService;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderNoGenService;
import com.oseasy.xlxq.service.api.order.service.OrderProductService;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.tenant.model.*;
import com.oseasy.xlxq.service.api.tenant.service.TenantHomeStayRoomService;
import com.oseasy.xlxq.service.api.tenant.service.TenantHomeStayService;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.mq.kafka.KafkaConsumer;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import org.apache.poi.util.Internal;
import org.jasypt.commons.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by cannan on 2017/5/31.
 */
@RestController
public class HomeStayController {
    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private TenantHomeStayService tenantHomeStayService;
    @Autowired
    private TenantHomeStayRoomService tenantHomeStayRoomService;
    @Autowired
    private HomeStayRoomStockService homeStayRoomStockService;
    @Autowired
    private TenantUserService tenantUserService;
    @Autowired
    private OrderNoGenService orderNoGenService;
    @Autowired
    private TenantProductService tenantProductService;
    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private OrderProductService orderProductService;

    //=================页面跳转==================

    //跳转到民宿列表
    @RequestMapping("/product/homestayindex")
    public ModelAndView hosindex (HttpServletRequest request){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
//            String code = request.getAttribute("code").toString();

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/homestay/homeStayList");
        return mav;
    }

    //跳转到民宿详情
    @RequestMapping("/product/homestaydetail")
    public ModelAndView tohomestay (String id){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            SessionUtils.put(Constant.HMS_ID,id);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/homestay/homestay_detail");
        return mav;
    }

    //跳转到订单确认
    @RequestMapping("/homestay/homestayok")
    public ModelAndView homestayok (String roomid,String buynum){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
        SessionUtils.put(Constant.HMSR_BUY_ID,roomid);
        SessionUtils.put(Constant.HMSR_BUY_NUM,buynum);
        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/homestay/homestay_ok");
        return mav;
    }



    //===========================数据处理============================

    //获取民宿列表
    @RequestMapping( "product/homelistbysearch")
    public RestResponse<Page<TenantHomeStay>> homestaylist(
            @RequestParam(defaultValue = "1") Integer pageno ,
            @RequestParam(defaultValue = "10") Integer pagesize,
            String searchtypep,String sorttype
    ){
        RestResponse<Page<TenantHomeStay>> response = new RestResponse<Page<TenantHomeStay>>();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            String hql = "";
            Page<TenantHomeStay> pagelist = new Page<TenantHomeStay>();
            if(CommonUtils.isNotEmpty(searchtypep)  ){
                hql = "from TenantHomeStay obj ";
                hql= HqlUtil.addCondition(hql,"tenantScenic.id",searchtypep);
                if(sorttype.equals("1")){
                    hql=HqlUtil.addOrder(hql,"startPrice","desc");
                }else if(sorttype.equals("2")){
                    hql=HqlUtil.addOrder(hql,"startPrice","");
                }else if(sorttype.equals("3")){
                    hql=HqlUtil.addOrder(hql,"salesVolume","desc");
                }else if(sorttype.equals("4")){
                    hql=HqlUtil.addOrder(hql,"salesVolume","");
                }
                pagelist = tenantHomeStayService.pageList(hql,pageno,pagesize);
            }else{
                if(CommonUtils.isEmpty(sorttype)){
                    pagelist = tenantHomeStayService.pageList(pageno,pagesize);
                }else{
                    if(sorttype.equals("1")){
                        hql = "from TenantHomeStay obj order by obj.startPrice desc";
                    }else if(sorttype.equals("2")){
                        hql = "from TenantHomeStay obj order by obj.startPrice asc";
                    }else if(sorttype.equals("3")){
                        hql = "from TenantHomeStay obj order by obj.salesVolume desc";
                    }else if(sorttype.equals("4")){
                        hql = "from TenantHomeStay obj order by obj.salesVolume asc";
                    }
                    pagelist = tenantHomeStayService.pageList(hql,pageno,pagesize);

                }

            }

            response.setSuccess(true);
            response.setData(pagelist);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }

        return response;
    }


    //获取民宿详情
    @RequestMapping("/product/homestaydetails")
    public RestResponse<TenantHomeStay> homeStaydetails(){
        RestResponse<TenantHomeStay> response = new RestResponse<TenantHomeStay>();
        String id = SessionUtils.get(Constant.HMS_ID).toString();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantHomeStay specialty =  tenantHomeStayService.findById(id);
            response.setSuccess(true);
            response.setData(specialty);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //获取民宿房型详情列表
    @RequestMapping("/homestay/roominfo")
    public RestResponse<List<TenantHomeStayRoom>> roominfo(String starttime,String  endtime){
        RestResponse<List<TenantHomeStayRoom>> response = new RestResponse<List<TenantHomeStayRoom>>();
        List<TenantHomeStayRoom> tenantHomeStayRoomList = new ArrayList<TenantHomeStayRoom>();
        String homeid = SessionUtils.get(Constant.HMS_ID).toString();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            Calendar a=Calendar.getInstance();
            String year = a.get(Calendar.YEAR) + "";
            starttime = new String(starttime.getBytes("iso8859-1"),"UTF-8");
            endtime = new String(endtime.getBytes("iso8859-1"),"UTF-8");
            SessionUtils.put(Constant.HM_STARTTIME,starttime);
            SessionUtils.put(Constant.HM_ENDTIME,endtime);
            String st[] = starttime.split("月");
            String st2[] = st[1].split("日");
            starttime = year + "-" + st[0] + "-" + st2[0];
            String et[] = endtime.split("月");
            String et2[] = et[1].split("日");
            endtime = year + "-" + et[0] + "-" + et2[0];
            String hql = "from TenantHomeStayRoom obj";
            hql= HqlUtil.addCondition(hql,"tenantHomestay.id",homeid);
            tenantHomeStayRoomList = tenantHomeStayRoomService.getList(hql);
            String hql2 = "from HomeStayRoomStock obj";
            hql2 = HqlUtil.addBetweenCondition(hql2,"orderTime",starttime,endtime);
            for(int i =0;i<tenantHomeStayRoomList.size();i++){
                List<HomeStayRoomStock> homeStayRoomStock = new ArrayList<HomeStayRoomStock>();
                hql2 = HqlUtil.addCondition(hql2,"tenantHomeStayRoom.id",tenantHomeStayRoomList.get(i).getId());
                homeStayRoomStock = (List<HomeStayRoomStock>) homeStayRoomStockService.findUnique(hql2);
                tenantHomeStayRoomList.get(i).setIforder("0");

                if(homeStayRoomStock != null){
                    for(int j = 0; j<homeStayRoomStock.size();j++){
                        if(homeStayRoomStock.get(j).getLeftNum().equals("0") ){
                            tenantHomeStayRoomList.get(i).setIforder("1");
                            break;
                        }else {
                            tenantHomeStayRoomList.get(i).setIforder("0");
                        }

                    }
                }


            }

            response.setSuccess(true);
            response.setData(tenantHomeStayRoomList);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //获取民宿房型详情
    @RequestMapping("/homestay/roomdetails")
    public RestResponse<TenantHomeStayRoom> roomdetails(String roomid){
        RestResponse<TenantHomeStayRoom> response = new RestResponse<TenantHomeStayRoom>();
        SessionUtils.put(Constant.HMSR_ID,roomid);
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantHomeStayRoom tenantHomeStayRoom =  tenantHomeStayRoomService.findById(roomid);
            response.setSuccess(true);
            response.setData(tenantHomeStayRoom);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //获取民宿房型详情(无入参)
    @RequestMapping("/homestay/homeroomdetails")
    public RestResponse<TenantHomeStayRoom> homeroomdetails(){
        RestResponse<TenantHomeStayRoom> response = new RestResponse<TenantHomeStayRoom>();
        String hmsrid = SessionUtils.get(Constant.HMSR_BUY_ID)+"";
        String hmsbuynum = SessionUtils.get(Constant.HMSR_BUY_NUM)+"";
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantHomeStayRoom tenantHomeStayRoom =  tenantHomeStayRoomService.findById(hmsrid);
            tenantHomeStayRoom.setBuyNum(hmsbuynum);
            tenantHomeStayRoom.setStartTime(SessionUtils.get(Constant.HM_STARTTIME).toString());
            tenantHomeStayRoom.setEndTime(SessionUtils.get(Constant.HM_ENDTIME).toString());
            response.setSuccess(true);
            response.setData(tenantHomeStayRoom);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }

        return response;
    }

    //保存民宿订单
    @RequestMapping("/homestay/homeroomsave")
    public String homeroomsave(String buyusername, String buyuserphone,String starttime,String endtime, String buynum){
        String result = "";
        String roomid = SessionUtils.get(Constant.HMSR_ID)+"";
        String userid = SessionUtils.get(Constant.USER_ID)+"";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
//        String userid = "4028d70a5c477e3c015c4782c38d0004";
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            Calendar a=Calendar.getInstance();
            String year = a.get(Calendar.YEAR) + "";
//            starttime = new String(starttime.getBytes("iso8859-1"),"UTF-8");
//            endtime = new String(endtime.getBytes("iso8859-1"),"UTF-8");
            String st[] = starttime.split("月");
            String st2[] = st[1].split("日");
            starttime = year + "-" + st[0] + "-" + st2[0];
            String et[] = endtime.split("月");
            String et2[] = et[1].split("日");
            endtime = year + "-" + et[0] + "-" + et2[0];
            Date startDate = sdf.parse(starttime);
            Date endDate = sdf.parse(endtime);
            TenantHomeStayRoom tenantHomeStayRoom =  tenantHomeStayRoomService.findById(roomid);
            TenantUser tenantUser = tenantUserService.findById(userid);

            //插入房型库存表
            int roomnum = Integer.parseInt(tenantHomeStayRoom.getRoomNum());
            int leftroom = roomnum - Integer.parseInt(buynum);
            HomeStayRoomStock homeStayRoomStock = new HomeStayRoomStock();
            homeStayRoomStock.setTenantHomeStayRoom(tenantHomeStayRoom);
            List<Date> orderdate = new ArrayList<Date>();
            String stockhql = "from HomeStayRoomStock obj ";
            orderdate = com.oseasy.xlxq.wx.utils.CommonUtils.getDatesBetweenTwoDate(startDate,endDate);
            for (int i = 0;i<orderdate.size();i++){
                stockhql = HqlUtil.addCondition(stockhql,"tenantHomeStayRoom.id",roomid);
                stockhql = HqlUtil.addCondition(stockhql,"orderTime",orderdate.get(i));
                HomeStayRoomStock homeStayRoomStockmore = homeStayRoomStockService.findUnique(stockhql);
                if(homeStayRoomStockmore != null){
                    String trueleftroom = Integer.parseInt(homeStayRoomStockmore.getLeftNum()) - Integer.parseInt(buynum) +"" ;
                    homeStayRoomStock.setLeftNum(trueleftroom);
                }else{
                    homeStayRoomStock.setLeftNum(leftroom+"");
                }
                homeStayRoomStock.setOrderTime(orderdate.get(i));
                homeStayRoomStockService.save(homeStayRoomStock);
            }



            TenantMerchant tenantMerchant = new TenantMerchant();
            TenantProduct tenantProduct = new TenantProduct();
            OrderProduct orderProduct = new OrderProduct();
            OrderInfo orderInfo = new OrderInfo();
            String ordernum = orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK);

            String hql = "from TenantProduct obj ";
            hql= HqlUtil.addCondition(hql,"id",roomid);
            tenantProduct = tenantProductService.findUnique(hql);
            tenantUser.setId(userid);
            tenantMerchant.setId(tenantProduct.getTenantMerchant().getId());
            orderInfo.setTenantUser(tenantUser);
            orderInfo.setTenantMerchant(tenantMerchant);
            orderInfo.setOrderNum(ordernum);
            BigDecimal fprice = tenantHomeStayRoom.getRoomSalprice();
            BigDecimal buynumber = new BigDecimal(buynum);
            fprice = fprice.multiply(buynumber);
            orderInfo.setOrderSum(fprice);
            orderInfo.setOrderType("2");
            orderInfo.setOrderStatus("1");
            orderInfo.setOrderUserName(buyusername);
            orderInfo.setOrderUserPhone(buyuserphone);
            //保存进订单信息
            orderInfo = orderInfoService.save(orderInfo);

            //订单产品信息

            orderProduct.setTenantProduct(tenantProduct);
            orderProduct.setTenantMerchant(tenantMerchant);
            orderProduct.setOrderInfo(orderInfo);
            orderProduct.setProductPrice(tenantProduct.getProductUnit());
            orderProduct.setPurchaseNum(Integer.parseInt(buynum));
            orderProduct.setFavorablePrice(tenantHomeStayRoom.getRoomSalprice());
            orderProduct = orderProductService.save(orderProduct);
            if(StringUtil.isNotBlank(orderInfo.getId().toString()) && StringUtil.isNotBlank(orderProduct.getId().toString())){
                result = "success";
            }else {
                result = "falls";
            }



        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return result;
    }
}
