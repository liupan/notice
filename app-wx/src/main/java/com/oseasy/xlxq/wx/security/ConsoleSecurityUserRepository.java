package com.oseasy.xlxq.wx.security;

import com.oseasy.xlxq.service.core.security.SecurityUser;
import com.oseasy.xlxq.service.web.security.SecurityUserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * Created by Tandy on 2016/6/8.
 */
@Component
public class ConsoleSecurityUserRepository implements SecurityUserRepository {

    private static  final Log logger = LogFactory.getLog(ConsoleSecurityUserRepository.class);

    @Override
    public SecurityUser loadSecurityUser(String token) {
        if(logger.isDebugEnabled()){
            logger.debug(String.format("load security user"));
        }
        return getUser(token);
    }

    /**
     * 根据用户名获取用户信息
     * @return
     */
    public SecurityUser getUser(String token){
        return null;
//        String uri = ConsoleConstants.REST_PREFIX_URL +  "/rest/account/get/current";
//        RestResponse<TenantAccount> restResponse = RestRequest.buildSecurityRequest(token).get(uri, TenantAccount.class);
//        TenantAccount account = restResponse.getData();
//        return new SecurityUser(account.getId(),account.getUserName(),account.getTenant().getCode(),account.getTenant().getId());
    }

}
