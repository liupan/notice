package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tandy on 17/5/8.
 */
@RestController
public class HaHaController {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);
    @RequestMapping("/open/haha")
    public String helloHaHa(String name){
        return "hello :" + name;
    }


    @RequestMapping("/open/test001")
    public RestResponse testRest(){
        RestResponse response = null;
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

            Tenant t = new Tenant();
            t.setName("haha");
             response = RestResponse.success(t);
             throw new Exception("aaaaaaaa");
        }catch(Exception ex) {
           logger.error("error is appreare",ex);
            response = RestResponse.failed("3330003","yichangle");
        }
        return response;
    }


}
