<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/7
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>全部订单</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body class="bg_fafafa" ontouchstart>
<ul class="stay" id="staypage"></ul>





<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script type="text/javascript">
    $(function() {
        getAllOrder();

        /*点击配送地址弹出地址详情*/
        $("body").on('click', '.manlageAdd', function() {
            if($(this).find(".addrbox").hasClass("addShow")) {
                $(this).find(".addrbox").removeClass("addShow");
            } else {
                $(".addrbox").removeClass("addShow");
                $(this).find(".addrbox").toggleClass("addShow");
            }
        });

        /*确认收货弹窗*/
        $(document).on("click", ".enterbtn", function() {
            var orderid =  $(this).attr("id");
            $.confirm( "","您确认收货吗?", function() {
                $.post("${cxt}/order/changeuserordertype?orderid="+orderid+"&ordertype=7",
                    function (data) {
                        if(data == "success"){
                            $.toast("确认成功");
                            var timer = setTimeout(function() {
                                location.reload();
                            }, 1000)
                        }
                    })

            }, function() {
                //取消操作
            });
        });
        /*取消订单是否取消*/
        $(document).on("click", ".deletebtn", function() {
            var orderid =  $(this).attr("id");
            $.confirm("", "是否取消订单?",function() {
                $.post("${cxt}/order/changeuserordertype?orderid="+orderid+"&ordertype=5",
                    function (data) {
                        if(data == "success"){
                            $.toast("订单已取消");
                            var timer = setTimeout(function() {
                                location.reload();
                            }, 1000)
                        }
                    })

            }, function() {
                //取消操作
            });
        });
    })


    //获取所有订单信息
        function  getAllOrder() {
            $.get("${ctx}/usermy/getallorder",
                function(data) {
                    if(data.success){
                        var html = "";
                        if(data.data.length>0){
                            for (var i=0;i<data.data.length;i++){
                                var totoalnum = 0 ;
                                var totoalfreight = 0 ;
                                html += '<li>';
                                <!--订单号-->
                                html += '<div class="weui-cells m-t-0">';
                                html += '<div class="weui-cell log">';
                                html += '<div class="weui-cell__bd weui-cell_primary">';
                                html += '<p class="recipient">';
                                html += ' <span>订单号：<em>'+data.data[i].orderNum+'</em></span>';
                                html += '<span class="fr">';
                                var time1 = new Date(data.data[i].createTime).Format("yyyy-MM-dd hh:mm:ss");
                                html += '时间：<em class="orderdate">'+time1+'</em>'
                                html += '</span></p> </div>';
                                html += '<div class="weui-cell__ft"> </div>';
                                html += ' </div></div>';

                                for(var j=0;j<data.data[i].orderProductList.length;j++){
                                    <!-- 产品详情-->
                                    html += '<div class="splist myorder">';
                                    html += '<div class="weui-panel_access">';
                                    html += ' <p class="title_flower"><i></i>'+data.data[i].orderProductList[j].tenantProduct.tenantMerchant.merchantName;
                                    if(data.data[i].orderStatus == "1"){
                                        html += '<span class="fr send color_27332b">待支付</span>';
                                    }else if(data.data[i].orderStatus == "2"){
                                        html += '<span class="fr send color_27332b">待发货</span>';
                                    }else if(data.data[i].orderStatus == "3"){
                                        html += '<span class="fr send color_27332b">待收货</span>';
                                    }else if(data.data[i].orderStatus == "4"){
                                        html += '<span class="fr send color_27332b">交易成功</span>';
                                    }else if(data.data[i].orderStatus == "5"){
                                        html += '<span class="fr send color_27332b">交易关闭</span>';
                                    }else if(data.data[i].orderStatus == "6"){
                                        html += '<span class="fr send color_27332b">待确认</span>';
                                    }else if(data.data[i].orderStatus == "7"){
                                        html += '<span class="fr send color_27332b">交易成功</span>';
                                    }else if(data.data[i].orderStatus == "11"){
                                        html += '<span class="fr send color_27332b">退款审批</span>';
                                    }else if(data.data[i].orderStatus == "12"){
                                        html += '<span class="fr send color_27332b">待退款</span>';
                                    }else if(data.data[i].orderStatus == "13"){
                                        html += '<span class="fr send color_27332b">已退款</span>';
                                    }else if(data.data[i].orderStatus == "14"){
                                        html += '<span class="fr send color_27332b">退款失败</span>';
                                    }
                                    html += '</p>';
                                    html += '<div class="weui-panel__bd"><div class="weui-media-box weui-media-box_appmsg"><div class="weui-media-box__hd">';
                                    html += '<img class="weui-media-box__thumb" src="'+data.data[i].orderProductList[j].tenantProduct.titleImgUrl+'">';
                                    html += '</div>';
                                    html += '<div class="weui-media-box__bd">';
                                    html += '<div class="tit_div">'+data.data[i].orderProductList[j].tenantProduct.productName+'</div>';
                                    html += '<div class="m-b-10">';
                                    html += '<em class="shipping">已售'+data.data[i].orderProductList[j].tenantProduct.salesVolume+'</em>';
                                    html += ' </div><div class="price_div">';
                                    html += '<span class="product-price1">¥<span class="big-price">'+data.data[i].orderProductList[j].tenantProduct.favorablePrice+'</span>';
                                    // html += '<span class="small-price">.00</span>';
                                    html += '<em class="originalprice">￥'+data.data[i].orderProductList[j].tenantProduct.productUnit+'</em></span>';
                                    html += '<div class="fr parendiv">';
                                    html += '<em class="xcount">X'+data.data[i].orderProductList[j].purchaseNum+'</em>';
                                    totoalnum += data.data[i].orderProductList[j].purchaseNum;
                                    html += '<em class="youhui">(已优惠'+ ((data.data[i].orderProductList[j].tenantProduct.productUnit - data.data[i].orderProductList[j].tenantProduct.favorablePrice) * data.data[i].orderProductList[j].purchaseNum) +'元)</em>';
                                    html += ' </div>';
                                    html += '</div>';
                                    html += ' </div></div>';
                                    totoalfreight += data.data[i].orderProductList[j].tenantProduct.freight;
                                }

                                html += '<div class="manlageAdd">';
                                html += ' <div class="weui-cells weui-cell_access m-t-0 border-top">';
                                html += '<a href="javascript:" class="weui-cell show-toast-text">';
                                html += '<div class="weui-cell__bd weui-cell_primary">';
                                html += '<p class="recipient">';
                                html += '<span>配送地址</span>';
                                html += ' <span class="fr"><em class="name">'+data.data[i].tenantUserAddrs.consignee+'</em><em>'+data.data[i].tenantUserAddrs.receivingTel+'</em></span>';
                                html += '</p>';
                                html += '</div>';
                                html += '<div class="weui-cell__ft"> </div>';
                                html += ' </a>';
                                html += '</div>';

                                //地址详情补充
                                html += '<div class="addrbox">';
                                html += '<div class="weui-cells m-t-0 addrDetail">';
                                html += '<i class="triangle"></i>';
                                html += ' <div class="weui-cell">';
                                html += '<div class="weui-cell__bd weui-cell_primary">';
                                html += '<p class="recipient">';
                                html += '<span class="recipientmen">收件人：'+data.data[i].tenantUserAddrs.consignee+'</span>';
                                html += ' <span class="fr">'+data.data[i].tenantUserAddrs.receivingTel+'</span>';
                                html += '<br>';
                                html += ' <span class="abposi"><i class="sstfont sst-dizhi fl"></i> <i class="shouhuo">'+data.data[i].tenantUserAddrs.receivingAddress+'</i></span></p>';
                                html += ' </div></div></div></div></div><div style="clear: both;"></div><div class="calculate">';
                                html += '<span>共计<em class="pieces">'+totoalnum+'</em>件商品</span>&nbsp&nbsp';
                                html += '<span class="subtotal">小计:￥'+data.data[i].orderSum+'</span>';//<span class="subtotal">小计:￥<em>75.</em>60</span>
                                html += '<span>含运费￥'+totoalfreight+'</span>';
                                html += '</div></div>';

                                //底部按钮添加
                                if(data.data[i].orderStatus == "1"){
                                    html += '<div class="salesdiv">';
                                    html += '<a id="'+data.data[i].id+'" class="weui-btn weui-btn_plain-default deletebtn">取消订单</a>'
                                    html += ' <a href="javascript:;" class="weui-btn weui-btn_plain-primary logisticsbtn">去付款</a>' ;
                                    html += '</div>';
                                }else if(data.data[i].orderStatus == "2"){
                                    html += '<div class="salesdiv">';
                                    html += ' <a href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
                                    html += '</div>';
                                }else if(data.data[i].orderStatus == "3"){
                                    html += '<div class="salesdiv">';
                                    html += ' <a href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
                                    html += ' <a href="${cxt}/user/togetlogistic?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-primary logisticsbtn">查看物流</a>' ;
                                    html += '<a id="'+data.data[i].id+'" class="weui-btn weui-btn_plain-primary logisticsbtn enterbtn">确认收货</a>';
                                    html += '</div>';
                                }else if(data.data[i].orderStatus == "4"){
                                    if(data.data[i].orderType == "1"){
                                        html += '<div class="salesdiv">';
                                        html += ' <a href=href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
                                        html += ' <a href="${cxt}/user/togetlogistic?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-primary logisticsbtn">查看物流</a>' ;
//                                        html += ' <a href="javascript:;" class="weui-btn weui-btn_plain-primary logisticsbtn">去评价</a>';
                                        html += '</div>';
                                    }else{
                                        html += '<div class="salesdiv">';
                                        html += ' <a href=href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
//                                        html += ' <a href="javascript:;" class="weui-btn weui-btn_plain-primary logisticsbtn">去评价</a>';
                                        html += '</div>';
                                    }

                                }
//                                else if(data.data[i].orderStatus == "5"){
//                                    html += '<span class="fr send color_27332b">交易关闭</span>';
//                                }else if(data.data[i].orderStatus == "6"){
//                                    html += '<span class="fr send color_27332b">待确认</span>';
//                                }
                                else if(data.data[i].orderStatus == "7"){
                                    if(data.data[i].orderType == "1"){
                                        html += '<div class="salesdiv">';
                                        html += ' <a href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
                                        html += ' <a href="${cxt}/user/togetlogistic?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-primary logisticsbtn">查看物流</a>' ;
                                        html += ' <a href="javascript:;" class="weui-btn weui-btn_plain-primary logisticsbtn">去评价</a>';
                                        html += '</div>';
                                    }else{
                                        html += '<div class="salesdiv">';
                                        html += ' <a href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
                                        html += ' <a href="javascript:;" class="weui-btn weui-btn_plain-primary logisticsbtn">去评价</a>';
                                        html += '</div>';
                                    }
                                }
// else if(data.data[i].orderStatus == "11"){
//                                    html += '<span class="fr send color_27332b">退款审批</span>';
//                                }else if(data.data[i].orderStatus == "12"){
//                                    html += '<span class="fr send color_27332b">待退款</span>';
//                                }else if(data.data[i].orderStatus == "13"){
//                                    html += '<span class="fr send color_27332b">已退款</span>';
//                                }else
                                else if(data.data[i].orderStatus == "14"){
                                    html += '<div class="salesdiv">';
                                    html += ' <a href="/user/toaftersale?orderId='+data.data[i].id+'" class="weui-btn weui-btn_plain-default">申请售后</a>'
                                    html += '</div>';
                                }
                                html += '</div></div></li>';

                                $("#staypage").append(html);

                            }

                        }


                    }else {
                        $.toast("订单信息获取失败");
                    }
                }
            );
        }





    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    // 例子：
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {        "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));    for (var k in o)    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));    return fmt;}
</script>
</body>

</html>