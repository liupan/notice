<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/9
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="applicable-device" content="mobile">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>个人资料</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <style type="text/css">
        html {
            font-size:18px;
        }
    </style>
</head>

<body class="bg_fafafa" ontouchstart>
<%--<section>--%>
    <!--	data-* 属性用于存储页面或应用程序的私有自定义数据。-->
    <%--<form id="formyz"  class="am-form" data-am-validator="" data-url="/register">--%>
        <ul class="weui-cells loginmain weui-cells_form">
            <li class="weui-cell">
                <div class="weui-cell__hd">
                    <label class="weui-label">姓名</label>
                </div>
                <div class="weui-cell__bd weui-cell_primary">
                    <input class="weui-input" type="text" name="name" value="yanglina" id="username" placeholder="请输入姓名">
                </div>
            </li>
            <li class="weui-cell weui-cell_select weui-cell_select-after">
                <div class="weui-cell__hd">
                    <label for="" class="weui-label">性别</label>
                </div>
                <div class="weui-cell__bd">
                    <select class="weui-select" name="select2" id="usersex">
                        <option value="1">男</option>
                        <option value="2">女</option>
                    </select>
                </div>
            </li>
            <li class="weui-cell">
                <div class="weui-cell__hd"><label for="birthday" class="weui-label">出生日期</label></div>

                <div class="weui-cell__bd">
                    <input class="weui-input" type="text" id="birthday" value="2017年6月9号"  placeholder="请选择日期">
                </div>
            </li>
            <li class="weui-cell">
                <div class="weui-cell__hd">
                    <label class="weui-label">手机号码</label>
                </div>
                <div class="weui-cell__bd weui-cell_primary">
                    <input class="weui-input" type="text" name="phone" value="18267148003" pattern="[0-9]*" placeholder="请输入号码" readonly="" id="phone">
                </div>
            </li>
            <li class="weui-cell">
                <div class=" aside">
                    <input class="weui-input" type="number" name="code" value="" pattern="[0-9]*" placeholder="输入验证码">
                </div>
                <div class="flower">
                    <!--<button class="weui-btn weui-btn_primary codebtn red-blue " type="button" id="sendCodeBtn">获取验证码</button>-->
                    <input type="button" name="sendCodeBtn" id="sendCodeBtn" value="获取验证码" class="weui-btn weui-btn_primary codebtn red-blue" />
                </div>

            </li>

        </ul>
        <div class="weui-btn-area">
            <button class="weui-btn weui-btn_primary shlogin" id="shadowbtn">保&nbsp;&nbsp;存</button>
        </div>
    <%--</form>--%>

<%--</section>--%>
</body>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script src="/resources/js/jquery-weui.js"></script>
<script src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script>

    /*日期初始化*/
    $("#birthday ").calendar({
        value: ['2016-12-12'],
        dateFormat: 'yyyy年mm月dd日'  // 自定义格式的时候，不能通过 input 的value属性赋值 '2016年12月12日' 来定义初始值，这样会导致无法解析初始值而报错。只能通过js中设置 value 的形式来赋值，并且需要按标准形式赋值（yyyy-mm-dd 或者时间戳)
    });

    var timer = null;
    function myTimeout(this_) {
        var second = 60;
        timer = setInterval(function() {
            second -= 1;
            if(second > 0) {
                $(this_).val(second + "秒后重新获取");
                $(this_).disabled = true; //可以被点击
                $(this_).addClass("weui-btn_disabled"); //this不能被点击了，点击失效
            } else {
                clearInterval(timer);
                timer = null;
                $(this_).val("获取验证码");
                $(this_).disabled = false; //可以被点击
                $(this_).removeClass("weui-btn_disabled"); //this不能被点击了，点击失效
            }
        }, 1000);
    }

    $(function() {
        getuserinfo();
        $("body").on("click", "#sendCodeBtn", function() {
            if(timer == null) {
                myTimeout(this);
                $.get("${cxt}/userbind/getidentify",
                    function (data) {
                        if(data == "scueess"){
                            $.toast("验证码已发送到您的手机");
                        }else if(data  == "false"){
                            $.toast("获取验证码失败");
                        }
                    }

                )
                return;
            }
        });

        $("#shadowbtn").click(function(){
            //var phone = $("input[name='phone']").val();
            var code = $("input[name='code']").val();
            if(code == '') {
                $.toast('请输入验证码', "cancel");
                return false;
            }else {
                $.get("${cxt}/user/checkcode",
                    {
                        code : code
                    },
                    function (data) {
                        if(data == "success"){
                            $.post("${cxt}/user/saveuserinfo",
                                {
                                    name :  $("#username").val(),
                                    sexFlag : $("#usersex").val(),
                                    birthday : $("#birthday").val(),
                                    telephone : $("#phone").val()
                                },

                                function (data2) {
                                    if(data2.success){
                                        $.toast("编辑成功");
                                        window.location.href = "${cxt}/user/index";
                                    }else if(data2.false){
                                        $.toast("编辑失败");
                                    }
                                }
                            );
                        }else if(data == "wrongcode"){
                            $.toast("验证码输入有误");
                        }

                    }
                );
            }


        });
    })
    
    //获取用户信息
    function getuserinfo() {
        $.get("${ctx}/userinfo/getuserinfo",
            function(data) {
                var time1 = new Date(data.data.birthday).Format("yyyy年MM月dd日");
                $("#username").val(data.data.name);
                $("#usersex").val(data.data.sexFlag);
                $("#birthday").val(time1);
                $("#phone").val(data.data.telephone);
                var strPhone = $("#phone").val().replace(/(\d{3})\d{4}(\d{4})/, '$1****$2');
                $("#phone").val(strPhone);


            }
        );
    }

    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    // 例子：
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {        "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));    for (var k in o)    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));    return fmt;}
</script>

</html>
