<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/7
  Time: 9:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>编辑地址</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <style type="text/css">
        html {
            font-size: 18px;
        }
    </style>

</head>

<body class="bg_fafafa" ontouchstart>
<div class="content">
    <div class="main">
        <form method="post" id="formobj" action="prpduct_addresslist.html">
            <div class="weui-cells m-t-0">
                <div class="weui-cell">
                    <div class="weui-cell__hd">
                        <label class="weui-label">姓名</label>
                    </div>
                    <div class="weui-cell__bd weui-cell_primary">
                        <input class="weui-input" type="text" name="name" value="" placeholder="请输入姓名">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd">
                        <label class="weui-label">联系电话</label>
                    </div>
                    <div class="weui-cell__bd weui-cell_primary">
                        <input class="weui-input" type="number" name="phone" value="" pattern="[0-9]*" placeholder="请输入号码">
                    </div>
                </div>
                <div class="weui-cell_access m-t-0 weui-cell">

                    <div class="weui-cell__hd"><label for="data" class="weui-label">快递地址</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" id="dizhi" type="text" name="province" value="">
                    </div>
                    <div class="weui-cell__ft"> </div>

                </div>

                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <textarea class="weui-textarea" placeholder="请填写详细地址" rows="3" name="address"></textarea>
                    </div>
                </div>
                <input type="hidden" id="data_code" name="data_code" value="">
            </div>
        </form>
    </div>
</div>

<div id="cart1" class="cart-concern-btm-fixed five-column four-column " style="display: table; z-index: 0;">
    <div class="weui-flex">
        <div class="weui-flex__item">
            <button class="weui-btn weui-btn_primary savebtn">使用并保存</button>
        </div>

    </div>
</div>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/city-picker.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>

<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>
<script type="text/javascript">
    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }
    $("#dizhi").cityPicker({
        title: "请选择地址",
        onChange: function(picker, values, displayValues) {
            console.log(values, displayValues);
        }
    });
    $(function() {
        getaddressInfo();
        $('.savebtn').click(function() {
            $("#data_code").val($("#dizhi").attr('data-codes'));
            console.log($("#data_code").val($("#dizhi").attr('data-codes')));
            var name = $("input[name='name']").val();
            var phone = $("input[name='phone']").val();
            var province = $("input[name='province']").val();
            var address = $("textarea[name='address']").val();
            var receivingAddress = province + "," + address;
            /* var default_address=$("input[name='default_address']").val();*/

            if(trim(name) == '') {
                $.toast('请填写姓名');
                return false;
            }
            if(trim(phone) == '') {
                $.toast('请填写手机号');
                return false;
            }
            if(!(/^1[3|4|5|7|8]\d{9}$/.test(phone))) {
                $.toast('请填写合法的手机号');
                return false;
            }
            if(trim(province) == '') {
                $.toast('请选择所在省市');
                return false;
            }
            if(trim(address) == '') {
                $.toast('请填写详细地址');
                return false;
            }

            $.post("${ctx}/useradrs/saveeditadre",
                {
                    consignee: name,
                    receivingTel: phone,
                    receivingAddress: receivingAddress
                },
                function (data) {
                    if(data.success){
                        $.toast("地址保存成功！");
                        setTimeout(function () {
                            window.location.href = "${ctx}/useradrs/adrslist"
                        },300)
                    }
                })
        });

    })
    
    
    function getaddressInfo() {
        $.get("${ctx}/useradrs/getaddressbyid",
            function(data) {
                var address = data.data.receivingAddress;
                var adresstemp = address.split(",");
                $("input[name='name']").val(data.data.consignee);
                $("input[name='phone']").val(data.data.receivingTel);
                $("input[name='province']").val(adresstemp[0]);
                $("textarea[name='address']").val(adresstemp[1]);

            }
        );
    }
</script>
</body>

</html>

