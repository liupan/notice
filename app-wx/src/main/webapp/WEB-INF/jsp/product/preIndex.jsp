<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/15
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
    <head>

        <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
        <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
        <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
        <meta charset="UTF-8">
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <meta name="format-detection" content="telphone=yes, email=yes" />
        <title>乡丽乡亲</title>


    </head>
<body ontouchstart class="bg_fafafa">
<!--主体部分 开始-->
<div class="content">
    <div class="swiper-container" data-space-between='10' data-pagination='.swiper-pagination' data-autoplay="1000">
        <div class="swiper-wrapper">
            <div class="swiper-slide"><img src="/resources/img/swiper-1.jpg" alt=""></div>
            <div class="swiper-slide"><img src="/resources/img/swiper-2.jpg" alt=""></div>
            <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
            <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination" id="on"></div>
    </div>
    <!--banner 结束-->
    <!--分类-->
    <div class="weui-grids whitebg">
        <a id="tospecialtylist" class="weui-grid js_grid"  >
            <div class="weui-grid__icon">
                <img src="/resources/img/menpiao.png" alt="土特产">
            </div>
            <p class="weui-grid__label">土特产</p>
        </a>
        <a id="tohomestaylist" class="weui-grid js_grid" data-id="button">
            <div class="weui-grid__icon">
                <img src="/resources/img/minsu.png" alt="民宿">
            </div>
            <p class="weui-grid__label">民宿</p>
        </a>
        <a id="toticketlist" class="weui-grid js_grid" data-id="button">
            <div class="weui-grid__icon">
                <img src="/resources/img/tutechan.png" alt="门票">
            </div>
            <p class="weui-grid__label">门票</p>
        </a>

    </div>
    <!--分类结束-->
    <div class="hr"></div>
    <!--土特产-->
    <div class="main">
        <div class="whitebg weui-grids">
            <div class="title_one">
                <a id="tospecialtylist1" class="more"></a>
            </div>
            <ul class="sp_list" id="specialtylist">
                <li>
                    <a href="" title="">
                        <div class="pic_div"><img src="/resources/img/caomei_03.jpg"></div>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无..
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="" title="">
                        <div class="pic_div"><img src="/resources/img/caomei_03.jpg"></div>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无..
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>

            </ul>

        </div>

        <div class="hr"></div>
        <!--民宿-->
        <div class="whitebg weui-grids">
            <div class="title_two">
                <a id="tohomestaylist1" class="more"></a>
            </div>
            <ul class="sp_list" id="homestaylist">
                <li>
                    <a href="" title="">
                        <div class="pic_div"><img src="/resources/img/caomei_03.jpg"></div>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无..
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="" title="">
                        <div class="pic_div"><img src="/resources/img/caomei_03.jpg"></div>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无..
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>

            </ul>

        </div>

        <div class="hr"></div>
        <!--门票-->
        <div class="whitebg weui-grids">
            <div class="title_three">
                <a id="toticketlist1" class="more"></a>
            </div>

            <ul class="sp_list" id="ticketlist">
                <li>
                    <a href="" title="">
                        <div class="pic_div"><img src="/resources/img/caomei_03.jpg"></div>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无..
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="" title="">
                        <div class="pic_div"><img src="/resources/img/caomei_03.jpg"></div>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无..
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>

            </ul>

        </div>

    </div>

</div>
<!--主体部分结束-->
<!--底部固定导航 开始-->
<%@include file="/inc/footdown.jsp" %>
</body>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type='text/javascript' src='/resources/js/swiper.min.js' charset='utf-8'></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>


<script>
    var largePrice = "";
    var smallPrice = "";
    $(function() {
        FastClick.attach(document.body);
        //向后台发送数据请求
        $.ajax({
            type:'post',
            url:"${ctx}/product/list",
            dataType : 'json',
            data : {},
            success : function(data) {

                for(var i=0;i<data.data.length;i++){
                    returnFloat(data.data[i].favorablePrice);

                    var html = "<li>";
                    html += " <a href='${cxt}/product/specialtydetail?id="+data.data[i].id+"' title=''>";
                    html += "<div class='pic_div'><img src='/resources/img/caomei_03.jpg'></div>";
                    html += " <div class='tit_div'>"+data.data[i].productName +"</div>";
                    html += "  <div class='price_div'>";
                    html += "<span class='product-price1'>¥<span class='big-price'>"+largePrice+"</span>";
                    html += "<span class='small-price'>"+smallPrice+"</span>";
                    html += "<em class='originalprice'>￥"+data.data[i].productUnit+"</em></span>";
                    html += "<span class='product-xiaoliang'>已售"+data.data[i].salesVolume+"</span>";
                    html += " </div>";
                    html += " </a>";
                    html += " </li>";
                    if(data.data[i].productType == "土特产"){
                        $("#specialtylist").html(html);
                    }else if(data.data[i].productType == "民宿"){
                        $("#homestaylist").html(html);
                    }else if(data.data[i].productType == "门票"){
                        $("#ticketlist").html(html);
                    }


                }
            }
        });

        $("#tospecialtylist").attr("href"," ${cxt}/product/specialtyindex");
        $("#tospecialtylist1").attr("href","${cxt}/product/specialtyindex");

        $("#tohomestaylist").attr("href"," ${cxt}/product/homestayindex");
        $("#tohomestaylist1").attr("href","${cxt}/product/homestayindex");

        $("#toticketlist").attr("href"," ${cxt}/product/ticketindex");
        $("#toticketlist1").attr("href","${cxt}/product/ticketindex");

        $("#videoproject").attr("href","${cxt}/video/videoindex");


    });


    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }



</script>
<script>
    $(".swiper-container").swiper({
        loop: true,
        autoplay: 3000
    });
</script>

</html>
