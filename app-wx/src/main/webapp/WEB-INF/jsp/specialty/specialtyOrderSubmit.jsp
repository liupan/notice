<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/26
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>订单确认</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <style type="text/css">
        html{
            font-size: 16px;
        }
        #formobj  .weui-label {
            width: 75px;
        }
    </style>
</head>

<body class="bg_fafafa" ontouchstart>
<section>
    <div class="content">
        <div class="main">
            <form method="post" id="formobj" action="prpduct_addresslist.html">
                <div class="weui-cells m-t-0">
                    <div class="weui-cell">
                        <div class="weui-cell__hd">
                            <label class="weui-label">姓名</label>
                        </div>
                        <div class="weui-cell__bd weui-cell_primary">
                            <input class="weui-input" type="text" name="name" value="" placeholder="请输入姓名">
                        </div>
                    </div>
                    <div class="weui-cell">
                        <div class="weui-cell__hd">
                            <label class="weui-label">联系电话</label>
                        </div>
                        <div class="weui-cell__bd weui-cell_primary">
                            <input class="weui-input" type="number" name="phone" value="" pattern="[0-9]*" placeholder="请输入号码">
                        </div>
                    </div>
                    <div class="weui-cells weui-cell_access m-t-0">
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label for="data" class="weui-label">快递地址</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input" id="dizhi" type="text" name="province" value="">
                            </div>
                            <div class="weui-cell__ft"> </div>
                        </div>
                    </div>

                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <textarea class="weui-textarea" placeholder="请填写详细地址" rows="3" name="address"></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="data_code" name="data_code" value="">
                </div>
            </form>
            <div class="splist">
                <div class="weui-panel_access">
                    <div class="hr"></div>
                    <p class="title_flower"></p>
                    <div class="hr"></div>
                    <div class="weui-panel__bd hidden">
                        <div class="weui-media-box weui-media-box_appmsg">
                            <div class="weui-media-box__hd">
                                <img class="weui-media-box__thumb" src="../img/caomei_03.jpg">
                            </div>
                            <div class="weui-media-box__bd">
                                <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无</div>
                                <div class="m-b-10">
                                    <em class="shipping">已售44</em>

                                </div>
                                <div class="price_div">
											<span class="product-price1">¥<span class="big-price">98</span>
											<span class="small-price">.00</span>
											<em class="originalprice">￥180</em></span>
                                    <span class="product-xiaoliang">快递：免运费</span>
                                </div>

                                <div class="weui-cell wellbox">
                                    <div class="weui-cell__bd weui-cell_primary">
                                        <div class="shopping_count">
                                            <span class="fr"><em class="youhui">(已优惠33.0元)</em></span>
                                        </div>
                                    </div>
                                    <div style="font-size: 0px;" class="weui-cell__ft">
                                        <span class="number-selector number-selector-sub needsclick">-</span>
                                        <input id="qty_num" pattern="[0-9]*" class="number-input" style="width: 50px;" value="1" data-min="1" data-step="1">
                                        <span class="number-selector number-selector-plus needsclick">+</span>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="calculate fr">
                            <span>共计<em class="pieces">2</em>件商品</span>
                            <span class="subtotal" id="subtotal1"></span>

                            <span id="distributionType">含运费￥0.00</span>

                        </div>

                    </div>
                    <div class="hr"></div>


                </div>
            </div>
        </div>

    </div>
    <!--底部固定导航 开始-->
    <div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table; z-index: 0;">
        <ul class="orderbtn">
            <li class="subtotalInfo">
                <div class="calculate fr">
                    <span>共计<em class="pieces_all">2</em>件商品</span>
                    <span class="combined_total" id="subtotal2"></span>

                    <span id="distributionType1"><em class="freight">￥0.00</em></span>

                </div>
            </li>
            <li class="orderOk">
                <button class="weui-btn weui-btn_primary submitOrderBtn">提交订单</button>
            </li>
        </ul>


    </div>
</section>

<!--底部固定导航 结束-->
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/city-picker.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>

<script type="text/javascript">

    var largePrice = "";
    var smallPrice = "";
    var sellpriceall = "";
    var productUnit ="";
    var freight = 0;
    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }
    $("#dizhi").cityPicker({
        title: "请选择地址",
        onChange: function(picker, values, displayValues) {
            console.log(values, displayValues);
        }
    });
    //获取产品详情
    function getspecialtydetail() {

        $.get("${ctx}/specialty/details",
            function(data) {
                returnFloat(data.data.favorablePrice);
                var buynum = data.data.buyNum;
                $("#qty_num").val(buynum);
                productUnit = data.data.productUnit;
                sellpriceall = data.data.favorablePrice;
                freight = data.data.freight;
                var youhui = productUnit - sellpriceall;
                youhui  = youhui * buynum;
                var html3 = '<i></i>'+data.data.tenantMerchant.merchantName;
                $('.title_flower').append(html3);
                $('.weui-media-box__thumb').html(data.data.titleImgUrl);
                $('.tit_div').html(data.data.productName);
                $('.shipping').html("已售"+data.data.salesVolume);
                $('.big-price').html(largePrice);
                $('.small-price').html(smallPrice);
                $('.originalprice').html("￥"+data.data.productUnit);
                if(data.data.distributionType == "包邮"){
                    $('.product-xiaoliang').html(data.data.distributionType);
                    $('.distributionType').css("display","none");
                    $('.distributionType1').css("display","none");
                }else{
                    $('.distributionType').html(data.data.distributionType);
                    $('.distributionType1').html(data.data.distributionType);
                    $('.product-xiaoliang').css("display","none");
                }

                $('.youhui').html("(已优惠"+youhui+"元)");
                $('.pieces').html(buynum);
                $('.pieces_all').html(buynum);
                $('.freight').html("含运费"+freight);
                var sellprice = sellpriceall * buynum + freight;
                returnFloat(sellprice);
                var html1 = '小计:￥<em>'+largePrice+'</em>'+smallPrice;
                $('#subtotal1').append(html1);
                var html2 = '合计:￥<em>'+largePrice+'</em>'+smallPrice;
                $('#subtotal2').append(html2);

            }
        );
    }

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }


    $(function() {
        getspecialtydetail();
        //变更购买数改变样式
        $('.number-selector-sub').click(function() {

            upDownOperationwWithParam($(this), sellpriceall, productUnit, freight);

        });

        $('.number-selector-plus').click(function() {
            upDownOperationwWithParam($(this), sellpriceall, productUnit, freight);
        });


        $('.submitOrderBtn').click(function() {
            $("#data_code").val($("#dizhi").attr('data-codes'));
            console.log($("#data_code").val($("#dizhi").attr('data-codes')));
            var name = $("input[name='name']").val();
            var phone = $("input[name='phone']").val();
            var province = $("input[name='province']").val();
            var address = $("textarea[name='address']").val();
            var receivingAddress = province + address;
            var buynum = $("#qty_num").val();
            /* var default_address=$("input[name='default_address']").val();*/

            if(trim(name) == '') {
                $.toast('请填写姓名', "cancel");
                return false;
            }
            if(trim(phone) == '') {
                $.toast('请填写手机号', "cancel");
                return false;
            }
            if(!(/^1[3|4|5|7|8]\d{9}$/.test(phone))) {
                $.toast('请填写合法的手机号', "cancel");
                return false;
            }
            if(trim(province) == '') {
                $.toast('请选择所在省市', "cancel");
                return false;
            }
            if(trim(address) == '') {
                $.toast('请填写详细地址', "cancel");
                return false;
            }

            $.post("${ctx}/specialty/save_new_order",
                {
                    consignee: name,
                    receivingTel: phone,
                    receivingAddress: receivingAddress,
                    buynum:buynum
                },
                function (data) {

                    if(data == "success"){
                            window.location.href = "${ctx}/order/toordersuc";
                    }
                })




            /*$('#formobj')[0].submit();*/
        });

    })



</script>

<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>
</body>

</html>
