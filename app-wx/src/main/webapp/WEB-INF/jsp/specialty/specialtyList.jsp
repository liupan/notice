<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/17
  Time: 17:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>土特产</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body ontouchstart class="bg_fafafa">
<!--主体部分 开始-->
<div class="content">
    <div class="sortbox">
        <div class="weui-row">
            <div class="weui-col-50">
                <a href="javascript:" class="on firstmenu">
                    <i class="type classification"></i>
                    <i  class="classType">分类</i>
                    <i class="up"></i>
                </a>

                <input type="text" name="" hidden="hidden" value="" class="menuval" id="menuval"/>
                <input type="text" name="" hidden="hidden" value="" class="secondary" id="secondary"/>

                <ul class="menu" id="gettype">

                </ul>
            </div>
            <div class="weui-col-50">
                <a href="javascript:" class="firstmenu">
                    <i class="type priceicon"></i>
                    <i  id="sort">排序</i>
                    <i class="up"></i>
                </a>
                <ul class="menu sortmenu">
                    <li id="pricedown">
                        价格从高到低
                    </li>
                    <li  id="priceup">
                        价格从低到高

                    </li>
                    <li id="inventorynumdown">
                        销量从高到低
                    </li>
                    <li id="inventorynumup">
                        销量从低到高
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="whitebg weui-grids m-t-10">
            <ul class="sp_list" id="specialtylist">

            </ul>

        </div>
        <!--点击下一页-->
        <div id="next" style="display: none;">
            <span style="cursor: pointer; ">共有记录855 ,共<span id="count">86</span>页&nbsp;</span>
            <span style="cursor: pointer; ">当前为第<em class="pageIndex">1</em>页&nbsp;</span>
        </div>

        <div class="weui-loadmore" id="infinite-1">
            <i class="weui-loading"></i>
            <span class="weui-loadmore__tips">正在加载</span>
        </div>

    </div>

</div>
<!--主体部分 结束-->

<!--底部固定导航 开始-->
<%@include file="/inc/footdown.jsp" %>


</body>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type='text/javascript' src='/resources/js/swiper.min.js' charset='utf-8'></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>


<script>
    var largePrice = "";
    var smallPrice = "";
    var pageNo = "1";
    var sortType = "";
    $(function() {
        FastClick.attach(document.body);
        //向后台发送数据请求
        getspecialtylist("","","");
        getspecialtytype();
        GoToNextPage();

        $(".sortbox .firstmenu").click(function() {
            $(".sortbox .firstmenu").removeClass("on");
            $(this).addClass("on");

        });

        $(".sortmenu li").click(function(){
            $("#sort").text($(this).text());
        });

    });
    //排序方式
    $("#pricedown").click(function () {
        var typep = $("#menuval").val();
        var typel = $("#secondary").val();
        sortType = "1";
        reflashdiv();
        getspecialtylist(typep,typel,1);
    });
    $("#priceup").click(function () {
        var typep = $("#menuval").val();
        var typel = $("#secondary").val();
        sortType = "2";
        reflashdiv();
        getspecialtylist(typep,typel,2);
    });
    $("#inventorynumdown").click(function () {
        var typep = $("#menuval").val();
        var typel = $("#secondary").val();
        sortType = "3";
        reflashdiv();
        getspecialtylist(typep,typel,3);
    });
    $("#inventorynumup").click(function () {
        var typep = $("#menuval").val();
        var typel = $("#secondary").val();
        sortType = "4";
        reflashdiv();
        getspecialtylist(typep,typel,4);
    });

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }


    //获取产品列表
    function getspecialtylist(pid,cid,sorttype) {

        $.get("${ctx}/product/listbysearch",
            {
                pageno: pageNo,
                pagesize: "10",
                searchtypep: pid,
                searchtypel: cid,
                sorttype :   sorttype
        },
        function(data) {

                for(var i=0;i<data.data.result.length;i++){
                    returnFloat(data.data.result[i].favorablePrice);

                    var html = "<li>";
                    html += " <a href='${cxt}/product/specialtydetail?id="+data.data.result[i].id+"' title=''>";
                    html += "<div class='pic_div'><img src='"+data.data.result[i].titleImgUrl+"'></div>";
                    html += " <div class='tit_div_line'>"+data.data.result[i].productName +"</div>";
                    html += "  <div class='price_div'>";
                    html += "<span class='product-price1'>¥<span class='big-price'>"+largePrice+"</span>";
                    html += "<span class='small-price'>"+smallPrice+"</span>";
                    html += "<em class='originalprice'>￥"+data.data.result[i].productUnit+"</em></span>";
                    html += "<span class='product-xiaoliang'>已售"+data.data.result[i].salesVolume+"</span>";
                    html += " </div>";
                    html += " </a>";
                    html += " </li>";
                    if(data.data.result[i].productType == "土特产"){
                        $("#specialtylist").append(html);
                    }

                }
            }
        );
    }


    //获取产品类别
    function getspecialtytype() {

        $.get("${ctx}/product/getspetype",
            function(data) {
                var firstlev = [];
                var secondlev = [];
                //分割为数组
                for(var i=0;i<data.data.length;i++){
                    if(data.data[i].productLevel == "0"){
                            firstlev.push( data.data[i]);
                    }else if(data.data[i].productLevel == "1"){
                            secondlev.push( data.data[i]);
                    }
                }

                var html = "";
                //拼装列表
                for(var i=0;i<firstlev.length;i++){
                    html += "<li id='"+firstlev[i].id+"'>"+firstlev[i].productName;
                    html += "<ul>";
                    for(var j=0;j<secondlev.length;j++){
                        if(secondlev[j].parentId  ==  firstlev[i].id){
                            html += "<li id='"+secondlev[j].id+"' onclick='pushlevid("+secondlev[j].id+",\""+secondlev[j].productName+"\",\""+firstlev[i].id+"\")'> <a href='javascript:'>"+secondlev[j].productName +"</a></li>"
                        }
                    }
                    html += "</ul>";
                    html += "</li>"
                }
                $("#gettype").append(html);

            }
        );
    }


    //滑动加载下一页
    var pageIndex = parseInt($(".pageIndex").text());
    function GoToNextPage() {

        var loading = false; //状态标记
        $(document.body).infinite().on("infinite", function() {
            if(pageNo + 1 <= parseInt($("#count").text())) {

                pageNo += 1;
//                $(".pageIndex").text(pageIndex);
                if(loading) return;
                loading = true;
                setTimeout(function() {
                    var typep = $("#menuval").val();
                    var typel = $("#secondary").val();
                    getspecialtylist(typep,typel,sortType);
                    $("#goodslist").append(html);
                    loading = false;
                }, 100); //模拟延迟

            } else {
                $("#infinite-1").html("(^_^)没有更多咯！");
            }

        });

    }

    //清空当前页面
    function  reflashdiv() {
        $("#specialtylist").empty();
        pageNo = "1";
    }

    function  pushlevid(levid,levname,firstid) {
//        $(this).find("a").attr("id");
        $(".secondary").val(levid);
        $(".classType").text(levname);
        $(".menuval").val(firstid);
        var typep = $("#menuval").val();
        var typel = $("#secondary").val();
        reflashdiv();
        getspecialtylist(typep,typel,sortType);
    }

</script>

<script type="text/javascript">
    $(function() {
        $(".sortbox .firstmenu").click(function() {
            $(".sortbox .firstmenu").removeClass("on");
            $(this).addClass("on");

        });

    })
</script>
</html>
