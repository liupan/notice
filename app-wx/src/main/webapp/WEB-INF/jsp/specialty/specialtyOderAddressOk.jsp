<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/22
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>订单确认</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />

</head>

<body class="bg_fafafa" ontouchstart>
<section>
    <div class="content">
        <div class="main">
            <form method="post">
                <div class="weui-cells weui-cell_access writeaddress m-t-0">
                    <a id="gotoaddreslist" class="weui-cell" >
                        <div class="weui-cell__bd weui-cell_primary">
                            <input id="addressid" value="" type="hidden"></input>
                            <p class="recipient" id="addressinfo" >

                            </p>
                        </div>
                        <div class="weui-cell__ft"> </div>
                    </a>
                </div>
            </form>
            <div class="splist">
                <div class="weui-panel_access">
                    <div class="hr"></div>
                    <p class="title_flower"></p>
                    <!--<div class="hr"></div>-->
                    <div class="weui-panel__bd hidden">
                        <div class="weui-media-box weui-media-box_appmsg">
                            <div class="weui-media-box__hd">
                                <img class="weui-media-box__thumb" src="/resources/img/58be5d19e2dc6_100_100.jpg">
                            </div>
                            <div class="weui-media-box__bd">
                                <div class="tit_div"></div>
                                <div class="m-b-10">
                                    <em class="shipping"></em>

                                </div>
                                <div class="price_div">
											<span class="product-price1">¥<span class="big-price"></span>
											<span class="small-price"></span>
											<em class="originalprice"></em></span>
                                    <span class="product-xiaoliang"></span>
                                </div>

                                <div class="weui-cell wellbox">
                                    <div class="weui-cell__bd weui-cell_primary">
                                        <div class="shopping_count">
                                            <span class="fr"><em class="youhui"></em></span>
                                        </div>
                                    </div>
                                    <div style="font-size: 0px;" class="weui-cell__ft">
                                        <span class="number-selector number-selector-sub needsclick">-</span>
                                        <input id="qty_num" pattern="[0-9]*" class="number-input" style="width: 50px;" value="1" data-min="1" data-step="1">
                                        <span class="number-selector number-selector-plus needsclick">+</span>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="calculate fr">
                            <span>共计<em class="pieces">2</em>件商品</span>
                            <span class="subtotal" id="subtotal1"></span>

                            <span id="distributionType"></span>

                        </div>

                    </div>





                    <%--<div class="hr"></div>--%>


                </div>
            </div>
        </div>

    </div>
    <!--底部固定导航 开始-->
    <div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table; z-index: 0;">
        <ul class="orderbtn">
            <li class="subtotalInfo">
                <div class="calculate fr">
                    <span>共计<em class="pieces_all">2</em>件商品</span>
                    <span class="combined_total" id="subtotal2"></span>

                    <span id="distributionType1"><em class="freight">￥0.00</em></span>

                </div>
            </li>
            <li class="orderOk">
                <button class="weui-btn weui-btn_primary submitOrderBtn">提交订单</button>
            </li>
        </ul>
    </div>

    <!--底部固定导航 结束-->
</section>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/city-picker.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>

<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>
<script type="text/javascript">

    var largePrice = "";
    var smallPrice = "";
    var sellpriceall = "";
    var productUnit ="";
    var freight = 0;
    $(function() {
        getaddressdetail();
        getspecialtydetail();

        $("#gotoaddreslist").click(function (){
            var nowbuynum = $("#qty_num").val();
            window.location.href = "${cxt}/useradrs/adrslist?nowbuynum="+nowbuynum;
        })

        $(".submitOrderBtn").click(function(){
//            $.toast('订单提交成功');
            var timer = setTimeout(function() {
                var paymoney = largePrice + smallPrice;
                $.post("${ctx}/specialty/submitform",
                    {
                        addressId: $("#addressid").val(),
                        buynum: $("#qty_num").val()
                    },
                    function(data) {
                        if(data == "success"){
                            window.location.href = "${ctx}/order/toordersuc";
                        }else {
                            $.toast('订单提交失败');
                        }

                    }
                );
//                location.href = "../pay/product_pay.html";
            }, 1500);



        });


        //变更购买数改变样式
        $('.number-selector-sub').click(function() {

            upDownOperationwWithParam($(this), sellpriceall, productUnit, freight);

        });

        $('.number-selector-plus').click(function() {
            upDownOperationwWithParam($(this), sellpriceall, productUnit, freight);
        });


    })

    //获取地址详情
    function getaddressdetail() {

        $.get("${ctx}/useradrs/getinfobyid",
            function(data) {
                $("#addressid").val(data.data.id);
                var html = ' <span class="recipientmen" >收件人：'+data.data.consignee+'</span>';
                html += '<span class="fr">'+data.data.receivingTel+'</span>';
                html += '<br>';
                html += '<span class="hidden"><i class="sstfont sst-dizhi fl"></i> <i class="shouhuo" >'+data.data.receivingAddress+'</i></span>';

                $("#addressinfo").append(html);

            }
        );
    }

    //获取产品详情
    function getspecialtydetail() {

        $.get("${ctx}/specialty/details",
            function(data) {
                returnFloat(data.data.favorablePrice);
                var buynum = data.data.buyNum;
                $("#qty_num").val(buynum);
                productUnit = data.data.productUnit;
                sellpriceall = data.data.favorablePrice;
                freight = data.data.freight;
                var youhui = productUnit - sellpriceall;
                youhui  = youhui * buynum;
                var html3 = '<i></i>'+data.data.tenantMerchant.merchantName;
                $('.title_flower').append(html3);
                $('.weui-media-box__thumb').html(data.data.titleImgUrl);
                $('.tit_div').html(data.data.productName);
                $('.shipping').html("已售"+data.data.salesVolume);
                $('.big-price').html(largePrice);
                $('.small-price').html(smallPrice);
                $('.originalprice').html("￥"+data.data.productUnit);
                if(data.data.distributionType == "包邮"){
                    $('.product-xiaoliang').html(data.data.distributionType);
                    $('.distributionType').css("display","none");
                    $('.distributionType1').css("display","none");
                }else{
                    $('.distributionType').html(data.data.distributionType);
                    $('.distributionType1').html(data.data.distributionType);
                    $('.product-xiaoliang').css("display","none");
                }

                $('.youhui').html("(已优惠"+youhui+"元)");
                $('.pieces').html(buynum);
                $('.pieces_all').html(buynum);
                $('.freight').html("含运费"+freight);
                var sellprice = sellpriceall * buynum + freight;
                returnFloat(sellprice);
                var html1 = '小计:￥<em>'+largePrice+'</em>'+smallPrice;
                $('#subtotal1').append(html1);
                var html2 = '合计:￥<em>'+largePrice+'</em>'+smallPrice;
                $('#subtotal2').append(html2);

            }
        );
    }


    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }



</script>
</body>

</html>
