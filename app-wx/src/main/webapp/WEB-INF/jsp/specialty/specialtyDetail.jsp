<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/17
  Time: 17:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>土特产</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/plyr.css" />
</head>

<body ontouchstart>
<!--主体部分 开始-->
<section>
    <div class="content">
        <!--播放器-->
        <div class="main">
            <div class="playbox">
                <video controls class="vs" poster="../img/swiper-1.png">
                    <source src="http://www.jplayer.org/video/m4v/Incredibles_Teaser.m4v" type="video/mp4">
                </video>
            </div>
            <div class="product_info" id="specialtyinfo">

            </div>


            <div class="hr"></div>
            <div class="sortbox">
                <div class="weui-row row50">
                    <div class="weui-col-50">
                        <a href="javascript:" class="on firstmenu scroll_a">
                            <i class="type detailicon"></i>
                            <i>详情</i>
                        </a>

                    </div>
                    <div class="weui-col-50">
                        <a href="javascript:" class="firstmenu scroll_b">
                            <i class="type pinjia"></i>
                            <i>评价</i>
                        </a>

                    </div>
                </div>
            </div>
            <div class="titone"></div>
            <div id="tab1" class="weui-tab__bd_item">


            </div>
            <div class="tittwo"></div>
            <!--评论页面开始-->
            <div class="tab2 whitebg">
                <ul class="comments commlist">
                    <li>
                        <div class="comicon"><img src="../img/caomei_03.jpg" /></div>
                        <div class="cons">
                            <h1>Deadeasy</h1>
                            <span class="time">10分钟前</span>
                            <div class="introduce
">水果收到了，特别新鲜，下次还来买！
                            </div>
                        </div>

                    </li>
                    <li>
                        <div class="comicon"><img src="../img/caomei_03.jpg" /></div>
                        <div class="cons">
                            <h1>Deadeasy</h1>
                            <span class="time">10分钟前</span>
                            <div class="introduce">
                                <p>水果收到了，特别新鲜，下次还来买！</p>

                            </div>
                        </div>

                    </li>
                    <li>
                        <div class="comicon"><img src="../img/caomei_03.jpg" /></div>
                        <div class="cons">
                            <h1>Deadeasy</h1>
                            <span class="time">10分钟前</span>
                            <div class="introduce">
                                <ul class="graphic">
                                    <li>
                                        <div class="pic">
                                            <img src="../img/shouji.png" alt="土特产">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pic">
                                            <img src="../img/shouji.png" alt="土特产">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pic">
                                            <img src="../img/s2.png" alt="土特产">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pic">
                                            <img src="../img/shouji.png" alt="土特产">
                                        </div>
                                    </li>
                                </ul>

                                <p>水果收到了，特别新鲜，下次还来买！</p>
                            </div>
                        </div>

                    </li>
                    <div class="btnbox">
                        <a href="product_comments.html" class="weui-btn weui-btn_plain-primary">查看全部488条评论</a>
                    </div>
                </ul>

            </div>
        </div>

    </div>
    <!--主体部分 结束-->
    <!--底部固定导航 开始-->
    <div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table;">
        <div class="concern-cart">
            <a href="tel:13545350035" class="dong-dong-icn J_ping" id="imBottom" href="#"> <em class="btm-act-icn"></em> <span class="color_27332b">联系客服 </span> </a>
            <a href="product_cart.html" class="cart-car-icn" id="toCart" href="/shop/index/cart.html">
                <em class="btm-act-icn" id="shoppingCart"> <i class="order-numbers" id="carNum"></i> </em> <span class="focus-info">购物车</span>
            </a>
        </div>

        <div class="action-list">

            <a class="weui-btn yellow-color" onclick="add_to_cart(27)" id="addcart">加入购物车</a>

            <a  class="red-blue" onclick="buynow()">立即购买</a>
        </div>
    </div>
</section>
</body>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script src="/resources/js/plyr.js"></script>


<script>
    var largePrice = "";
    var smallPrice = "";
    var pageNo = "1";
    var productId = "";
    var inventoryNum ="";
    FastClick.attach(document.body);

    plyr.setup();
    $(".sortbox .firstmenu").click(function() {
        $(".sortbox .firstmenu").removeClass("on");
        $(this).addClass("on");
    });
    $('.number-selector-plus').click(function() {
        upDownOperation($(this));
    });
    $('.number-selector-sub').click(function() {
        upDownOperation($(this));
    });


    //购买数量加减控制
    $(".product_info").on('click','.number-selector-plus',function(){
        upDownOperation($(this));

    });
    $(".product_info").on('click','.number-selector-sub',function(){
        upDownOperation($(this));

    });

    $(function() {
        //向后台发送数据请求
        getspecialtydetail();
        getspecialtytype();

        $(".scroll_a").click(function() {
            $('html,body').animate({
                scrollTop: $("#tab1").offset().top
            }, 800);
        });
        $(".scroll_b").click(function() {
            $('html,body').animate({
                scrollTop: $(".tab2").offset().top
            }, 800);
        });
    });



    function upDownOperation(element) {
        var _input = element.parent().find('input'),
            _value = _input.val(),
            _step = _input.attr('data-step') || 1;
        //检测当前操作的元素是否有disabled，有则去除
        element.hasClass('disabled') && element.removeClass('disabled');
        //检测当前操作的元素是否是操作的添加按钮（.input-num-up）'是' 则为加操作，'否' 则为减操作
        if(element.hasClass('number-selector-plus')) {
            var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
                _max = _input.attr('data-max') || false,
                _down = element.parent().find('.number-selector-sub');

            //若执行'加'操作且'减'按钮存在class='disabled'的话，则移除'减'操作按钮的class 'disabled'
            _down.hasClass('disabled') && _down.removeClass('disabled');
            if(_max && _new_value >= _max) {
                _new_value = _max;
                element.addClass('disabled');
            }
        } else {
            var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
                _min = _input.attr('data-min') || false,
                _up = element.parent().find('.number-selector-plus');
            //若执行'减'操作且'加'按钮存在class='disabled'的话，则移除'加'操作按钮的class 'disabled'
            _up.hasClass('disabled') && _up.removeClass('disabled');
            if(_min && _new_value <= _min) {
                _new_value = _min;
                element.addClass('disabled');
            }
        }
        _input.val(_new_value);
    };

    /*加入购物车的图标	*/
    function refresh_carNum(num) {
        $('#carNum').text(num);
    }



//    function add_to_cart(info_id, rightnow) {
//        if(josnData[1].status == 1) {
//            $.toast("加入购物车成功");
//            refresh_carNum($("#qty_num").val());
//        } else if(josnData[1].status == '-1') {
//            $.toast("购买商品时输入的数量不合法");
//        } else if(josnData[1].status == '-2') {
//            $.toast("没有登陆,请先登陆");
//            window.location = "sh_login.html";
//        } else if(josnData[1].status == '-3') {
//            $("#addcart").addClass("addCard").removeClass("yellow-color");
//            $.toast("库存不足！");
//
//
//        } else {
//            $.toast("添加到购物车失败");
//        }
//    }

    $(document).ready(function() {
        var addto_cart = $('#add_cart');
        var direc_buy = $('#directorder');
        var good_id = 27;
        addto_cart.click(function() {
            add_to_cart(good_id);
        });
        direc_buy.click(function() {
            add_to_cart(good_id, true)
        });
    });


    //获取产品详情
    function getspecialtydetail() {

        $.get("${ctx}/product/details",
            function(data) {
                returnFloat(data.data.favorablePrice);
                var youhui = data.data.productUnit - data.data.favorablePrice;
               html = " <div class='tit_div'>"+data.data.productName+"</div>";
               html += "<div class='price_div'>";
               html += " <span class='product-price1'>¥<span class='big-price'>"+largePrice+"</span>";
               html += "<span class='small-price'>"+smallPrice+"</span>";
               html += " <em class='originalprice'>￥"+data.data.productUnit+"</em></span>";
               html += " </div>";
               html += " <div class='m-b-10'>";
               html += "<em class='shipping'>"+data.data.distributionType+"</em></span>";
               html += "<span class='product-xiaoliang'>已售"+data.data.salesVolume+"</span>";
               html += " </div>";
               html += "  <div class='weui-cell wellbox'>";
               html += "<div class='weui-cell__bd weui-cell_primary'>";
               html += "<div class='shopping_count' id='detailone'>";
               html += " <span>购买数量<em class='shipping'>(库存"+data.data.inventoryNum+"件)</em></span>";
               html += "<span class='fr'><em class='youhui'>(已优惠"+youhui+"元)</em></span>"
               html += "</div>";
               html += "</div>";
               html += " <div style='font-size: 0px;' class='weui-cell__ft'>";
               html += "<span class='number-selector number-selector-sub needsclick'>-</span>";
               html += " <input id='qty_num' pattern='[0-9]*' class='number-input' style='width: 50px;' value='1' data-min='1' data-step='1'>";
               html += "<span class='number-selector number-selector-plus needsclick'>+</span>";
               html += " </div>" ;
               html += "</div>" ;
               html += "<p class='title_flower'><i></i>"+data.data.tenantMerchant.merchantName+"</p>";

                $("#specialtyinfo").append(html);

               html1='';
               html1 = '<div class="content-box">';
               html1 += data.data.productDetails;
               html1 += ' </div>';
               $("#tab1").append(html1);

               productId = data.data.id;
               inventoryNum = data.data.inventoryNum;


            }
        );
    }


     function add_to_cart(info_id, rightnow) {
        var addcart_url = "/shop/index/cart_add_item.html";
        var directbuy_url = "/shop/index/cart.html";
         $.get("${ctx}/product/listbysearch",
             {
                 pageno: pageNo,
                 pagesize: "10",
                 searchtypep: pid,
                 searchtypel: cid,
                 sorttype :   sorttype
             },
             function(data) {

                 for(var i=0;i<data.data.result.length;i++){
                     returnFloat(data.data.result[i].favorablePrice);

                     if(data.data.result[i].productType == "土特产"){
                         $("#specialtylist").append(html);
                     }

                 }
             }
         );




        $.post(addcart_url, {good_id: info_id, count: $('#qty_num').val()}, function (msg) {
        //console.log(msg);return false;
        if(msg.status) {
        if(rightnow && msg.status == 1) {
            location.replace(directbuy_url);
        } else {
        if(msg.status == 1) {
            $.toast("加入购物车成功");
            refresh_carNum(msg.cart_count);
        } else if(msg.status == '-1') {
            $.toast("购买商品时输入的数量不合法");
        } else if(msg.status == '-2') {
            $.toast("没有登陆,请先登陆");
            window.location = "/login";
        } else if(msg.status == '-3') {
            $.toast("库存不足！");
        } else {
            $.toast("添加到购物车失败");
        }
        }
     } else {
        $.toast('添加到购物车失败。');
     }
     }, 'json');
     }


    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }





    //获取产品类别
    function getspecialtytype() {

        $.get("${ctx}/product/getspetype",
            function(data) {
                var firstlev = [];
                var secondlev = [];
                //分割为数组
                for(var i=0;i<data.data.length;i++){
                    if(data.data[i].productLevel == "0"){
                            firstlev.push( data.data[i]);
                    }else if(data.data[i].productLevel == "1"){
                            secondlev.push( data.data[i]);
                    }
                }

                var html = "";
                //拼装列表
                for(var i=0;i<firstlev.length;i++){
                    html += "<li id='"+firstlev[i].id+"'>"+firstlev[i].productName;
                    html += "<ul>";
                    for(var j=0;j<secondlev.length;j++){
                        if(secondlev[j].parentId  ==  firstlev[i].id){
                            html += "<li id='"+secondlev[j].id+"' onclick='pushlevid("+secondlev[j].id+",\""+secondlev[j].productName+"\",\""+firstlev[i].id+"\")'> <a href='javascript:'>"+secondlev[j].productName +"</a></li>"
                        }
                    }
                    html += "</ul>";
                    html += "</li>"
                }
                $("#gettype").append(html);

            }
        );
    }




    //清空当前页面
    function  reflashdiv() {
        $("#specialtylist").empty();
        pageNo = "1";
    }

    //立即购买
    function buynow() {
        var buynum = $("#qty_num").val();
        if(inventoryNum <= buynum){
            $.toast("库存不足！");
        }else {
            window.location.href = "${ctx}/specialty/buynow?specialtyid="+productId+"&buynum="+buynum;

        }

    }


</script>

<script type="text/javascript">
    $(function() {
        $(".sortbox .firstmenu").click(function() {
            $(".sortbox .firstmenu").removeClass("on");
            $(this).addClass("on");

        });

    })
</script>
</html>
