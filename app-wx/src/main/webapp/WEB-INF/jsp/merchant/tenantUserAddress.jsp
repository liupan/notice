<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/23
  Time: 11:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>地址管理</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body class="bg_fafafa">
<section>
    <div class="content">
        <a href="${cxt}/useradrs/addAddress" class="addaddress_a" >
            <i></i>新增收货地址
        </a>
        <div class="weui-cells" id="addresslist">




        </div>
    </div>
</section>
</body>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script>
    $(function(){
        getaddresslist();


        //删除地址
        $(document).on("click", ".deletebtn", function() {
            var addressid =  $(this).attr("id");
            $.confirm("", "确认删除吗？",  function() {
                $.post("${cxt}/useradrs/deleteadre?addressid="+addressid,
                    function (data) {
                        if(data.success){
                            $.toast("删除成功");
                            var timer = setTimeout(function() {
                                location.reload();
                            }, 1000)
                        }
                    })
            }, function() {
                //取消操作
            });
        });

    });

    //获取地址列表
    function getaddresslist() {

        $.get("${ctx}/useradrs/getaddreslist",
            function(data) {
                if(data != null){
                for (var i=0;i<data.data.length;i++){
                    var html = '<div class="addmer" >';
                    html += '<a href="${cxt}/useradrs/backorder?addressid='+data.data[i].id+'" >';
                    html += ' <label class="weui-cell">';
                    html += '<div class="weui-cell__bd weui-cell_primary">';
                    html += ' <p class="recipient">';
                    html += '<span class="recipientmen">收件人：'+data.data[i].consignee+'</span>';
                    html += '<span class="fr">'+data.data[i].receivingTel+'</span>';
                    html += '<br>';
                    html += '<span class="hidden"><i class="sstfont sst-dizhi fl"></i> <i class="shouhuo">'+data.data[i].receivingAddress+'</i></span></p>';
                    html += '</div>';
                    html += '</label>';
                    html += '<div class="removeAddr hidden">';
                    html += '<div class="fr divedit">';
                    html += '<a href="${cxt}/useradrs/editAddress?editAdresId='+data.data[i].id+'">编辑</a>';
                    html += '<a id="'+data.data[i].id+'"  class="deletebtn">删除</a>';
                    html += ' </div>';
                    html += '</div>';
                    html += '</div></a>';
                    html += '<div class="hr-15"></div>';

                    $("#addresslist").append(html);
                }


                }
            }
        );
    }
    
    


</script>
</html>
