<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/15
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html  lang="zn-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>民宿</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />

    <link rel="stylesheet" type="text/css" href="/resources/css/JTCalendar.css" />
    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">
    <style type="text/css">
        html {
            font-size: 16px;
        }

        .weui-label {
            width: auto;
            padding-right: 10px;
        }
    </style>
</head>
<body ontouchstart class="bg_fafafa">
<!--主体部分 开始-->
<div class="content">
    <div class="sortbox">
        <div class="weui-row">
            <div class="weui-col-50">
                <a href="javascript:" class="on firstmenu">
                    <i class="type scenic"></i>
                    <i class="classType">景点</i>
                    <i class="up"></i>
                </a>
                <input type="text" name="" hidden="hidden" value="" class="menuval" id="menuval"/>
                <ul class="menu" id="scenic">

                </ul>
            </div>
            <div class="weui-col-50">
                <a href="javascript:" class="firstmenu">
                    <i class="type priceicon"></i>
                    <i id="sort">排序</i>
                    <i class="up"></i>
                </a>
                <ul class="menu sortmenu">
                    <li id="pricedown">
                        价格从高到低
                    </li>
                    <li  id="priceup">
                        价格从低到高

                    </li>
                    <li id="inventorynumdown">
                        销量从高到低
                    </li>
                    <li id="inventorynumup">
                        销量从低到高
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main">
        <%--<div class="weui-flex choose_time_box choose_time" id="calendar">--%>
            <%--<div class="weui-flex__item intime">--%>
						<%--<span data-intime class="choose_time_span">--%>
							 <%--<i class="home" style="margin-right:30px;">入住</i>--%>
				             <%--<span class="choose_date" id="startTime"></span>--%>
						<%--<i class="f">今天</i>--%>
						<%--</span>--%>

            <%--</div>--%>
            <%--<strong>共<i class="tottime">1</i>晚</strong>--%>
            <%--<div class="weui-flex__item outtime">--%>
						<%--<span data-outtime class="choose_time_span">--%>
						 <%--<i class="home" style="margin-left:42px;">离店</i>--%>
						 <%--<span class="choose_date" id="endTime"></span>--%>
						<%--<i class="f">周三</i>--%>
						<%--</span>--%>

            <%--</div>--%>
        <%--</div>--%>

        <div class="whitebg weui-grids m-t-3">

            <ul class="sp_list" id="homestaylist">
                <%--<li>--%>
                    <%--<a href="product_detail.html" title="">--%>
                        <%--<div class="pic_div"><img src="/resources/img/58f1e56e9502a.jpg"></div>--%>
                        <%--<div class="tit_div_line">生态阁楼--%>
                        <%--</div>--%>
                        <%--<div class="price_div">--%>
									<%--<span class="product-price1">¥<span class="big-price">98</span>--%>
									<%--<span class="small-price">.00起</span>--%>
									<%--</span>--%>
                            <%--<span class="product-xiaoliang">已售88</span>--%>
                        <%--</div>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                    <%--<a href="product_detail.html" title="">--%>
                        <%--<div class="pic_div"><img src="/resources/img/58f1e56e9502a.jpg"></div>--%>
                        <%--<div class="tit_div_line">生态阁楼--%>
                        <%--</div>--%>
                        <%--<div class="price_div">--%>
									<%--<span class="product-price1">¥<span class="big-price">98</span>--%>
									<%--<span class="small-price">.00起</span>--%>
									<%--</span>--%>
                            <%--<span class="product-xiaoliang">已售88</span>--%>
                        <%--</div>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                    <%--<a href="product_detail.html" title="">--%>
                        <%--<div class="pic_div"><img src="/resources/img/58f1e56e9502a.jpg"></div>--%>
                        <%--<div class="tit_div_line">生态阁楼--%>
                        <%--</div>--%>
                        <%--<div class="price_div">--%>
									<%--<span class="product-price1">¥<span class="big-price">98</span>--%>
									<%--<span class="small-price">.00起</span>--%>
									<%--</span>--%>
                            <%--<span class="product-xiaoliang">已售88</span>--%>
                        <%--</div>--%>
                    <%--</a>--%>
                <%--</li>--%>

            </ul>

        </div>
    </div>

</div>
<!--主体部分 结束-->

<!--底部固定导航 开始-->
<%@include file="/inc/footdown.jsp" %>

</body>

<%--<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>--%>
<%--<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>--%>
<script type='text/javascript' src='/resources/js/swiper.min.js' charset='utf-8'></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>

<script type="text/javascript" src="/resources/js/zepto.min.js"></script>
<script type="text/javascript" src="/resources/js/JTCalendar.js"></script>
<script>
    var largePrice = "";
    var smallPrice = "";
    var pageNo = "1";
    var sortType = "";


    $(function() {
        getscenic();
        GetTomorrow();
        gethomestaylist("","","");
        FastClick.attach(document.body);
        $(".sortbox .firstmenu").click(function() {
            $(".sortbox .firstmenu").removeClass("on");
            $(this).addClass("on");
        });
        /*calendar初始化*/
        $("#startTime").text(GetNowDate());
        /*$("#date").val(GetTomorrow());
         */
        $('#calendar').JTCalendar({
            title: '选择入住离店日期',
            inTime: new Date(),
            minDay: 1,
            monthNumber: 3,
            onClick: function($elm, cb) {
                console.log($elm, cb)

                $elm.find('[data-intime] .choose_date').text(cb.inTime.month + '月' + cb.inTime.date + '日')
                    .next('i.f').text(cb.inTime.dayText);

                $elm.find('[data-outtime] .choose_date').text(cb.outTime.month + '月' + cb.outTime.date + '日')
                    .next('i.f').text(cb.outTime.dayText);

                $elm.find('i.tottime').text(cb.totTime);

            }
        })



        FastClick.attach(document.body);



    //排序方式
    $("#pricedown").click(function () {
        var typep = $("#menuval").val();
        sortType = "1";
        reflashdiv();
        gethomestaylist(typep,1);
    });
    $("#priceup").click(function () {
        var typep = $("#menuval").val();
        sortType = "2";
        reflashdiv();
        gethomestaylist(typep,2);
    });
    $("#inventorynumdown").click(function () {
        var typep = $("#menuval").val();
        sortType = "3";
        reflashdiv();
        gethomestaylist(typep,3);
    });
    $("#inventorynumup").click(function () {
        var typep = $("#menuval").val();
        sortType = "4";
        reflashdiv();
        gethomestaylist(typep,4);
    });



    });



    function GetNowDate() {
        var myDate, myDateStr;
        myDate = new Date(); //创建日期对象
        var MM = myDate.getMonth() + 1; //取月份
        if(MM < 10) //如果月份小于10就在前面加0
            MM = "0" + MM;
        var dd = myDate.getDate(); //取日
        if(dd < 10)
            dd = "0" + dd;

        myDateStr = MM + "月" + dd + "日";
        return myDateStr; //返回系统时间
    }

    function GetTomorrow() {
        var myDate, myDateStr;
        myDate = new Date();
        var yyyy = myDate.getFullYear(); //取四位年份
        var MM = myDate.getMonth() + 1;
        if(MM < 10)
            MM = "0" + MM;
        var dd = myDate.getDate();
        if(dd < 10)
            dd = "0" + dd;
        myDateStr = yyyy + "-" + MM + "-" + dd;
        var arr = myDateStr.split("-");
        var newdt = new Date(Number(arr[0]), Number(arr[1]) - 1, Number(arr[2]) + 1);
        repnewdt = (newdt.getMonth() + 1) + "月" + newdt.getDate() + "日";
        $('#endTime').text(repnewdt);
    }


    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }

    //获取景区信息
    function getscenic() {

        $.get("${ctx}/product/getscenic",
            function(data) {
                var html = "";
                //拼装列表
                for(var i=0;i<data.data.length;i++){
                    html += "<li id='"+data.data[i].id+"' onclick='choosebyscenic("+data.data[i].id+",\""+data.data[i].scenicName+"\")'>"+data.data[i].scenicName;
                    html += "</li>"
                }
                $("#scenic").append(html);

            }
        );
    }

    //获取民宿列表
    function gethomestaylist(pid,sorttype) {

        $.get("${ctx}/product/homelistbysearch",
            {
                pageno: pageNo,
                pagesize: "10",
                searchtypep: pid,
                sorttype :   sorttype
            },
            function(data) {

                for(var i=0;i<data.data.result.length;i++){
                    returnFloat(data.data.result[i].startPrice);

                    var html = "<li>";
                    html += " <a href='${cxt}/product/homestaydetail?id="+data.data.result[i].id+"' title=''>";
                    html += "<div class='pic_div'><img src='"+data.data.result[i].titleImgUrl+"'></div>";
                    html += "<div class='tit_div_line'><input value='"+data.data.result[i].homestayName+"' maxlength='6'></input>";
                    html += "</div>";
                    html += "<div class='price_div'>";
                    html += "<span class='product-price1'>¥<span class='big-price'>"+largePrice+"</span>";
                    html += "<span class='small-price'>"+smallPrice+"起</span>";
                    html += "</span>";
                    html += "<span class='product-xiaoliang'>已售"+data.data.result[i].salesVolume+"</span>";
                    html += " </div>";
                    html += " </a>";
                    html += " </li>";


                    $("#homestaylist").append(html);


                }
            }
        );
    }

    //清空当前页面
    function  reflashdiv() {
        $("#homestaylist").empty();
        pageNo = "1";
    }

    //根据所选景区搜索
    function choosebyscenic(scenicid , scenicidname) {

        $(".classType").text(scenicidname);
        $(".menuval").val(scenicid);
        var typep = $("#menuval").val();
        reflashdiv();
        gethomestaylist(typep,sortType);
    }

</script>


</html>
