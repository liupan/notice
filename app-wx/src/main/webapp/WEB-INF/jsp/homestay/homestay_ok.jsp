<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/2
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>订单确认</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/JTCalendar.css" />
</head>

<body class="bg_fafafa" ontouchstart style="overflow-x:hidden">
<h1 class="demo_title" id="scenicTitle">九寨沟景区</h1>
<div class="content">
    <div class="main">
        <form method="post" id="formobjorder" action="prpduct_addresslist.html">
            <div class="weui-cells m-t-0">
                <div class="weui-cell">
                    <div class="weui-cell__hd">
                        <label class="weui-label">入住人</label>
                    </div>
                    <div class="weui-cell__bd weui-cell_primary">
                        <input class="weui-input" type="text" name="name" value="" placeholder="请输入姓名" id="username">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd">
                        <label class="weui-label">电话</label>
                    </div>
                    <div class="weui-cell__bd weui-cell_primary">
                        <input class="weui-input" type="number" name="phone" value="" pattern="[0-9]*" placeholder="请输入号码" id="userphone">
                    </div>
                </div>
                <div id="calendar">
                    <div class="weui-cell">
                        <div class="weui-cell__hd">
                            <label class="weui-label">入住</label>
                        </div>
                        <div class="weui-cell__bd weui-cell_primary">
									<span data-intime class="choose_time_span">
					                    <span class="choose_date" id="startTime">5月26日</span>
									<%--<i class="f">今天</i>--%>
									</span>
                        </div>
                    </div>
                    <div class="weui-cell" style="position: relative;">
                        <div class="weui-cell__hd">
                            <label class="weui-label">离店</label>
                        </div>
                        <div class="weui-cell__bd weui-cell_primary">
									<span data-outtime class="choose_time_span">
									    <span class="choose_date" id="endTime">5月27日</span>
									<%--<i class="f">明天</i>--%>
									</span>
                        </div>
                        <span class="nights"><i class="tottime">1</i>晚</span>

                    </div>

                </div>

                <div class="weui-cell">
                    <div class="weui-cell__bd weui-cell_primary">
                        <div>
                            <span>房间数量</span>

                        </div>
                    </div>

                    <div class="preferential">
                        <div style="font-size: 0px; overflow: hidden;" class="weui-cell__ft">
                            <a class="number-selector number-selector-sub needsclick">-</a>
                            <input id="qty_num" pattern="[0-9]*" class="number-input" style="width: 50px;" value="1" data-min="1" data-step="1">
                            <a class="number-selector number-selector-plus needsclick">+</a>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="titaler" id="youhui">(已优惠<em>33.00</em>元)</div>
                    </div>
                </div>
                <div class="weui-cell">

                    <div class="weui-flex__item">
                        <span class="titaler" style="margin-left:10px;" id="orderrule">预定后不可退</span>
                    </div>
                    <div class="calculate fr orderbtn">
                        <span>共计<em class="pieces">2</em>间</span>
                        <!--	<span class="subtotal">小计:￥<em>75.</em>60</span>-->
                        <span class="subtotal">合计：<em>75.</em><i>60</i></span>
                    </div>

                </div>

            </div>
        </form>
    </div>
</div>

<!--底部固定导航 开始-->
<div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table; z-index: 0;">
    <ul class="orderbtn">
        <li class="subtotalInfo">
            <a href="tel:13545350035" class="customer">
                <em class="cusicon"></em>
                <p>联系客服</p>
            </a>
            <div class="calculate fr">
                <span class="subtotal">合计：<em>75.</em><i>60</i></span>
            </div>
        </li>
        <li class="orderOk">
            <button class="weui-btn weui-btn_primary submitOrderBtn">提交订单</button>
        </li>
    </ul>

</div>
<script type="text/javascript" src="/resources/js/zepto.min.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/JTCalendar.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>

<script type="text/javascript">
    var largePrice = "";
    var smallPrice = "";
    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }

    $(function() {
        getroomuserdetails();
        getroomdetails();
        JTCalendarData();
        /*点击购买数量*/
        $(".number-selector-sub").click(function() {
            upDownOperation($(this));
        });
        $(".number-selector-plus").click(function() {
            upDownOperation($(this));
        });

        /*提交订单*/
        $('.submitOrderBtn').click(function() {
            var name = $("input[name='name']").val();
            var phone = $("input[name='phone']").val();

            if(trim(name) == '') {
                $.toast('请填写姓名');
                return false;
            }
            if(trim(phone) == '') {
                $.toast('请填写手机号');
                return false;
            }
            if(!(/^1[3|4|5|7|8]\d{9}$/.test(phone))) {
                $.toast('请填写合法的手机号');
                return false;
            }
            var buyusername = $("#username").val();
            var buyuserphone = $("#userphone").val();
            var starttime =  $("#startTime").text();
            var endtime = $("#endTime").text();
            var buynum = $("#qty_num").val();
            $.post("${ctx}/homestay/homeroomsave",
                {
                    buyusername :buyusername,
                    buyuserphone : buyuserphone,
                    starttime : starttime,
                    endtime : endtime,
                    buynum : buynum
                },
                function (data) {
                    if (data == "success"){
                        $.toast('订单提交成功');
                        var timer = setTimeout(function() {
                            window.location.href = "${ctx}/order/toordersuc";
                        }, 1500)
                    }else {
                        $.toast('订单提交失败');
                    }
                });

            /*$('#formobj')[0].submit();*/
        });

    })

    //获取用户信息
    function getroomuserdetails( ) {
        $.get("${ctx}/userinfo/getuserinfo",

            function(data) {
                $("#username").val(data.data.name);
                $("#userphone").val(data.data.telephone);

            }

        );
    }

    //获取房型详细信息
    function getroomdetails( ) {
        $.get("${ctx}/homestay/homeroomdetails",

            function(data) {
                $("#scenicTitle").val(data.data.tenantHomestay.tenantScenic.scenicName);
                $("#startTime").html(data.data.startTime);
                $("#endTime").html(data.data.endTime);
                var buynum = data.data.buyNum;
                var youhui = data.data.roomPrice - data.data.roomSalprice;
                $("#qty_num").val(buynum);
                youhui = youhui * buynum;
                var sellprice = data.data.roomSalprice * buynum;
                returnFloat(sellprice);
                $("#youhui").html("(已优惠<em>"+youhui+"</em>元)");
                $("#orderrule").html(data.data.unsubscribeInfo);
                $(".pieces").html(buynum);
                $(".subtotal").html("合计：<em>"+largePrice+"</em><i>"+smallPrice+"</i>");


            }
        );
    }

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }
</script>
</body>

</html>