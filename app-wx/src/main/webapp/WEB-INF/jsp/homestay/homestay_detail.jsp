<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/31
  Time: 14:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html lang="zn-CN">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>民宿</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/JTCalendar.css" />
</head>

<body ontouchstart style="overflow-x:hidden">
<!--主体部分 开始-->
<section>
    <div class="content">
        <div class="main">
            <div class="swiper-container" data-space-between='10' data-pagination='.swiper-pagination' data-autoplay="1000">
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src="/resources/img/swiper-1.jpg" alt=""></div>
                    <div class="swiper-slide"><img src="/resources/img/swiper-2.jpg" alt=""></div>
                    <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
                    <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination" id="on"></div>
            </div>
            <!--banner 结束-->
            <dl class="dlinfo">
                <dt>
                <div class="product_info" id="homestayinfo">

                </div>
                </dt>
                <dd>
                    <div class="service">
                        <!--<em class="youhui text_center">(已优惠33.0元)</em>-->
                        <div class="map" id="tomap">


                        </div>
                        <a href="tel:18267148003" class="red-blue weui-btn servicebtn">
                            <i></i> 联系客服
                        </a>
                    </div>

                </dd>
            </dl>

            <div class="hr"></div>
            <div class="sortbox">
                <div class="weui-row row80 search-letters">
                    <div class="weui-col-30">
                        <a href="javascript:" class="on firstmenu" onclick="javascript:scroller('tabA', 800);">
                            <i class="type convention"></i>
                            <i>预定</i>
                        </a>

                    </div>
                    <div class="weui-col-30">
                        <a href="javascript:" class="firstmenu" onclick="javascript:scroller('tabB', 800);">
                            <i class="type detailicon"></i>
                            <i>详情</i>
                        </a>

                    </div>
                    <div class="weui-col-30">
                        <a href="javascript:" class="firstmenu" onclick="javascript:scroller('tabC', 800);">
                            <i class="type pinjia"></i>
                            <i>评价</i>
                        </a>

                    </div>
                </div>
            </div>

            <div class="tit_0">
                <a href="#" name="A"></a>
            </div>

            <div id="tabA" class="weui-tab__bd_item">
                <div class="weui-flex choose_time_box choose_time" id="calendar">
                    <div class="weui-flex__item intime">
								<span data-intime class="choose_time_span">
							         <i class="home" style="margin-right:30px;">入住</i>
				                     <span class="choose_date" id="startTime"></span>
								<i class="f">今天</i>
								</span>

                    </div>
                    <strong>共<i class="tottime">1</i>晚</strong>
                    <div class="weui-flex__item outtime">
								<span data-outtime class="choose_time_span">
						 <i class="home" style="margin-left:42px;">离店</i>
						 <span class="choose_date" id="endTime"></span>
								<i class="f">明天</i>
								</span>

                    </div>
                </div>
                <!--<div class="splist">-->
                <div class="weui-panel__bd hidden homeproduct" id="roomlist">
                    <a href="javascript:void(0);" class="weui-media-box weui-media-box_appmsg">
                        <div class="weui-media-box__hd">
                            <img class="weui-media-box__thumb" src="../img/58be5d19e2dc6_100_100.jpg" id="pb1">
                        </div>
                        <div class="weui-media-box__bd" id="yudi">
                            <div class="tit_div">温馨大床房（有房）</div>
                            <div class="m-b-10">
                                <em class="shipping">已售44</em>
                            </div>
                            <div class="price_div">
										<span class="product-price1">¥<span class="big-price">98</span>
										<span class="small-price">.00</span>
										<em class="originalprice">￥180</em></span>
                            </div>

                            <div class="weui-cell wellbox">
                                <div class="weui-cell__bd weui-cell_primary">

                                </div>
                            </div>

                        </div>
                        <div class="resbox">
                            <span><em class="youhui">(已优惠33.0元)</em></span>
                            <button class="weui-btn weui-btn_primary reserBtn red-blue">预定</button>
                        </div>

                    </a>

                    <a href="javascript:void(0);" class="weui-media-box weui-media-box_appmsg">
                        <div class="weui-media-box__hd">
                            <img class="weui-media-box__thumb" src="../img/58be5d19e2dc6_100_100.jpg" id="pb2">
                        </div>
                        <div class="weui-media-box__bd" id="manfy">
                            <div class="tit_div">温馨大床房（有房）</div>
                            <div class="m-b-10">
                                <em class="shipping">已售44</em>
                            </div>
                            <div class="price_div">
										<span class="product-price1">¥<span class="big-price">98</span>
										<span class="small-price">.00</span>
										<em class="originalprice">￥180</em></span>
                            </div>

                            <div class="weui-cell wellbox">
                                <div class="weui-cell__bd weui-cell_primary">

                                </div>
                            </div>

                        </div>
                        <div class="resbox">
                            <span><em class="youhui">(已优惠33.0元)</em></span>
                            <button class="weui-btn reserBtn weui-btn_disabled addCard">满房</button>
                        </div>
                    </a>

                </div>
                <!--</div>-->
                <div class="weui-cells__title address_booK_letter">
                    <a href="#" name="B">可提供的服务</a>
                </div>

            </div>

            <div id="tabB" class="weui-tab__bd_item">
                <!--<div class="titone"></div>-->
                <div class="weui-flex serverBox" id="offerService">

                </div>
                <div class="artical" id="homestaydetials">

                </div>
            </div>
            <div class="tittwo address_booK_letter">
                <a href="#" name="C"></a>
            </div>
            <!--评论页面开始-->
            <div class="tab2 whitebg" id="tabC">
                <ul class="comments commlist">
                    <li>
                        <div class="comicon"><img src="../img/caomei_03.jpg" /></div>
                        <div class="cons">
                            <h1>Deadeasy</h1>
                            <span class="time">10分钟前</span>
                            <div class="introduce">水果收到了，特别新鲜，下次还来买！
                            </div>
                        </div>

                    </li>
                    <li>
                        <div class="comicon"><img src="../img/caomei_03.jpg" /></div>
                        <div class="cons">
                            <h1>Deadeasy</h1>
                            <span class="time">10分钟前</span>
                            <div class="introduce">
                                <p>水果收到了，特别新鲜，下次还来买！</p>

                            </div>
                        </div>

                    </li>
                    <li>
                        <div class="comicon"><img src="../img/caomei_03.jpg" /></div>
                        <div class="cons">
                            <h1>Deadeasy</h1>
                            <span class="time">10分钟前</span>
                            <div class="introduce">
                                <ul class="graphic">
                                    <li>
                                        <div class="pic">
                                            <img src="../img/shouji.png" alt="土特产">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pic">
                                            <img src="../img/shouji.png" alt="土特产">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pic">
                                            <img src="../img/s2.png" alt="土特产">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pic">
                                            <img src="../img/shouji.png" alt="土特产">
                                        </div>
                                    </li>
                                </ul>

                                <p>水果收到了，特别新鲜，下次还来买！</p>
                            </div>
                        </div>

                    </li>
                    <div class="btnbox">
                        <a href="product_comments.html" class="weui-btn weui-btn_plain-primary">查看全部488条评论</a>
                    </div>
                </ul>

            </div>
        </div>

        <!-- 民宿房间详情页-->
        <div id="full" class="weui-popup__container">
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal">
                <h1 id="roomtitle">温馨大床房(有窗)
                    <span class="close close-popup"></span>
                </h1>
                <ul class="homeul">
                    <li>
                        <a href="#"><img src="../img/58f1e56e9502a.jpg" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="../img/58f1e56e9502a.jpg" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="../img/58f1e56e9502a.jpg" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="../img/58f1e56e9502a.jpg" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="../img/58f1e56e9502a.jpg" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="../img/58f1e56e9502a.jpg" /></a>
                    </li>
                </ul>
                <div class="homeinfo">
                    <div class="weui-flex">
                        <div class="weui-flex__item">
                            <label class="honmelab">床型</label>
                            <span class="homespan" id="bedtype">大床</span>
                        </div>
                        <div class="weui-flex__item">
                            <label class="honmelab">早餐</label>
                            <span class="homespan" id="breakfastflag">无早</span>
                        </div>
                    </div>
                    <div class="weui-flex">
                        <div class="weui-flex__item">
                            <label class="honmelab">床数</label>
                            <span class="homespan" id="bednum">1个</span>
                        </div>
                        <div class="weui-flex__item">
                            <label class="honmelab">面积</label>
                            <span class="homespan" id="roomarea">12平米</span>
                        </div>
                    </div>
                    <div class="weui-flex">
                        <div class="weui-flex__item">
                            <label class="honmelab">可住</label>
                            <span class="homespan" id="peoplenum">1个</span>
                        </div>
                        <div class="weui-flex__item">
                            <span class="titaler" id="unsubscribeFlag">入住前2个小时可住</span>
                        </div>
                    </div>

                    <div class="weui-flex">
                        <div class="weui-flex__item">
                            <div class="price_div">
										<span class="product-price1">¥<span class="big-price" id="largeprice">98</span>
										<span class="small-price" id="smallprice">.00</span>
										<em class="originalprice" id="roomPrice">￥180</em></span>
                            </div>

                        </div>
                        <div class="weui-flex__item">
                            <div class="preferential">
                                <div style="font-size: 0px; overflow: hidden;" class="weui-cell__ft">
                                    <a class="number-selector number-selector-sub needsclick">-</a>
                                    <input id="qty_num" pattern="[0-9]*" class="number-input" style="width: 50px;" value="1" data-min="1" data-step="1">
                                    <a class="number-selector number-selector-plus needsclick">+</a>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="titaler" id="youhui">(已优惠<em>33.00</em>元)</div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="weui-media-box__bd room">
                    <h4 class="weui-media-box__title">房间介绍</h4>
                    <p class="media-box" id="roomExplain">由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道。</p>
                </div>
                <!--<a href="javascript:" class="weui-btn weui-btn_primary yudinbtn red-blue">预定</a>-->
                <!--底部按钮-->
                <div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table; z-index: 0;">
                    <ul class="orderbtn">
                        <li class="subtotalInfo">
                            <a href="tel:13545350035" class="customer">
                                <em class="cusicon"></em>
                                <p>联系客服</p>
                            </a>
                            <div class="calculate fr">
                                <span class="subtotal" id="subtotal">合计：<em>75.</em><i>60</i></span>
                            </div>
                        </li>
                        <li class="orderOk">
                            <button class="weui-btn weui-btn_primary yudinbtn">预定</button>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>
    <!--主体部分 结束-->

</section>

<!--底部固定导航 结束-->
<!--<script type="text/javascript" src="../js/jquery-2.1.4.js"></script>-->
<script type="text/javascript" src="/resources/js/zepto.min.js"></script>
<script type="text/javascript" src="/resources/js/JTCalendar.js"></script>

<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type='text/javascript' src='/resources/js/swiper.min.js' charset='utf-8'></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script>
    /*算价格*/
    function upDownOperation2(element, money, yujia, yufei) {
        var _input = element.parent().find('input'),
            _value = _input.val(),
            _step = _input.attr('data-step') || 1;

        //检测当前操作的元素是否有disabled，有则去除
        element.hasClass('disabled') && element.removeClass('disabled');
        //检测当前操作的元素是否是操作的添加按钮（.input-num-up）'是' 则为加操作，'否' 则为减操作
        if(element.hasClass('number-selector-plus')) {
            var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
                _max = _input.attr('data-max') || false,
                _down = element.parent().find('.number-selector-sub');

            //若执行'加'操作且'减'按钮存在class='disabled'的话，则移除'减'操作按钮的class 'disabled'
            _down.hasClass('disabled') && _down.removeClass('disabled');
            if(_max && _new_value >= _max) {
                _new_value = _max;
                element.addClass('disabled');
            }
        } else {
            var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
                _min = _input.attr('data-min') || false,
                _up = element.parent().find('.number-selector-plus');
            //若执行'减'操作且'加'按钮存在class='disabled'的话，则移除'加'操作按钮的class 'disabled'
            _up.hasClass('disabled') && _up.removeClass('disabled');
            if(_min && _new_value <= _min) {
                _new_value = _min;
                element.addClass('disabled');
            }
        }
        _input.val(_new_value);
        _youhui = parseFloat(parseFloat(yujia) - parseFloat(money)) * _new_value;
        _xiaoji = parseFloat(money) * _new_value + parseFloat(yufei);
        $(".youh").text(_youhui);

        $(".pieces").text(_new_value);
        $(".totalpro").text(_xiaoji);
        $(".yufei").text(yufei);
        $('.freight').text(yufei);
        $('.combined_total em ').text(_xiaoji);
    };



    var largePrice = "";
    var smallPrice = "";
    var pageNo = "1";
    var productId = "";
    var inventoryNum ="";
    var roomPrice = "";
    var roomSalprice = "";

    $(function() {
        GetTomorrow();
        GetNowDate();
        /*图片插件轮播初始化*/
        swiper();
        /*时间插件初始化*/
        JTCalendarDatabyroom();

        //获取民宿详情
        gethomestaydetials();

        //获取房型信息
        getroominfo();

        /*点击列表是满房弹出房间详情页面的时候预定按钮灰色*/
//        $("body").on('click','.yudi',function(){
//            $("#full .yudinbtn").removeClass("addCard").addClass("red-blue");;
//            $("#full .yudinbtn").text("预定");
//            $("#full").popup();
//        });
//        $("body").on('click','.manfy',function(){
//            $("#full").popup();
//            $("#full .yudinbtn").addClass("addCard").removeClass('red-blue');
//            $("#full .yudinbtn").text("满房");
//        });
//        $(".yudi").click(function() {
//            $("#full .yudinbtn").removeClass("addCard").addClass("red-blue");
//            $("#full .yudinbtn").text("预定");
//            $("#full").popup();
//        });

//        $(".manfy").click(function() {
//            $("#full").popup();
//            $("#full .yudinbtn").addClass("addCard").removeClass('red-blue');
//            $("#full .yudinbtn").text("满房");
//        });

        $("#full .red-blue").click(function() {
            if($(this).text() == "满房") {
                return false;
            }
            location.href = "home_orderok.html";
        });

        /*点击预定按钮并且不是满房跳转到下单页*/
        $("#yudi").next('.resbox').find(".red-blue").click(function() {
            location.href = "home_orderok.html";
        });

        /*图片放大初始化	*/
        var pb1 = $.photoBrowser({
            items: [
                "../img/swiper-1.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-1.jpg*/ ,
                "../img/swiper-2.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-2.jpg*/ ,
                "../img/swiper-3.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-3.jpg*/ ,
                "../img/swiper-3.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-3.jpg*/
            ],

            onSlideChange: function(index) {
                console.log(this, index);
            },

            onOpen: function() {
                console.log("onOpen", this);

            },
            onClose: function() {
                console.log("onClose", this);
            }
        });
        $("#pb1").click(function() {
            pb1.open();
        });

        $("#pb2").click(function() {
            pb1.open();
        });

        /*评论页面图片初始化*/
        $(".graphic").click(function() {
            commd();
        });

        /*点击减算出合计和优惠价格*/
        $('.number-selector-sub').click(function() {

            upDownOperation2($(this), roomSalprice, roomPrice, 0);


        });
        /*点击加号算出合计和优惠价格*/
        $('.number-selector-plus').click(function() {

            upDownOperation2($(this), roomSalprice, roomPrice, 0);

        });

        $(".orderOk").click(function(){

            var can_home = parseInt($(".canhome").text());
            var count_shop = parseInt($("#qty_num").val());
            if (can_home < count_shop) {
                $.toast('房间数量不足', "cancel");
                return false;
            }

            window.location.href = "${ctx}/homestay/homestayok?buynum="+count_shop;
        });

    })


    //获取民宿详情
    function gethomestaydetials() {
        $.get("${ctx}/product/homestaydetails",
            function(data) {
            returnFloat(data.data.startPrice)
            var html = '<div class="tit_div">'+data.data.homestayName+'</div>';
            html += '<div class="detail_address">';
            html += data.data.fullAddress;
            html += '</div>';
            html += '<div class="price_div">';
            html += ' <span class="product-price1">¥<span class="big-price">'+largePrice+'</span>';
            html += '<span class="small-price">'+smallPrice+'</span>起';
            html += '</span>';
            html += '</div>';
            html += '<div class="m-b-10">';
            html += '<em class="shipping">已销'+data.data.salesVolume+'</em>'
            html += '</div>';

            $("#homestayinfo").append(html);

            var html2 ='<a href="http://api.map.baidu.com/geocoder?address='+data.data.fullAddress+'&output=html&src=oe|xlxq">'
            html2 += '<i></i> 查看地图';
            html2 += ' </a>';

            $("#tomap").append(html2);

            var hsService = data.data.hsService.split(",");
            var html3 = '';
            for(var i=0; i<hsService.length; i++){
                html3 += '<div class="weui-flex__item">' ;
                html3 += '<i class="arr_'+hsService[i]+'"></i>';
                if(hsService[i] == 1){
                    html3 += '<p>无线wifi</p>';
                }else if(hsService[i] == 2){
                    html3 += '<p>行李寄存</p>';
                }else if(hsService[i] == 3){
                    html3 += '<p>独立卫浴</p>';
                }else if(hsService[i] == 4){
                    html3 += '<p>热水淋浴</p>';
                }else if(hsService[i] == 5){
                    html3 += '<p>电吹风</p>';
                }else if(hsService[i] == 6){
                    html3 += '<p>洗护用品</p>';
                }
                html3 += '</div>';

            }
            $("#offerService").append(html3);

            $("#homestaydetials").append(data.data.homestayDetails);

            }
        );
    }

    //获取房型信息
    function getroominfo() {
        var starttime = $("#startTime").text();
        var endtime = $("#endTime").text();
        $.get("${ctx}/homestay/roominfo",
            {
                starttime : starttime,
                endtime : endtime
            },
        function (data) {
            for(var i=0; i<data.data.length;i++){
                returnFloat(data.data[i].roomSalprice);
                var youhui = data.data[i].roomPrice - data.data[i].roomSalprice;
                var html = '<a href="javascript:void(0);" class="weui-media-box weui-media-box_appmsg" roomId="'+data.data[i].id+'">';
                html += '<div class="weui-media-box__hd">';
                html += '<img class="weui-media-box__thumb" src="'+data.data[i].titleImgUrl+'" id="pb'+i+'">';
                html += ' </div>';
                if(data.data[i].iforder == "0"){
                    html += '<div class="weui-media-box__bd yudi" onclick="reserveopen('+data.data[i].id+')">';
                }else if(data.data[i].iforder == "1"){
                    html += '<div class="weui-media-box__bd manfy" onclick="fullopen('+data.data[i].id+')">';
                }

                html += '<div class="tit_div">'+data.data[i].roomName+'</div>';
                html += '<div class="m-b-10">';
                html += '<em class="shipping">已售'+data.data[i].salesVolume+'</em>';
                html += '</div>';
                html += '<div class="price_div">';
                html += '<span class="product-price1">¥<span class="big-price">'+largePrice+'</span>';
                html += '<span class="small-price">'+smallPrice+'</span>';
                html += '<em class="originalprice">￥'+data.data[i].roomPrice+'</em></span>';
                html += '</div>';
                html += '<div class="weui-cell wellbox">';
                html += '<div class="weui-cell__bd weui-cell_primary">';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '<div class="resbox">';
                html += '<span><em class="youhui">(已优惠'+youhui+'元)</em></span>';
                if(data.data[i].iforder == "0"){
                    html += '<button class="weui-btn weui-btn_primary reserBtn red-blue" onclick="toorderok('+data.data[i].id+')">预定</button>';
                }else if(data.data[i].iforder == "1"){
                    html += '<button class="weui-btn reserBtn weui-btn_disabled addCard" >满房</button>';
                }

                html += '</div>';
                html += '</a>';

                $("#roomlist").append(html);

            }
        })
    }

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }

    /*时间插件*/
    function JTCalendarDatabyroom() {
        $('#calendar').JTCalendar({
            title: '选择入住离店日期',
            inTime: new Date(),
            minDay: 1,
            monthNumber: 3,
            onClick: function($elm, cb) {
                console.log($elm, cb)
                getroominfo();
                $elm.find('[data-intime] .choose_date').text(cb.inTime.month + '月' + cb.inTime.date + '日')
                    .next('i.f').text(cb.inTime.dayText);

                $elm.find('[data-outtime] .choose_date').text(cb.outTime.month + '月' + cb.outTime.date + '日')
                    .next('i.f').text(cb.outTime.dayText);

                $elm.find('i.tottime').text(cb.totTime);

            }
        })

    }


    //获取可预定房间信息
    function  reserveopen(roomid) {
        getroomdetails(roomid);
        $("#full .yudinbtn").removeClass("addCard").addClass("red-blue");;
        $("#full .yudinbtn").text("预定");
        $("#full").popup();

    }

    function fullopen(roomid) {
        getroomdetails(roomid);
        $("#full").popup();
        $("#full .yudinbtn").addClass("addCard").removeClass('red-blue');
        $("#full .yudinbtn").text("满房");

    }

    //获取房型详细信息
    function getroomdetails(roomid ) {
        $.get("${ctx}/homestay/roomdetails",
            {
                roomid : roomid
            },
            function(data) {
                returnFloat(data.data.roomSalprice);
                var youhui = data.data.roomPrice - data.data.roomSalprice;
                var html1 = data.data.roomName+'<span class="close close-popup"></span>';
                $("#roomtitle").html(html1);
                $("#bedtype").html(data.data.bedType);
                $("#breakfastflag").html(data.data.breakfastFlag);
                $("#bednum").html(data.data.bedNum+"个");
                $("#roomarea").html(data.data.roomArea+"平米");
                $("#peoplenum").html(data.data.peopleNum+"人");
                $("#unsubscribeFlag").html(data.data.unsubscribeInfo);
                $("#largeprice").html(largePrice);
                $("#smallprice").html(smallPrice);
                $("#roomPrice").html(data.data.roomPrice);
                var html2 = '(已优惠<em>'+youhui +'</em>元)'
                $("#youhui").html(html2);
                $("#roomExplain").html(data.data.roomExplain);
                var html3 = '合计：<em>'+largePrice+'</em><i>'+smallPrice+'</i>';
                    $("#subtotal").html(html3);






            }
        );
    }

//列表直接预定房间
    function toorderok(roomid) {
        window.location.href = "${ctx}/homestay/homestayok?roomid="+roomid+"&buynum=1";
    }
</script>
</body>

</html>
