package com.oseasy.xlxq.service.oss;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 */
public interface OSSService {

    /**
     * 通过流方式上传文件到云存储空间
     * @param stream          输入流
     * @param fileLength    文件长度
     * @param orignalFileName      源文件名称
     * @return
     */
    public boolean uploadFileStream(InputStream stream, long fileLength,String orignalFileName,String repository,  String fileKey);


}
