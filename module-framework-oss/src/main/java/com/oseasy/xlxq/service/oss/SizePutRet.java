package com.oseasy.xlxq.service.oss;

public class SizePutRet {

    public String hash;
    public String key;
    public String bucket;
    public Long fsize;

    public SizePutRet() {

    }
}
