package com.oseasy.xlxq.service.api.base;

import com.oseasy.xlxq.service.api.exceptions.PageSizeTooLargeException;
import com.oseasy.xlxq.service.core.exceptions.MatchMutiEntitiesException;
import com.oseasy.xlxq.service.core.utils.Page;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("rawtypes")
public interface BaseService<T>{

	public T findById(Integer id);
	
	public long count();
	
	public T save(T entity);
	
	public void delete(Serializable id) throws IllegalAccessException, InvocationTargetException;
	
	public void delete(T entity) throws IllegalAccessException, InvocationTargetException;
	
	public Iterable<T> list();

    public Page<T> pageList(int pageNo, int pageSize) throws PageSizeTooLargeException, PageSizeTooLargeException;

    public Iterable<T> list(String hql, Object... objects);

    public Page pageList(String hql, Integer pageNo, Integer pageSize, Object... params) throws PageSizeTooLargeException;

	public T findUnique(String hql, Object... params) throws MatchMutiEntitiesException;

	public Iterable<T> save(Iterable<T> list);

	public void update(Integer id, T obj);

	public Iterable<T> findAll(Collection<String> ids);

    public void updateByArray(String feild, Object value, String idFiled, String[] idValues);

	public void updateNativeHql(String hql);

    public Object getByHql(String hql);

	public List<Object> getListBySql(String sql);

	public List<T> getList(String hql);

	public int executeSql(String sql);

	public List<T> executNativeQuery(String sql, Class clazz);

	public BigInteger executNativeTotalQuery(String sql);

	public Page pageNDList(String hql, int pageNo, int pageSize) throws PageSizeTooLargeException;

	public T getObjectByCondition(String hql,Object...params);

	public void trueDelete(T t);

	public Page<T> getPageLists(String hql,Integer pageNo, Integer pageSize,List<Object> list);

}
