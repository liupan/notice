package com.oseasy.xlxq.service.api.user.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.user.model.UserInfo;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface UserInfoService extends BaseService<UserInfo>{
}
