package com.oseasy.xlxq.service.api.exceptions;

/**
 * 账号不存在或账号被锁定会抛出此异常
 * Created by liups on 2016/8/1.
 */
public class AccountNotFoundException extends XlxqRuntimeException {
    public AccountNotFoundException(String message) {
        super("000022",message,null);
    }
}
