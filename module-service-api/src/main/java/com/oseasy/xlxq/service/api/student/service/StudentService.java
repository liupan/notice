package com.oseasy.xlxq.service.api.student.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.student.model.Student;

/**
 * Created by Administrator on 2017/9/16.
 */
public interface StudentService  extends BaseService<Student>{
}
