package com.oseasy.xlxq.service.api.message.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.message.model.MessageRead;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface MessageReadService extends BaseService<MessageRead>{
}
