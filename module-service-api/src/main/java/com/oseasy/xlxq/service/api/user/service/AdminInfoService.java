package com.oseasy.xlxq.service.api.user.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.user.model.AdminInfo;

/**
 * Created by Administrator on 2017/10/15.
 */
public interface AdminInfoService extends BaseService<AdminInfo> {
}
