package com.oseasy.xlxq.service.api.sys.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sys.model.FeedBackInfo;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface FeedBackService extends BaseService<FeedBackInfo>{
}
