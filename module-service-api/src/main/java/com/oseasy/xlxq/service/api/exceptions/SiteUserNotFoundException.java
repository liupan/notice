package com.oseasy.xlxq.service.api.exceptions;

/**
 * 账号不存在或账号被锁定会抛出此异常
 * Created by liups on 2016/8/9.
 */
public class SiteUserNotFoundException extends XlxqRuntimeException{
    public SiteUserNotFoundException(String message){
        super("004400",message,null);
    }
}
