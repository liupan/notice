package com.oseasy.xlxq.service.api.classinfo.model;


import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2017/9/17.
 */
@Entity
@Table( name = "t_class_info")
public class ClassInfo implements Serializable{

    private Integer id;

    private String name;

    private String code;

    private String grade;

    private String school;

    private String classNo;

    private String classmaster;

    private Integer teacherId;

    private Integer studentCount;

    private Integer parentCount;

    private Integer isDelete = 0;

    private Date deleteTime;

    private Date insertTime;

    private String publishDate;


    @Transient
    public String getPublishDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return simpleDateFormat.format(insertTime);
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }


    @Column(name = "is_delete")
    public Integer getIsDelete() {
        return isDelete;
    }

    @Column(name = "insert_time")
    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Column(name = "delete_time")
    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    @Transient
    public Integer getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(Integer studentCount) {
        this.studentCount = studentCount;
    }

    @Transient
    public Integer getParentCount() {
        return parentCount;
    }

    public void setParentCount(Integer parentCount) {
        this.parentCount = parentCount;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="grade")
    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Column(name="school")
    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    @Column(name="classNo")
    public String getClassNo() {
        return classNo;
    }

    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    @Column(name="classmaster")
    public String getClassmaster() {
        return classmaster;
    }

    public void setClassmaster(String classmaster) {
        this.classmaster = classmaster;
    }

    @Column(name="teacherId")
    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }
}
