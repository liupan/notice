package com.oseasy.xlxq.service.api.exceptions;

/**
 * 注册异常
 * Created by liups on 2016/6/23.
 */
public class RegisterException extends XlxqRuntimeException{
    public RegisterException(String message) {
        super("003300",message,null);
    }
}
