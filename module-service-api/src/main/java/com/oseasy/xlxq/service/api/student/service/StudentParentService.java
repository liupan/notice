package com.oseasy.xlxq.service.api.student.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.student.model.StudentParent;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface StudentParentService  extends BaseService<StudentParent> {
}
