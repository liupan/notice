package com.oseasy.xlxq.service.api.user.model;

import javax.persistence.*;

/**
 * Created by Administrator on 2017/10/15.
 */
@Entity
@Table(name="t_admin_info")
public class AdminInfo {

    private Integer id;

    private String username;

    private String password;

    private Integer type;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name="password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
