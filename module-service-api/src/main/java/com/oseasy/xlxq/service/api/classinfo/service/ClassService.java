package com.oseasy.xlxq.service.api.classinfo.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;

/**
 * Created by Administrator on 2017/9/17.
 */
public interface ClassService  extends BaseService<ClassInfo> {
}
