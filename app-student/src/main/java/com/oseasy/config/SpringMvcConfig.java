package com.oseasy.config;

import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Created by Tandy on 2016/6/6.
 */
@Configuration
@Component
public class SpringMvcConfig extends WebMvcConfigurerAdapter{

    private Log logger = LogFactory.getLog(SpringMvcConfig.class);

    @Autowired
    private RedisCacheService redisCacheService;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if(logger.isDebugEnabled()){
            logger.debug("添加静态资源文件映射");
        }
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");

    }

    //添加自定义的拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new TokenInterceptor(redisCacheService));
    }



    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setSuffix(".jsp");
        viewResolver.setPrefix("/WEB-INF/jsp/");
        registry.viewResolver(viewResolver);
    }

}
