package com.oseasy.controller;

import com.oseasy.util.StringUtil;
import com.oseasy.xlxq.service.api.user.model.UserInfo;
import com.oseasy.xlxq.service.api.user.service.UserInfoService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.HttpClientUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/9/17.
 */
@RestController
@RequestMapping("/userinfo")
public class UserInfoController {

    private final static String appid = "wx55bdb11fc9da6979";
    private final static String secret = "f7c01999917465cabd2193936cc92c9b";


    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private RedisCacheService redisCacheService;

    @RequestMapping("/updateType")
    @ResponseBody
    public RestResponse insert(Integer userId, Integer type) {
        RestResponse restResponse = new RestResponse();
        if (StringUtil.isBlank(type)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("类型不能为空");
            return restResponse;
        }
        if (StringUtil.isBlank(userId)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("用户ID不能为空");
            return restResponse;
        }
        try {
            UserInfo userInfo = userInfoService.findById(userId);
            if (userInfo == null) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("该用户不存在");
                return restResponse;
            }
            userInfo.setType(type);
            userInfoService.update(userId,userInfo);
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }

    @RequestMapping("/findUserById")
    @ResponseBody
    public RestResponse findUserById(Integer id) {
        RestResponse restResponse = new RestResponse();
        if (StringUtil.isBlank(id)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("id不能为空");
            return restResponse;
        }
        try {
            UserInfo userInfo = userInfoService.findById(id);
            restResponse.setSuccess(true);
            restResponse.setData(userInfo);
            return restResponse;
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
    }

    @RequestMapping("/findUserByWxId")
    @ResponseBody
    public Map findUserByWxId(String code) {
        Map restResponse = new HashMap<>();
        try {
            if (StringUtil.isBlank(code)) {
                restResponse.put("status", 0);
                restResponse.put("msg", "code不能为空");
                return restResponse;
            }
            String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code";
            String html = HttpClientUtil.httpGet(url);
            Map map = JSONUtil.parseObject(html);
            if (map.get("errcode") != null) {
                restResponse.put("status", 0);
                restResponse.put("msg", map.get("errmsg"));
                return restResponse;
            }
            String wxId = map.get("openid").toString();
            if (StringUtil.isBlank(wxId)) {
                restResponse.put("status", 0);
                restResponse.put("msg", "获取appid失败");
                return restResponse;
            }
            UserInfo userInfo = userInfoService.getObjectByCondition("from UserInfo obj where obj.isDelete = 0 and obj.wxId = ?", wxId);
            if (userInfo == null) {
                userInfo = new UserInfo();
                userInfo.setWxId(wxId);
                userInfo.setType(-1);
                userInfoService.save(userInfo);
                userInfo.setWxId(null);
                restResponse.put("status", 1);
                restResponse.put("msg", "成功");
                restResponse.put("data", userInfo);
            } else {
                restResponse.put("status", 2);
                restResponse.put("msg", "成功");
                userInfo.setWxId(null);
                restResponse.put("data", userInfo);
            }
            String token = UUIDGenerator.uuid();
            redisCacheService.set("notice:login:token:" + token, userInfo.getId().toString());
            restResponse.put("token", token);
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.put("status", 0);
            restResponse.put("msg", "失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }


    @RequestMapping("/checkToken")
    public RestResponse checkToken(String token) {
        String userId = redisCacheService.get("notice:login:token:" + token);
        if (!StringUtil.isBlank(userId)) {
            redisCacheService.set("notice:login:token:" + token, userId);
        }
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("ok");
        return restResponse;
    }

}
