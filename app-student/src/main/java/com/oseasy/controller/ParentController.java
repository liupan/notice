package com.oseasy.controller;

import com.oseasy.util.StringUtil;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;
import com.oseasy.xlxq.service.api.classinfo.service.ClassService;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.model.StudentParent;
import com.oseasy.xlxq.service.api.student.service.StudentParentService;
import com.oseasy.xlxq.service.api.student.service.StudentService;
import com.oseasy.xlxq.service.api.user.model.UserInfo;
import com.oseasy.xlxq.service.api.user.service.UserInfoService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/9/17.
 */
@RestController
@RequestMapping("/parent")
public class ParentController {

    @Autowired
    private StudentService studentService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private StudentParentService studentParentService;
    @Autowired
    private ClassService classService;
    @Autowired
    private RedisCacheService redisCacheService;

    @RequestMapping("getClassInfoById")
    @ResponseBody
    public RestResponse findClassInfo(Integer id, String token) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(id)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("id不能为空");
                return restResponse;
            }
            Integer parentId = Integer.parseInt(redisCacheService.get("notice:login:token:" + token));
            List<StudentParent> studentParentList = studentParentService
                    .getList("from StudentParent obj where obj.isDelete = 0 and obj.parentId = " + parentId);
            boolean flag = false;
            if (studentParentList != null && studentParentList.size() > 0) {
                for (StudentParent studentParent : studentParentList) {
                    Student student = studentService.getObjectByCondition("from Student where isDetele = 0 and id = ?",
                            studentParent.getStudentId());
                    if(id.equals(student.getClassId())){
                        flag = true;
                    }
                }
            }
            if(!flag){
                restResponse.setErrorCode("20002");
                restResponse.setErrorMsg("查询失败，您未加入该班级");
            }
            ClassInfo classInfo = classService
                    .getObjectByCondition("from ClassInfo obj where obj.isDelete = 0 and obj.id = ?", id);
            restResponse.setSuccess(true);
            restResponse.setData(classInfo);
            return restResponse;
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("异常信息" + e.getMessage());
            return restResponse;
        }
    }

    @RequestMapping("/findParentListByStudentId")
    @ResponseBody
    public RestResponse findParentListById(Integer studentId) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(studentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学生id不能为空");
                return restResponse;
            }
            List<Object> list = userInfoService.getListBySql(
                    "select parent_id from t_student_parent where is_delete = 0 and student_id = " + studentId);
            String ids = "";
            for (int i = 0; i < list.size(); i++) {
                Integer id = Integer.parseInt(list.get(i).toString());
                if (i == 0) {
                    ids += (id);
                } else {
                    ids += ("," + id);
                }
            }
            List<UserInfo> userInfoList = new ArrayList<>();
            if (list != null && list.size() != 0) {
                userInfoList = userInfoService
                        .getList("from UserInfo obj where obj.isDelete = 0 and obj.id in(" + ids + ")");
                for (UserInfo userInfo : userInfoList) {
                    userInfo.setRelationship(studentParentService.getObjectByCondition(
                            "from StudentParent bean where bean.isDelete = 0 and bean.parentId = ?", userInfo.getId())
                            .getRelationship());
                }
            }

            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(userInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("异常信息" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("addStudent")
    @ResponseBody
    public RestResponse addStudent(Integer parentId, String studentName, Integer classId) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(parentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("家长id不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(studentName)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学生姓名不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班级id不能为空");
                return restResponse;
            }
            Student student = studentService.getObjectByCondition(
                    "from Student obj where obj.isDelete = 0 and  obj.classId = ? and obj.studentName = ?", classId,
                    studentName);
            if (student == null) {
                student = new Student();
                student.setClassId(classId);
                student.setStudentName(studentName);
                studentService.save(student);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(student);
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("/joinClass")
    @ResponseBody
    public RestResponse addClass(String name, String relationship, String phone, Integer parentId, Integer studentId,
                                 String nickName) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(name)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("姓名不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(relationship)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("家长与学生关系不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(phone)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("电话不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(parentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("家长id不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(studentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学生id不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(nickName)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("昵称不能为空");
                return restResponse;
            }
            Student student = studentService
                    .getObjectByCondition("from Student where isDelete=0 and id = " + studentId);
            List<StudentParent> studentParentList = studentParentService
                    .getList("from StudentParent where isDelete = 0 and parentId = " + parentId);
            if (studentParentList != null && studentParentList.size() > 0) {
                for (StudentParent studentParent : studentParentList) {
                    Student student1 = studentService.getObjectByCondition(
                            "from Student where isDelete=0 and id = " + studentParent.getStudentId());
                    if (student1 != null & student1.getClassId() == student.getClassId()) {
                        restResponse.setSuccess(false);
                        restResponse.setErrorMsg("一个班只能绑定一个学生");
                        return restResponse;
                    }
                }
            }
            StudentParent studentParent = new StudentParent();
            studentParent.setParentId(parentId);
            studentParent.setStudentId(studentId);
            studentParent.setRelationship(relationship);
            studentParentService.save(studentParent);

            UserInfo userInfo = userInfoService.findById(parentId);

            userInfo.setPhone(phone);
            userInfo.setName(name);
            userInfo.setNikeName(nickName);
            userInfoService.update(userInfo.getId(), userInfo);

            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

}
