package com.oseasy.controller;

import com.oseasy.util.StringUtil;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;
import com.oseasy.xlxq.service.api.classinfo.service.ClassService;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.service.StudentService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2017/9/19.
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;
    @Autowired
    private ClassService classService;

    @RequestMapping("/findStudentListByClassId")
    @ResponseBody
    public RestResponse findStudentListByClassId(Integer id,Integer pageNo,Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(id)) {
                restResponse.setSuccess(true);
                restResponse.setErrorMsg("班级id不能为空");
                return restResponse;
            }
            Page<Student> studentList = studentService.pageList("from Student obj where obj.isDelete = 0 and obj.classId = " + id, pageNo, pageSize);
            for (Student student : studentList.getResult()) {
                student.setParentCount(studentService.executNativeTotalQuery("select count(1) from t_student_parent where is_delete = 0 and student_id = " + student.getId()).intValue());
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(studentList);
        }catch (Exception e){
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("/deleteStudent")
    @ResponseBody
    public RestResponse deleteStudent(Integer studentId) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(studentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学生id不能为空");
                return restResponse;
            }
            Student student = studentService.findById(studentId);
            studentService.trueDelete(student);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("异常信息" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("/addStudent")
    @ResponseBody
    public RestResponse addStudent(Student student) {
        RestResponse restResponse = new RestResponse();
        try {
            if (student == null) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("非法请求");
                return restResponse;
            }
            if (StringUtil.isBlank(student.getClassId())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班级id不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(student.getStudentName())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学生姓名不能为空");
                return restResponse;
            }
            Student oldStudent = studentService.getObjectByCondition("from Student obj where obj.isDelete = 0 and obj.classId = ? and obj.studentName = ?", student.getClassId(), student.getStudentName());
            if (oldStudent == null) {
                studentService.save(student);
                restResponse.setSuccess(true);
                restResponse.setErrorMsg("成功");
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("该班级已存在该名学生，是否为同名，建议区分一下");
            }
        }catch (Exception e){
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }
}
