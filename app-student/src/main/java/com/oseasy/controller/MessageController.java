package com.oseasy.controller;

import com.oseasy.util.StringUtil;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;
import com.oseasy.xlxq.service.api.classinfo.service.ClassService;
import com.oseasy.xlxq.service.api.message.model.MessageAttachment;
import com.oseasy.xlxq.service.api.message.model.MessageInfo;
import com.oseasy.xlxq.service.api.message.model.MessageRead;
import com.oseasy.xlxq.service.api.message.service.MessageAttachmentService;
import com.oseasy.xlxq.service.api.message.service.MessageReadService;
import com.oseasy.xlxq.service.api.message.service.MessageService;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.model.StudentParent;
import com.oseasy.xlxq.service.api.student.service.StudentParentService;
import com.oseasy.xlxq.service.api.student.service.StudentService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


/**
 * Created by Administrator on 2017/9/17.
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageAttachmentService messageAttachmentService;
    @Autowired
    private MessageReadService messageReadService;
    @Autowired
    private StudentParentService studentParentService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private ClassService classService;

    @RequestMapping("findMessageListByTeacherId")
    public RestResponse list(Integer teacherId, Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        if (StringUtil.isBlank(teacherId)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("老师id不能为空");
            return restResponse;
        }
        if (StringUtil.isBlank(pageNo)) {
            pageNo = 1;
        }
        if (StringUtil.isBlank(pageSize)) {
            pageSize = 10;
        }
        try {
            Page<MessageInfo> messageInfos = messageService.pageList("from MessageInfo obj  where obj.isDelete = 0 and obj.teacherId = ? order by obj.priority desc,obj.isUrgent desc,obj.inserttime desc", pageNo, pageSize, teacherId);
            for (MessageInfo messageInfo : messageInfos.getResult()) {
                ClassInfo classInfo = classService.getObjectByCondition("from ClassInfo bean where bean.isDelete=0 and  bean.id = ?", messageInfo.getClassId());
                messageInfo.setClassmaster(classInfo.getClassmaster());
                messageInfo.setClassName(classInfo.getName());
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(messageInfos);
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("异常信息" + e.getMessage());
        }
        return restResponse;
    }


    @RequestMapping("/addMessage")
    @ResponseBody
    public RestResponse addMessage(MessageInfo messageInfo, String images, String sounds) {
        RestResponse restResponse = new RestResponse();
        try {
            if (messageInfo == null) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("请求错误");
                return restResponse;
            }
            if (StringUtil.isBlank(messageInfo.getTeacherId()) && StringUtil.isBlank(messageInfo.getParentId())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("请求参数id不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(messageInfo.getParentId()) && StringUtil.isBlank(messageInfo.getClassId())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班级不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(messageInfo.getParentId()) && StringUtil.isBlank(messageInfo.getTitle())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("标题不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(messageInfo.getContent())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("内容不能为空");
                return restResponse;
            }
            if (messageInfo.getContent().length() > 300) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("内容太长");
                return restResponse;
            }
            if (StringUtil.isBlank(messageInfo.getTeacherId()) && StringUtil.isBlank(messageInfo.getParentMessageId())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("回复消息ID不能为空");
                return restResponse;
            }
            boolean flag = true;
            if (!StringUtil.isBlank(messageInfo.getParentMessageId())) {
                MessageInfo messageInfo1 = messageService.findById(messageInfo.getParentMessageId());
                List<StudentParent> studentParentList = studentParentService.getList("from StudentParent obj where obj.isDelete = 0 and obj.parentId = " + messageInfo.getParentId());
                if (studentParentList != null && studentParentList.size() > 0) {
                    for (StudentParent studentParent : studentParentList) {
                        Student student = studentService.getObjectByCondition("from Student where id = ?", studentParent.getStudentId());
                        if(student.getClassId().equals(messageInfo1.getClassId())){
                            flag = false;
                        }
                    }
                }
                messageInfo.setClassId(messageInfo1.getClassId());
                if(flag){
                    restResponse.setSuccess(false);
                    restResponse.setErrorMsg("您未加入该班级,不能回复此消息!");
                    return restResponse;
                }
            }
            messageInfo.setInserttime(new Date());
            messageService.save(messageInfo);
            if (!StringUtil.isBlank(images)) {
                String[] imgs = images.split(",");
                for (String str : imgs) {
                    MessageAttachment messageAttachment = new MessageAttachment();
                    messageAttachment.setMessageId(messageInfo.getId());
                    messageAttachment.setUrl(str);
                    messageAttachment.setType(1);
                    messageAttachmentService.save(messageAttachment);
                }
            }

            if (!StringUtil.isBlank(sounds)) {
                MessageAttachment messageAttachment = new MessageAttachment();
                messageAttachment.setMessageId(messageInfo.getId());
                messageAttachment.setUrl(sounds);
                messageAttachment.setType(2);
                messageAttachmentService.save(messageAttachment);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(messageInfo.getId());
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("findMessageById")
    @ResponseBody
    public Map findMessageInfo(Integer id, Integer teacherId, Integer parentId, Integer pageNo, Integer pageSize) {
        Map restResponse = new HashMap<>();

        try {
            if (StringUtil.isBlank(teacherId) && StringUtil.isBlank(parentId)) {
                restResponse.put("status", "false");
                restResponse.put("msg", "请求异常,教师id或家长id为空");
                return restResponse;
            }
            if (StringUtil.isBlank(id)) {
                restResponse.put("status", "false");
                restResponse.put("msg", "请求异常,消息id为空");
                return restResponse;
            }
            MessageInfo messageInfo = messageService.findById(id);

            if (!StringUtil.isBlank(messageInfo.getClassId())) {
                ClassInfo classInfo = classService.getObjectByCondition("from ClassInfo bean where bean.isDelete = 0 and bean.id = ?", messageInfo.getClassId());
                messageInfo.setClassmaster(classInfo.getClassmaster());
                messageInfo.setClassName(classInfo.getName());
            }
            boolean flag = true;
            //家长访问
            if (StringUtil.isBlank(teacherId)) {
                List<StudentParent> studentParentList = studentParentService.getList("from StudentParent obj where obj.isDelete = 0 and obj.parentId = " + parentId);
                if (studentParentList != null && studentParentList.size() > 0) {
                    for (StudentParent studentParent : studentParentList) {
                        Student student = studentService.getObjectByCondition("from Student where id = ? and classId = ?", studentParent.getStudentId(), messageInfo.getClassId());
                        if (student != null) {
                            flag = false;
                            //记录阅读时间
                            MessageRead messageRead = messageReadService.getObjectByCondition("from MessageRead obj where obj.messageId= ? and obj.studentId = ?", messageInfo.getId(), studentParent.getStudentId());
                            if (messageRead == null) {
                                messageRead = new MessageRead();
                                messageRead.setMessageId(messageInfo.getId());
                                messageRead.setParentId(studentParent.getParentId());
                                messageRead.setStudentId(studentParent.getStudentId());
                                messageRead.setInserttime(new Date());
                                messageReadService.save(messageRead);
                            }
                        }
                    }
                }
                if (flag) {
                    restResponse.put("success", false);
                    restResponse.put("errorCode", 20002);
                    restResponse.put("errorMsg", "您未加入该班级,信息获取失败!");
                    return restResponse;
                }
                MessageInfo replyMessageInfo = messageService.getObjectByCondition("from MessageInfo obj where obj.isDelete = 0 and obj.parentMessageId = ? and obj.parentId=?", messageInfo.getId(), parentId);
                restResponse.put("replyMessageInfo", replyMessageInfo);
            }

            //老师访问
            if (StringUtil.isBlank(parentId)) {
                if(!teacherId.equals(messageInfo.getTeacherId())){
                    restResponse.put("success", false);
                    restResponse.put("errorCode", 20002);
                    restResponse.put("errorMsg", "该班级不是您创建，查看失败");
                    return restResponse;
                }
                //获取学生列表
                List<Student> studentList = studentService.getList("from Student where isDelete = 0 and classId = " + messageInfo.getClassId());
                if (studentList != null) {
                    restResponse.put("studentList", studentList);
                    Integer readCount = 0;
                    Integer replyCount = 0;
                    for (Student student : studentList) {
                        MessageRead messageRead = messageReadService.getObjectByCondition("from MessageRead obj where obj.studentId = ? and obj.messageId = ?", student.getId(), messageInfo.getId());
                        if (messageRead != null) {
                            readCount++;
                            student.setIsRead(1);
                        }
                        Integer count = messageService.executNativeTotalQuery("select count(1) from t_message_info where is_delete = 0 and  parent_id in (select parent_id from t_student_parent where is_delete = 0 and student_id = " + student.getId() + ") and parent_message_id = " + id).intValue();
                        if (count != 0) {
                            replyCount++;
                            student.setIsReply(1);
                        }
                    }
                    restResponse.put("readCount", readCount);
                    restResponse.put("replyCount", replyCount);
                }
            }

            restResponse.put("messageInfo", messageInfo);

            if (messageInfo != null) {
                List<MessageAttachment> messageAttachmentList = messageAttachmentService.getList(" from MessageAttachment obj where obj.isDelete=0 and obj.messageId = " + messageInfo.getId());
                restResponse.put("messageAttachmentList", messageAttachmentList);
            }

            restResponse.put("success", "true");
            restResponse.put("msg", "成功");
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.put("success", "false");
            restResponse.put("msg", "失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }


    @RequestMapping("findMessageListByParentId")
    @ResponseBody
    public RestResponse findMessageListByParentId(String parentId, Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(parentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("家长id不能为空");
                return restResponse;
            }
            List<Object> objectList = studentParentService.getListBySql("select class_id from t_student_info where is_delete = 0 and  id in(select student_id from t_student_parent where parent_id = " + parentId + ")");
            String ids = "";
            for (int i = 0; i < objectList.size(); i++) {
                Integer id = Integer.parseInt(objectList.get(i).toString());
                if (i == 0) {
                    ids += (id);
                } else {
                    ids += ("," + id);
                }
            }
            Page<MessageInfo> messageList = new Page<MessageInfo>();
            if (objectList != null && objectList.size() != 0) {
                messageList = messageService.pageList("from MessageInfo bean where bean.isDelete = 0 and bean.classId in (" + ids + ") and bean.parentId is null order by bean.priority desc,bean.isUrgent desc,bean.inserttime desc", pageNo, pageSize);
                for (MessageInfo messageInfo : messageList.getResult()) {
                    ClassInfo classInfo = classService.getObjectByCondition("from ClassInfo bean where bean.isDelete = 0 and  bean.id = ?", messageInfo.getClassId());
                    messageInfo.setClassmaster(classInfo.getClassmaster());
                    messageInfo.setClassName(classInfo.getName());
                }
            }
            restResponse.setData(messageList);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");

        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }


    @RequestMapping("findReplyMessageById")
    @ResponseBody
    public RestResponse findReplyMessageById(Integer studentId, Integer messageId) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(studentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学生ID不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(messageId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("消息ID不能为空");
                return restResponse;
            }
            List<Map> resultList = new ArrayList<>();
            List<Object> objectList = studentParentService.getListBySql("select parent_id from t_student_parent where is_delete = 0 and student_id = " + studentId);
            String ids = "";
            for (int i = 0; i < objectList.size(); i++) {
                Integer id = Integer.parseInt(objectList.get(i).toString());
                if (i == 0) {
                    ids += (id);
                } else {
                    ids += ("," + id);
                }
            }
            if (objectList != null && objectList.size() != 0) {
                List<MessageInfo> messageLists = messageService.getList("from MessageInfo obj where obj.isDelete = 0 and obj.parentId in (" + ids + " ) and obj.parentMessageId = " + messageId);
                for (MessageInfo messageInfo : messageLists) {
                    Map map = new HashMap<>();
                    MessageRead messageRead = messageReadService.getObjectByCondition("from MessageRead bean where bean.messageId = ? and bean.parentId = ? and bean.studentId = ?", messageId, messageInfo.getParentId(), studentId);
                    map.put("content", messageInfo.getContent());
                    map.put("readTime", messageRead.getReadTime());
                    map.put("replyDate", messageInfo.getPublishDate());
                    StudentParent studentParent = studentParentService.getObjectByCondition("from StudentParent obj where obj.isDelete = 0 and obj.parentId = ? and studentId = ?", messageInfo.getParentId(), studentId);
                    map.put("relationship", studentParent.getRelationship());
                    List<MessageAttachment> messageAttachmentList = messageAttachmentService.getList(" from MessageAttachment obj where obj.isDelete = 0 and obj.messageId = " + messageInfo.getId());
                    map.put("messageAttachmentList", messageAttachmentList);
                    resultList.add(map);
                }
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(resultList);
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
        }
        return restResponse;
    }

}
