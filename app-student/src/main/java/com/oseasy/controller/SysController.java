package com.oseasy.controller;

import com.oseasy.util.StringUtil;
import com.oseasy.xlxq.service.api.exceptions.UploadFileException;
import com.oseasy.xlxq.service.api.sys.model.FeedBackInfo;
import com.oseasy.xlxq.service.api.sys.service.FeedBackService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.config.SystemConfig;
import com.oseasy.xlxq.service.core.security.SecurityUser;
import com.oseasy.xlxq.service.core.utils.DateUtils;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.oseasy.xlxq.service.oss.OSSService;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Administrator on 2017/9/17.
 */
@RestController
@RequestMapping("/sys")
public class SysController {

    @Autowired
    private FeedBackService feedBackService;

    @Autowired
    private OSSService ossService;

    @Autowired
    private RedisCacheService redisCacheService;

    @RequestMapping("/addFeedBack")
    @ResponseBody
    public RestResponse addFeedBack(FeedBackInfo feedBackInfo) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(feedBackInfo.getContent())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("内容不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(feedBackInfo.getUserId())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("用户主键不能为空");
                return restResponse;
            }

            feedBackService.save(feedBackInfo);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
        } catch (Exception e) {
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }


    @RequestMapping("/uploadFile")
    public RestResponse uploadResouces(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile[] files) throws UploadFileException {
        List<Map> uploadFilesResult = new LinkedList<>();
        for (MultipartFile file : files) {
            if (file.getSize() > 0) {
                Map fileResult = uploadFile(request, file);
                uploadFilesResult.add(fileResult);
            }
        }
        RestResponse responsex = RestResponse.success(uploadFilesResult);
        responsex.setErrorMsg("成功");
        return responsex;
    }

    private Map uploadFile(HttpServletRequest request, MultipartFile file) throws UploadFileException {
        String fileid = UUIDGenerator.uuid();
        String userId = redisCacheService.get("notice:login:token:" + request.getParameter("token"));
        String dt = DateUtils.getDate("yyyyMMdd");
        String ossBucket = SystemConfig.getProperty("global.oss.aliyun.bucket");
        int suffix = file.getOriginalFilename().lastIndexOf(".");
        String fileKey = String.format("student/%s/%s/%s" + file.getOriginalFilename().substring(suffix), userId, dt, fileid);
        boolean uploadResult = false;
        try {
            if (file != null) {
                uploadResult = ossService.uploadFileStream(file.getInputStream(), file.getSize(), file.getOriginalFilename(), ossBucket, fileKey);
                if (!uploadResult) {
                    throw new UploadFileException("上传文件时出现异常");
                }
            }
        } catch (Exception ex) {
            throw new UploadFileException(ex);
        }

        Map resultMap = new HashMap<>();
        resultMap.put("url", SystemConfig.getProperty("global.oss.aliyun.endpoint.internet") + "/" + fileKey);
        return resultMap;
    }


    @RequestMapping("/uoloadSounds")
    public RestResponse uploadSounds(MultipartFile file, HttpServletRequest request) {
        RestResponse restResponse = new RestResponse();
        try {
            if (file.getSize() > 0) {
                String userId = redisCacheService.get("notice:login:token:" + request.getParameter("token"));
                String filename = file.getOriginalFilename();
                String path = request.getSession().getServletContext().getRealPath("../");
                String fileUUIDname = UUIDGenerator.uuid();
                String prefix = filename.substring(filename.lastIndexOf(".") + 1);
                String NewFileName = fileUUIDname + "." + prefix;
                String completepath = path + "/notice_sounds/" + userId + "/" + NewFileName;
                File targetFile = new File(completepath);
                if (!targetFile.exists()) {
                    targetFile.mkdirs();
                }
                file.transferTo(targetFile);
                restResponse.setData("/notice_sounds/"+userId + "/"+ NewFileName);
            }
        }catch (Exception e){
            e.printStackTrace();
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败"+e.getMessage());
        }
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }
}
