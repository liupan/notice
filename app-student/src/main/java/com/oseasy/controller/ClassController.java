package com.oseasy.controller;

import com.oseasy.util.StringUtil;
import com.oseasy.xlxq.service.api.classinfo.model.ClassInfo;
import com.oseasy.xlxq.service.api.classinfo.service.ClassService;
import com.oseasy.xlxq.service.api.message.model.MessageAttachment;
import com.oseasy.xlxq.service.api.message.model.MessageInfo;
import com.oseasy.xlxq.service.api.message.model.MessageRead;
import com.oseasy.xlxq.service.api.message.service.MessageService;
import com.oseasy.xlxq.service.api.student.model.Student;
import com.oseasy.xlxq.service.api.student.model.StudentParent;
import com.oseasy.xlxq.service.api.student.service.StudentParentService;
import com.oseasy.xlxq.service.api.student.service.StudentService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Administrator on 2017/9/17.
 */
@RestController
@RequestMapping("/classinfo")
public class ClassController {

    @Autowired
    private ClassService classService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private RedisCacheService redisCacheService;
    @Autowired
    private StudentParentService studentParentService;
    @Autowired
    private MessageService messageService;

    @RequestMapping("/addClass")
    @ResponseBody
    public RestResponse addClass(ClassInfo classInfo) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(classInfo.getName())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班级名称不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classInfo.getSchool())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("学校全称不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classInfo.getGrade())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("年级不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classInfo.getClassNo())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班级不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classInfo.getClassmaster())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班主任不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classInfo.getTeacherId())) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("教师ID不能为空");
                return restResponse;
            }
            String code = "";
            do {
                code = (int) ((Math.random() * 9 + 1) * 100000) + "";
            } while (redisCacheService.get("notice:class:code:" + code) != null
                    && classService.getObjectByCondition("from ClassInfo bean where bean.code = ?", code) != null);

            classInfo.setCode(code);
            classInfo.setInsertTime(new Date());
            classService.save(classInfo);

            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(classInfo.getId());
            redisCacheService.set("notice:class:code:" + code, classInfo.getId().toString());
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("/getClassListByTeacherId")
    @ResponseBody
    public RestResponse list(Integer teacherId, Integer status, Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(teacherId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("教师ID不能为空");
                return restResponse;
            }
            if (status == 1) {
                if (StringUtil.isBlank(pageNo)) {
                    pageNo = 1;
                }
                if (StringUtil.isBlank(pageSize)) {
                    pageSize = 10;
                }
                Page<ClassInfo> classInfoList = classService.pageList(
                        "from ClassInfo obj where obj.teacherId = ? and obj.isDelete = 0", pageNo, pageSize, teacherId);
                for (ClassInfo classInfo : classInfoList.getResult()) {
                    classInfo.setStudentCount(studentService.executNativeTotalQuery(
                            "select count(1) from t_student_info where is_delete = 0 and class_id ="
                                    + classInfo.getId())
                            .intValue());
                    classInfo.setParentCount(studentService.executNativeTotalQuery(
                            "select count(1) from t_student_parent where  is_delete = 0 and student_id in (select id from t_student_info where is_delete = 0 and class_id = "
                                    + classInfo.getId() + ")")
                            .intValue());
                }
                restResponse.setData(classInfoList);
            } else {
                List<ClassInfo> classInfoList = classService
                        .getList("from ClassInfo obj where obj.isDelete=0 and obj.teacherId = " + teacherId);
                restResponse.setData(classInfoList);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("/deleteClassInfo")
    @ResponseBody
    public RestResponse delClassInfo(Integer id, Integer teacherId) {
        RestResponse restResponse = new RestResponse();

        if (StringUtil.isBlank(id)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("班级ID不能为空");
            return restResponse;
        }
        if (StringUtil.isBlank(teacherId)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("老师ID不能为空");
            return restResponse;
        }
        ClassInfo classInfo = classService.findById(id);
        if (!classInfo.getTeacherId().equals(teacherId)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("该班级无法由您解散");
            return restResponse;
        }
        try {
            classService.trueDelete(classInfo);
            classService.executeSql("delete from Student where classId = " + id);
            classService.executeSql("delete from MessageInfo where classId = " + id);
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restResponse;
    }

    @RequestMapping("/checkClassCode")
    @ResponseBody
    public RestResponse findClassIdByCode(String code) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(code)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("识别码不能为空");
                return restResponse;
            }
            String classId = redisCacheService.get("class:code:" + code);
            if (StringUtil.isBlank(classId)) {
                ClassInfo classInfo = classService
                        .getObjectByCondition("from ClassInfo obj where obj.isDelete = 0 and obj.code = ?", code);
                if (classInfo == null) {
                    restResponse.setSuccess(false);
                    restResponse.setErrorMsg("该识别码不存在");
                    return restResponse;
                } else {
                    restResponse.setSuccess(true);
                    restResponse.setErrorMsg("成功");
                    restResponse.setData(classInfo.getId());
                    redisCacheService.set("notice:class:code:" + code, classInfo.getId().toString());
                    return restResponse;
                }
            } else {
                restResponse.setSuccess(true);
                restResponse.setErrorMsg("成功");
                restResponse.setData(classId);
            }
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("/findClassListByParentId")
    @ResponseBody
    public RestResponse findClassListByParentId(Integer parentId, Integer pageNo, Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        if (StringUtil.isBlank(parentId)) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("家长id不能为空");
            return restResponse;
        }
        List<Object> list = new ArrayList<>();
        try {
            list = classService.getListBySql(
                    "select class_id from t_student_info where is_delete = 0 and id in(select student_id from t_student_parent where is_delete = 0 and parent_id = "
                            + parentId + ")");
            String ids = "";
            for (int i = 0; i < list.size(); i++) {
                Integer id = Integer.parseInt(list.get(i).toString());
                if (i == 0) {
                    ids += (id);
                } else {
                    ids += ("," + id);
                }
            }
            Page<ClassInfo> classInfoPage = new Page<ClassInfo>();
            if (list != null && list.size() != 0) {
                classInfoPage = classService.pageList("from ClassInfo where isDelete = 0 and id in( " + ids + " )",
                        pageNo, pageSize);
            }
            restResponse.setData(classInfoPage);
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        restResponse.setSuccess(true);
        restResponse.setErrorMsg("成功");
        return restResponse;
    }

    @RequestMapping("/quitClass")
    @ResponseBody
    public RestResponse quitClass(Integer parentId, Integer classId) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(parentId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("家长id不能为空");
                return restResponse;
            }
            if (StringUtil.isBlank(classId)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("班级id不能为空");
                return restResponse;
            }
            List<StudentParent> studentParentList = studentParentService
                    .getList("from StudentParent obj where obj.isDelete = 0 and obj.parentId = " + parentId);
            for (StudentParent studentParent : studentParentList) {
                Student student = studentService.getObjectByCondition("from Student where id = ? and classId = ?",
                        studentParent.getStudentId(), classId);
                if (student != null) {
                    studentParentService.trueDelete(studentParent);
                    MessageInfo messageInfo = messageService.getObjectByCondition(
                            "from MessageInfo where classId = ? and parentId = ?", classId, parentId);
                    if (messageInfo != null) {
                        studentParentService.executeSql(
                                "delete from MessageInfo where parentId = " + parentId + " and classId = " + classId);
                        studentParentService
                                .executeSql("delete from MessageAttachment where messageId = " + messageInfo.getId());
                        studentParentService.executeSql("delete from MessageRead where messageId = "
                                + messageInfo.getId() + " and parentId = " + parentId);
                    }
                }
            }

            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }

    @RequestMapping("getClassInfoById")
    @ResponseBody
    public RestResponse findClassInfo(String token, Integer id) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isBlank(id)) {
                restResponse.setSuccess(false);
                restResponse.setErrorMsg("id不能为空");
                return restResponse;
            }
            Integer userId = Integer.parseInt(redisCacheService.get("notice:login:token:" + token));
            ClassInfo classInfo = classService
                    .getObjectByCondition("from ClassInfo obj where obj.isDelete = 0 and obj.id = ?", id);
            if(!userId.equals(classInfo.getTeacherId())){
                restResponse.setSuccess(false);
                restResponse.setErrorCode("20002");
                restResponse.setErrorMsg("该班级不是您创建查看失败!");
                return restResponse;
            }
            restResponse.setSuccess(true);
            restResponse.setErrorMsg("成功");
            restResponse.setData(classInfo);
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setErrorMsg("失败" + e.getMessage());
            return restResponse;
        }
        return restResponse;
    }
}
