package com.oseasy.app.backend;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.monitor.FrameworkMonitorConfig;
import com.oseasy.xlxq.service.mq.FrameworkMQConfig;
import com.oseasy.xlxq.service.sms.FrameworkSmsConfig;
import com.oseasy.xlxq.service.web.web.AbstractSpringBootWebStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * Created by Tandy on 2016/6/13.
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@Import({ServiceApiConfig.class,ServiceConfig.class, FrameworkCacheConfig.class,
        FrameworkSmsConfig.class, FrameworkMQConfig.class, FrameworkOSSConfig.class, FrameworkMonitorConfig.class})
public class MainClass extends AbstractSpringBootWebStarter {

    public static final String systemId = "app.backend";

    static {
        System.setProperty("systemId",systemId);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MainClass.class, args);
    }

    @Override
    public String systemId() {
        return systemId;
    }
}
