package com.oseasy.xlxq.service.web.utils;

import java.util.Date;

import com.oseasy.xlxq.service.core.utils.DateUtils;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import org.springframework.core.convert.converter.Converter;

public class StringDateConvertor implements Converter<String, Date>{

	public Date convert(String source) {
		if(StringUtil.isNotEmpty(source)){
			return DateUtils.parseDate(source);
		}else{
			return new Date();
		}
		
	}
	
 
}
